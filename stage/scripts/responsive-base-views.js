this["J"] = this["J"] || {};
this["J"]["views"] = this["J"]["views"] || {};

this["J"]["views"]["dynamic-cart/dynamic-cart-item"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"dc-item-row "
    + ((stack1 = (helpers.ifValue||(depth0 && depth0.ifValue)||alias2).call(alias1,(depth0 != null ? depth0.ProductId : depth0),{"name":"ifValue","hash":{"equals":(depths[1] != null ? depths[1].lastProductAdded : depths[1])},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":24},"end":{"line":2,"column":99}}})) != null ? stack1 : "")
    + "\" data-id=\""
    + alias4(((helper = (helper = helpers.ProductId || (depth0 != null ? depth0.ProductId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductId","hash":{},"data":data,"loc":{"start":{"line":2,"column":110},"end":{"line":2,"column":125}}}) : helper)))
    + "\" data-rec-id=\""
    + alias4(((helper = (helper = helpers.RecId || (depth0 != null ? depth0.RecId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RecId","hash":{},"data":data,"loc":{"start":{"line":2,"column":140},"end":{"line":2,"column":151}}}) : helper)))
    + "\" data-attribute-id=\"-1\">\r\n    <div class=\"img\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":4,"column":8},"end":{"line":8,"column":15}}})) != null ? stack1 : "")
    + "    </div>\r\n    <div class=\"item\">\r\n        <div class=\"description\">\r\n            <a class=\"name\" href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage||(depth0 && depth0.isStage)||alias2).call(alias1,{"name":"isStage","hash":{},"data":data,"loc":{"start":{"line":12,"column":40},"end":{"line":12,"column":49}}}),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":34},"end":{"line":12,"column":64}}})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data,"loc":{"start":{"line":12,"column":64},"end":{"line":12,"column":81}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data,"loc":{"start":{"line":12,"column":83},"end":{"line":12,"column":93}}}) : helper)))
    + "</a>\r\n            <span class=\"item-number\">"
    + alias4((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcItemNumber",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":13,"column":38},"end":{"line":13,"column":68}}}))
    + ": "
    + alias4(((helper = (helper = helpers.Articlenumber || (depth0 != null ? depth0.Articlenumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Articlenumber","hash":{},"data":data,"loc":{"start":{"line":13,"column":70},"end":{"line":13,"column":89}}}) : helper)))
    + "</span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.Comments : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":14,"column":12},"end":{"line":22,"column":19}}})) != null ? stack1 : "")
    + "        </div>\r\n    </div>\r\n    <div class=\"price\">\r\n        <span class=\"price-string\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded||(depth0 && depth0.isVatIncluded)||alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data,"loc":{"start":{"line":27,"column":18},"end":{"line":27,"column":33}}}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.program(14, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":27,"column":12},"end":{"line":31,"column":19}}})) != null ? stack1 : "")
    + "        </span>\r\n        <span class=\"separator\"> / </span>\r\n        <span class=\"qty-suffix\">"
    + alias4(((helper = (helper = helpers.QuantitySuffix || (depth0 != null ? depth0.QuantitySuffix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"QuantitySuffix","hash":{},"data":data,"loc":{"start":{"line":34,"column":33},"end":{"line":34,"column":53}}}) : helper)))
    + "</span>\r\n    </div>\r\n    <div class=\"qty\">\r\n        <span class=\"label\">"
    + alias4((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcQty",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":37,"column":28},"end":{"line":37,"column":51}}}))
    + ": </span>\r\n        <span class=\"value\">"
    + alias4(((helper = (helper = helpers.Quantity || (depth0 != null ? depth0.Quantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Quantity","hash":{},"data":data,"loc":{"start":{"line":38,"column":28},"end":{"line":38,"column":42}}}) : helper)))
    + "</span>\r\n    </div>\r\n    <div class=\"delete\">\r\n        <i class=\"fa fa-times-circle\"></i>\r\n    </div>\r\n    <div class=\"total\">\r\n        <span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded||(depth0 && depth0.isVatIncluded)||alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data,"loc":{"start":{"line":45,"column":18},"end":{"line":45,"column":33}}}),{"name":"if","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.program(18, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":45,"column":12},"end":{"line":49,"column":19}}})) != null ? stack1 : "")
    + "        </span>\r\n    </div>\r\n</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "product-added";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <a href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage||(depth0 && depth0.isStage)||alias2).call(alias1,{"name":"isStage","hash":{},"data":data,"loc":{"start":{"line":5,"column":23},"end":{"line":5,"column":32}}}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":17},"end":{"line":5,"column":47}}})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data,"loc":{"start":{"line":5,"column":47},"end":{"line":5,"column":64}}}) : helper)))
    + "\"><img src=\""
    + alias4(container.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.Images : depth0)) != null ? stack1["0"] : stack1)) != null ? stack1.Url : stack1), depth0))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data,"loc":{"start":{"line":5,"column":99},"end":{"line":5,"column":109}}}) : helper)))
    + "\"></a>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "/stage";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <a href=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.isStage||(depth0 && depth0.isStage)||alias2).call(alias1,{"name":"isStage","hash":{},"data":data,"loc":{"start":{"line":7,"column":23},"end":{"line":7,"column":32}}}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":17},"end":{"line":7,"column":47}}})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.ProductLink || (depth0 != null ? depth0.ProductLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ProductLink","hash":{},"data":data,"loc":{"start":{"line":7,"column":47},"end":{"line":7,"column":64}}}) : helper)))
    + "\"><img src=\"/m1/stage/images/responsive-base/missing-image.png\" alt=\""
    + alias4(((helper = (helper = helpers.Name || (depth0 != null ? depth0.Name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data,"loc":{"start":{"line":7,"column":133},"end":{"line":7,"column":143}}}) : helper)))
    + "\"></a>\r\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "            <div class=\"comments\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.Comments : depth0),{"name":"each","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":16,"column":16},"end":{"line":20,"column":25}}})) != null ? stack1 : "")
    + "            </div>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression;

  return "                    <div class=\"comment\">\r\n                        <span class='key'>"
    + alias3((helpers.unescape||(depth0 && depth0.unescape)||alias2).call(alias1,(depth0 != null ? depth0.Name : depth0),{"name":"unescape","hash":{},"data":data,"loc":{"start":{"line":18,"column":42},"end":{"line":18,"column":59}}}))
    + ":</span> <span class=\"value\">"
    + alias3(((helper = (helper = helpers.Comment || (depth0 != null ? depth0.Comment : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"Comment","hash":{},"data":data,"loc":{"start":{"line":18,"column":88},"end":{"line":18,"column":99}}}) : helper)))
    + "</span>\r\n                    </div>\r\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.ItemPriceSumText || (depth0 != null ? depth0.ItemPriceSumText : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ItemPriceSumText","hash":{},"data":data,"loc":{"start":{"line":28,"column":16},"end":{"line":28,"column":40}}}) : helper))) != null ? stack1 : "")
    + "\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.ItemPriceSumNoVatText || (depth0 != null ? depth0.ItemPriceSumNoVatText : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ItemPriceSumNoVatText","hash":{},"data":data,"loc":{"start":{"line":30,"column":16},"end":{"line":30,"column":45}}}) : helper))) != null ? stack1 : "")
    + "\r\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalPriceSumText || (depth0 != null ? depth0.TotalPriceSumText : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalPriceSumText","hash":{},"data":data,"loc":{"start":{"line":46,"column":16},"end":{"line":46,"column":41}}}) : helper))) != null ? stack1 : "")
    + "\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalPriceSumNoVatText || (depth0 != null ? depth0.TotalPriceSumNoVatText : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalPriceSumNoVatText","hash":{},"data":data,"loc":{"start":{"line":48,"column":16},"end":{"line":48,"column":46}}}) : helper))) != null ? stack1 : "")
    + "\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.ProductsCart : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":53,"column":9}}})) != null ? stack1 : "");
},"useData":true,"useDepths":true});

this["J"]["views"]["dynamic-cart/dynamic-cart-totals"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalProductSumText || (depth0 != null ? depth0.TotalProductSumText : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalProductSumText","hash":{},"data":data,"loc":{"start":{"line":6,"column":16},"end":{"line":6,"column":43}}}) : helper))) != null ? stack1 : "")
    + "\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                "
    + ((stack1 = ((helper = (helper = helpers.TotalProductSumNoVatText || (depth0 != null ? depth0.TotalProductSumNoVatText : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"TotalProductSumNoVatText","hash":{},"data":data,"loc":{"start":{"line":8,"column":16},"end":{"line":8,"column":48}}}) : helper))) != null ? stack1 : "")
    + "\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing;

  return "        <div class=\"vat-total\">\r\n            <span class=\"label\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded||(depth0 && depth0.isVatIncluded)||alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data,"loc":{"start":{"line":15,"column":18},"end":{"line":15,"column":33}}}),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data,"loc":{"start":{"line":15,"column":12},"end":{"line":19,"column":19}}})) != null ? stack1 : "")
    + "            </span>\r\n            <span class=\"value\">"
    + ((stack1 = ((helper = (helper = helpers.TotalProductVatSumText || (depth0 != null ? depth0.TotalProductVatSumText : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"TotalProductVatSumText","hash":{},"data":data,"loc":{"start":{"line":21,"column":32},"end":{"line":21,"column":62}}}) : helper))) != null ? stack1 : "")
    + "</span>\r\n        </div>\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.translate||(depth0 && depth0.translate)||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"dcOfWhichVat",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":16,"column":16},"end":{"line":16,"column":46}}}))
    + "\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.translate||(depth0 && depth0.translate)||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"dcPlusVat",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":18,"column":16},"end":{"line":18,"column":43}}}))
    + "\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing;

  return "<div id=\"dc-totals\">\r\n    <div class=\"sum-total\">\r\n        <span class=\"label\">"
    + container.escapeExpression((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcItemTotal",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":3,"column":28},"end":{"line":3,"column":57}}}))
    + ":</span>\r\n        <span class=\"value\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.isVatIncluded||(depth0 && depth0.isVatIncluded)||alias2).call(alias1,{"name":"isVatIncluded","hash":{},"data":data,"loc":{"start":{"line":5,"column":18},"end":{"line":5,"column":33}}}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":5,"column":12},"end":{"line":9,"column":19}}})) != null ? stack1 : "")
    + "        </span>\r\n    </div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showTotalVat : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":4},"end":{"line":23,"column":11}}})) != null ? stack1 : "")
    + "</div>\r\n";
},"useData":true});

this["J"]["views"]["dynamic-cart/dynamic-cart"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression;

  return "<div id='dc-wrapper' class='reveal-modal' data-reveal data-options=\"close_on_background_click:true\">\r\n    <div id=\"dc-inner\">\r\n        <h3 id=\"dc-header\">"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcCartHeader",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":3,"column":27},"end":{"line":3,"column":57}}}))
    + "</h3>\r\n        <div id=\"dc-body\">\r\n            <div id=\"dc-items-header\" class=\"clearfix\">\r\n                <div class=\"img\">&nbsp;</div>\r\n                <div class=\"item\">"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcItem",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":7,"column":34},"end":{"line":7,"column":58}}}))
    + "</div>\r\n                <div class=\"price\">"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcPrice",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":8,"column":35},"end":{"line":8,"column":60}}}))
    + "</div>\r\n                <div class=\"qty\">"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcQty",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":9,"column":33},"end":{"line":9,"column":56}}}))
    + "</div>\r\n                <div class=\"delete\">&nbsp;</div>\r\n                <div class=\"total\">"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcTotal",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":11,"column":35},"end":{"line":11,"column":60}}}))
    + "</div>\r\n            </div>\r\n            <div id=\"dc-content\">\r\n                <!-- Cart content goes here, populated from view dynamic-cart-item -->\r\n            </div>\r\n            <div id=\"dc-loader-msg\"><span class=\"add-prod\">"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcProdLoadMsg",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":16,"column":59},"end":{"line":16,"column":90}}}))
    + "</span></div>\r\n            <div id=\"dc-totals\">\r\n            </div>\r\n        </div>\r\n        <div id=\"dc-btns\">\r\n            <button id=\"dc-checkout-btn\" class=\"button\" href=\""
    + alias3((helpers.config||(depth0 && depth0.config)||alias2).call(alias1,"urls.CheckoutUrl",{"name":"config","hash":{},"data":data,"loc":{"start":{"line":21,"column":62},"end":{"line":21,"column":93}}}))
    + "\"><span>"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcCheckoutBtnText",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":21,"column":101},"end":{"line":21,"column":136}}}))
    + "</span></button>\r\n            <button id=\"dc-continue-btn\" class=\"button\" href=\"\"><span>"
    + alias3((helpers.translate||(depth0 && depth0.translate)||alias2).call(alias1,"dcContinueBtnText",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":22,"column":70},"end":{"line":22,"column":105}}}))
    + "</span></button>\r\n        </div>\r\n    </div>\r\n</div>\r\n";
},"useData":true});

this["J"]["views"]["cat-nav-mobile/list-pages"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <li class=\"\">\r\n        <a class=\"\" href=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data,"loc":{"start":{"line":4,"column":26},"end":{"line":4,"column":33}}}) : helper)))
    + "\">\r\n            <span>"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":5,"column":18},"end":{"line":5,"column":27}}}) : helper)))
    + "</span>\r\n        </a>\r\n    </li>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<ul id=\"list-pages\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":8,"column":13}}})) != null ? stack1 : "")
    + "</ul>";
},"useData":true});

this["J"]["views"]["instagram-feed/instagram-feed"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"instagram-feed-wrapper\">\r\n    <h2>"
    + container.escapeExpression((helpers.translate||(depth0 && depth0.translate)||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"instaFeedHeaderText",{"name":"translate","hash":{},"data":data,"loc":{"start":{"line":2,"column":8},"end":{"line":2,"column":43}}}))
    + "</h2>\r\n    <ul id=\"instafeed\"></ul>\r\n</div>\r\n";
},"useData":true});