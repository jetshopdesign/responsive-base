// Inserted by build-process, getSassBreakpoints()
J.config.breakpoints = {"small":640,"medium":1008,"large":1360,"xlarge":1600};

// 
//    Module: js-translations
//
var jsTranslations = {
    translations: [
      'AddingItemToCart',
      'Article',
      'Cart',
      'ContinueShopping',
      'FilterAllSelected',
      'FilterApplyFilter',
      'FilterBuyButton',
      'FilterClose',
      'FilterInfoButton',
      'FilterLoadMoreProducts',
      'FilterMaxPrice',
      'FilterMinPrice',
      'FilterMobileShowFilter',
      'FilterNoMatch',
      'FilterOk',
      'FilterReset',
      'FilterSearch',
      'FilterSelectAll',
      'FilterSelected',
      'FilterStockStatus',
      'FilterYes',
      'FilterYourChoices',
      'GettingCart',
      'IncludingVAT',
      'ItemNumber',
      'Menu',
      'OnlyAvailableInWarehouse',
      'OnlyAvailableInWebshop',
      'PlusVAT',
      'Price',
      'ProceedToCheckout',
      'Quantity',
      'ResponsiveMyPages_OrderCartRecreationItemErrorNotBuyable',
      'ResponsiveMyPages_OrderCartRecreationItemErrorOutOfStock',
      'Search',
      'Sort_ArticleNumber',
      'Sort_Bestseller',
      'Sort_Custom',
      'Sort_DateAdded',
      'Sort_Name',
      'Sort_Price',
      'Sort_SubName',
      'Total',
      'TotalItems',
      'ViewCart',
      'YourShoppingCart'
    ],
    init: function () {
        jsTranslations.add();
    },
    add: function () {
        $.each(jsTranslations.translations, function (index, translation) {
            var obj = {};
            var insideObj = {};
            insideObj[J.data.language] = JetshopData.Translations[translation];
            obj[translation] = insideObj;

            J.translations.push(obj);
        });
    }
};

J.pages.addToQueue("all-pages", jsTranslations.init);


// 
//    Module: checkout-validation
//
var checkoutValidation = {
    config: {
        errorDivOffset: 2
    },
    init: function () {
        // Only if responsive checkout
        if (typeof Checkout != "undefined") {
            if (typeof (Page_ClientValidate) != "undefined") {
                ValidatorUpdateDisplay = checkoutValidation.validate;
            }
        }
    },
    validate: function (val) {
        var that = $(val);
        if (val.isvalid) {
            that.hide();
        } else {
            that.fadeIn('fast');
            var labelHeight = that.outerHeight();
            var inputHeight = that.parent().find(".form-text").outerHeight();
            var setHeight = labelHeight + inputHeight + checkoutValidation.config.errorDivOffset;
            that.css("margin-top", -setHeight);
            that.parent().find(".form-text").addClass("active-error").focus(function () {
                checkoutValidation.clearError($(this));
            }).on("input", function () {
                checkoutValidation.clearError($(this));
            });
        }
    },
    clearError: function (that) {
        that.removeClass("active-error");
        that.parent().find(".customer-info-input-error").hide();
    }
};

J.pages.addToQueue("checkout-page", checkoutValidation.init);


// 
//    Module: dynamic-cart
//
var dynamicCart = {
    //TODO animation on delete item? Async, happening too fast?
    settings: {
        type: "modal", // "modal" or "dropdown"
        showTotalVat: true,
        topDistance: 100,
        topDistanceSmall: 30
    },

    lastProductAdded: 0, // Save the ID for last product added to cart, used for animation

    dcWrapper: false,

    render: function () {
        // Save the ID for last product added to cart, used for animation
        J.cart.lastProductAdded = dynamicCart.lastProductAdded;
        // Items
        var itemTemplate = J.views['dynamic-cart/dynamic-cart-item'];
        var itemHtml = itemTemplate(J.cart);
        $("#dc-content").html(itemHtml);
        // Totals
        var totalSumData = {
            TotalProductSumText: J.cart.TotalProductSumText,
            TotalProductSumNoVatText: J.cart.TotalProductSumNoVatText,
            TotalProductVatSumText: J.cart.TotalProductVatSumText,
            showTotalVat: dynamicCart.settings.showTotalVat
        };
        var totalsTemplate = J.views['dynamic-cart/dynamic-cart-totals'];
        var totalsHtml = totalsTemplate(totalSumData);
        $("#dc-totals").html(totalsHtml);
        dynamicCart.dcWrapper.removeClass("loading").removeClass("deleting");
        setTimeout(function(){
            $(".dc-item-row").removeClass("product-added"); // This class is added in view to highlight last product added
        }, 300);
    },

    open: function (isCartAddOpening) {
        if (isCartAddOpening) {
            dynamicCart.dcWrapper.addClass("loading");
            $("#dc-checkout-btn").addClass("disabled").prop("disabled", true);
        }
        if (dynamicCart.settings.type === "modal" || Foundation.utils.is_small_only()) {
            if (Foundation.utils.is_small_only()) {
                dynamicCart.dcWrapper.data().cssTop = dynamicCart.settings.topDistanceSmall;
            } else {
                dynamicCart.dcWrapper.data().cssTop = dynamicCart.settings.topDistance;
            }
            dynamicCart.dcWrapper.foundation("reveal", "open");
        }
        else if (dynamicCart.settings.type === "dropdown") {
            dynamicCart.dcWrapper.addClass("dropdown-open").find("#dc-inner").stop(true, true).slideDown("fast");
        }
    },

    close: function () {
        dynamicCart.dcWrapper.removeClass("loading");
        dynamicCart.dcWrapper.foundation("reveal", "close"); // Always close modal
        if (dynamicCart.settings.type === "dropdown") {
            dynamicCart.dcWrapper.removeClass("dropdown-open").find("#dc-inner").stop(true, true).slideUp("fast");
        }
        $(window).trigger("dynamic-cart-closed");
    },

    bind: function () {
        // Bind reveal closed to purge style attributes from cart wrapper
        $(document).on("closed.fndtn.reveal", '[data-reveal]', function () {
            var modal = $(this);
            modal.removeAttr("style");
        });
        // Bind button "To cart"
        $("#dc-checkout-btn").click(function(event){
            event.preventDefault();
            window.location.href = J.config.urls.CheckoutUrl;
        });
        // Bind cart click
        $(".small-cart-body").click(function () {
            if (dynamicCart.settings.type === "modal") {
                dynamicCart.open(false);
            }
            else if (dynamicCart.settings.type === "dropdown") {
                if (dynamicCart.dcWrapper.hasClass("dropdown-open")) {
                    dynamicCart.close();
                }
                else {
                    dynamicCart.open();
                }
            }
        });
        // Bind continue button
        $("#dc-continue-btn").click(function (event) {
            event.preventDefault();
            dynamicCart.close();
        });
        // Bind delete button
        dynamicCart.dcWrapper.on("click", ".delete i", function (event) {
            event.preventDefault();
            var clickedBtn = $(this);
            var prodRow = clickedBtn.closest(".dc-item-row");
            var prodRowHeight = prodRow.height();
            var recId = prodRow.attr("data-rec-id");
            prodRow.addClass("deleting").slideUp(300, "easeInOutCubic");
            dynamicCart.dcWrapper.addClass("deleting");
            dynamicCart.lastProductAdded = null;
            J.components.deleteFromCart(recId);
        });
    },

    init: function () {
        $(window).on('cart-updated', function () {
            $("#dc-checkout-btn").removeClass("disabled").prop("disabled", false);
        });
        J.translations.push(
            {
                dcProdLoadMsg: {
                    sv: "Lägger varan i kundvagnen...",
                    nb: "Legger vare i handlekurven...",
                    da: "Lægger varen i indkøbsvognen...",
                    fi: "Tuote lisätään ostoskoriin...",
                    de: "Artikel zum Warenkorb hinzufügen...",
                    en: "Adding item to cart...",
                    et: "Lisab kirje ostukorvi"
                },
                dcCartLoadMsg: {
                    sv: "Hämtar kundvagnen...",
                    nb: "Henter handlekurven...",
                    da: "Henter indkøbsvogn...",
                    fi: "Ostoskorille siirrytään...",
                    de: "Zum Warenkorb...",
                    en: "Getting cart...",
                    et: "Laadimine ostukorvi"
                },
                dcContinueBtnText: {
                    sv: "Fortsätt handla",
                    nb: "Handle mer",
                    da: "Fortsæt med at shoppe",
                    fi: "Jatka ostoksia",
                    de: "Mit dem Einkauf fortfahren",
                    en: "Continue shopping",
                    et: "Jätkan ostlemist"
                },
                dcCheckoutBtnText: {
                    sv: "Till kassan",
                    nb: "Gå til kassen",
                    da: "Gå til kassen",
                    fi: "Mene kassalle",
                    de: "Zur Kasse",
                    en: "Proceed to checkout",
                    et: "Kassasse"
                },
                dcCartHeader: {
                    sv: "Din kundvagn",
                    nb: "Din handlekurv",
                    da: "Din kundevogn",
                    fi: "Ostoskorisi",
                    de: "Ihr Warenkorb",
                    en: "Your shopping cart",
                    et: "Sinu ostukäru"
                },
                dcItem: {
                    sv: "Artikel",
                    nb: "Artikkel",
                    da: "Vare",
                    fi: "Tuote",
                    de: "Artikel",
                    en: "Article",
                    et: "Artikli"
                },
                dcPrice: {
                    sv: "Pris",
                    nb: "Pris",
                    da: "Pris",
                    fi: "Hinta",
                    de: "Preis",
                    en: "Price",
                    et: "Hind"
                },
                dcQty: {
                    sv: "Antal",
                    nb: "Antall",
                    da: "Antal",
                    fi: "Määrä",
                    de: "Anzahl",
                    en: "Qty",
                    et: "Kogus"
                },
                dcTotal: {
                    sv: "Summa",
                    nb: "Summer",
                    da: "I alt",
                    fi: "Summa",
                    de: "Summe",
                    en: "Total",
                    et: "Kokku"
                },
                dcItemTotal: {
                    sv: "Summa artiklar",
                    nb: "Summer artikler",
                    da: "Varer i alt",
                    fi: "Yhteensä tuotteita",
                    de: "Summe Artikel",
                    en: "Total items",
                    et: "Toodete hind kokku"
                },
                dcOfWhichVat: {
                    sv: "varav moms",
                    nb: "herav mva",
                    da: "heraf moms",
                    fi: "josta alv:n osuus",
                    de: "davon MwSt",
                    en: "of which VAT",
                    et: "sh k/m:"
                },
                dcPlusVat: {
                    sv: "moms tillkommer med",
                    nb: "+ moms",
                    da: "der tillægges moms på",
                    fi: "hinta sisältää alv:n",
                    de: "+ MwSt",
                    en: "+ VAT",
                    et: "+ KM"
                },
                dcItemNumber: {
                    sv: "Artikelnummer",
                    nb: "Artikkelnummer",
                    da: "Varenummer",
                    fi: "Tuotenumero",
                    de: "Artikelnummer",
                    en: "Item number",
                    et: "Artikli number"
                },
                dcViewCart: {
                    sv: "Se kundvagn",
                    nb: "Se handlekurv",
                    da: "Se indkøbsvogn",
                    fi: "Näytä ostoskori",
                    de: "Warenkorb anzeigen",
                    en: "View cart",
                    et: "ostukäru"
                }
            }
        );

        // Render cart container if not already rendered
        if (!$("#dc-wrapper").length) {
            var template = J.views['dynamic-cart/dynamic-cart'];
            var html = template(null);
            $(".cart-area-wrapper").append(html);
        }
        dynamicCart.dcWrapper = $("#dc-wrapper");

        // Set type as html class
        if (dynamicCart.settings.type === "modal") {
            $("html").addClass("dynamic-cart-modal");
        }
        else if (dynamicCart.settings.type === "dropdown") {
            $("html").addClass("dynamic-cart-dropdown");
        }

        // Bind
        dynamicCart.bind();

        // Hijack add to cart function
        var oldAddToCart = JetShop.StoreControls.Services.General.AddCartItem;
        JetShop.StoreControls.Services.General.AddCartItem = function () {
            dynamicCart.lastProductAdded = arguments[0].ProductID;
            dynamicCart.open(true);
            return oldAddToCart.apply(this, arguments);
        };

        J.switch.addToMediumUp(function () {
            if (dynamicCart.settings.type === "dropdown") {
                dynamicCart.close();
                dynamicCart.dcWrapper.removeAttr("style");
            }
        });
        J.switch.addToSmall(function () {
            if (dynamicCart.settings.type === "dropdown") {
                dynamicCart.close();
            }
        });

        $(window).on("cart-updated", function () {
            dynamicCart.render();
        });
    }
};

J.pages.addToQueue("all-pages", dynamicCart.init);


// 
//    Module: cat-nav-mega-dd
//
// INIT TOP DYNAMIC MENU -------------
var catNav = {
    initTopMenu: function () {

        J.translations.push(
            {
                seeAll: {
                    sv: "Se alla",
                    en: "See all",
                    da: "Se alle",
                    nb: "Se alle",
                    fi: "Katso kaikki"
                }
            }
        );

        var catNav = $("#cat-nav");
        var navBar = catNav.children(".nav-bar");
        // TOP MENU STYLE
        catNav.addClass("style-megadropdown");
        // MAKE SLIGHT DELAY ON HOVER
        var hoverAction;
        catNav.on("mouseenter", function () {
            clearTimeout(hoverAction);
            hoverAction = setTimeout(function () {
                catNav.addClass("hovered");
            }, 100);
        }).on("mouseleave", function () {
            clearTimeout(hoverAction);
            catNav.removeClass("hovered");
            catNav.checkTopMenuLayout();
        });

        // ADD SEE ALL LINK
        $("#cat-nav li.lv2").each(function () {
            var lv2Item = $(this);
            var lv2LinkHref = lv2Item.children("a").attr("href");
            var lv3List = lv2Item.children("ul.lv3");
            lv3List.append("<li class='see-all'><a href='" + lv2LinkHref + "'>" + J.translate("seeAll") + "</a></li>");
        });

        // ADD TOUCH SUPPORT FOR DESKTOP
        if (J.checker.isTouch && Foundation.utils.is_large_up()) {
            var expandingElements = $("#cat-nav li.lv1.has-subcategories > a");
            expandingElements.click(function (event) {
                var clickedNodeLink = $(this);
                event.preventDefault();
                if (!clickedNodeLink.parent().hasClass("clicked-once")) {
                    $("#cat-nav .clicked-once").not(clickedNodeLink.closest(".lv1")).removeClass("clicked-once");
                    $(this).parent().addClass("clicked-once");
                }
                else {
                    location.href = $(this).attr("href");
                }
            });
            $("body").on("click", function (event) {
                var target = $(event.target);
                if ($("#cat-nav .clicked-once").length && !target.closest("#cat-nav").length) {
                    $("#cat-nav .clicked-once").removeClass("clicked-once");
                }
            });
        }

        // STOP CLICK PROPAGATION FOR LINKS
        if (Foundation.utils.is_large_up()) {
            catNav.find('ul.category-navigation').on("click", "a", function (event) {
                event.stopPropagation();
            });
        }

        // TOP CAT MENU SPACE CALCULATION
        var lastWindowHeight = 0;
        catNav.checkTopMenuLayout = function () {
            if (Foundation.utils.is_large_up()) {
                var winScrollTop = $(window).scrollTop();
                var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
                var navBarTopOffset = navBar.offset().top;
                var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
                var viewportHeight = $(window).height();
                var currentTopOffset = 0; // OFFSET TO BE USED
                var viewportMaxCoverRatio = 0.9; // HOW MUCH OF VIEWPORT CAN BE COVERED BY MENU
                var windowEnlarged = false;
                // CHECK IF WINDOW WAS ENLARGED
                if (viewportHeight > lastWindowHeight) {
                    windowEnlarged = true;
                }
                lastWindowHeight = viewportHeight;
                // CHECK IF TOP MENU BAR SHOULD BE FIXED
                if (winScrollTop > (catNavTopOffset + navBarHeight)) { // IS SCROLLED
                    if (!catNav.is(".hovered")) {
                        catNav.height(navBarHeight);
                        $("html").addClass("menu-scrolled");
                        setTimeout(function () {
                            $("html").removeClass("menu-static");
                        }, 10);
                        currentTopOffset = navBarHeight;
                    }
                }
                else { // RETURN TO NORMAL
                    $("html").removeClass("menu-scrolled");
                    catNav.css("height", "auto");
                    setTimeout(function () {
                        $("html").addClass("menu-static");
                    }, 10);
                    currentTopOffset = catNavTopOffset;
                }
                var menuSpace = viewportHeight - currentTopOffset;
                if (winScrollTop <= currentTopOffset) {
                    menuSpace += winScrollTop;
                }
                else {
                    menuSpace += currentTopOffset;
                }
                // IF WINDOW WAS ENLARGED, SHOW ALL HIDDEN CATS
                if (windowEnlarged) {
                    catNav.find("li.lv3.hidden").removeClass("hidden");
                }
                // RESIZING FUNCTION
                catNav.resizeMegadropdownList = function (listElement) {
                    // START RESIZING ALL LV2 CATS IF NOT SPECIFIED
                    if (!listElement) {
                        listElement = $("#cat-nav ul.lv2");
                    }
                    catNav.addClass("resizing");
                    // CHECK EACH LV2 HEIGHT TO SEE IF IT FITS
                    listElement.each(function (index) {
                        var lv2List = $(this);
                        var lv2CatHeight = lv2List.height();
                        var largestLv3CatQty = 0;
                        if (lv2CatHeight > (menuSpace * viewportMaxCoverRatio)) {
                            //log("Cat menu too high - resizing...");
                            lv2List.find("ul.lv3").each(function () {
                                var lv3List = $(this);
                                var lv3CatQty = lv3List.find("li.lv3").not(".hidden").length;
                                if (lv3CatQty > largestLv3CatQty) {
                                    largestLv3CatQty = lv3CatQty;
                                }
                            });
                            // HIDE LAST 3 LV3 OF THE HIGHEST LV2 CATS
                            lv2List.find("ul.lv3").each(function () {
                                var lv3List = $(this);
                                var catQtyToHide = largestLv3CatQty - 3;
                                if (catQtyToHide >= 0) {
                                    lv3List.find("li.lv3:gt(" + catQtyToHide + ")").addClass("hidden");
                                }
                                else {
                                    lv3List.find("li.lv3").addClass("hidden");
                                }
                            });
                            // RE-CHECK HEIGHT FOR THIS CAT IF NOT ALREADY AT MINIMUM
                            if (lv2List.find("li.lv3").not(".hidden").length) {
                                setTimeout(function () {
                                    catNav.resizeMegadropdownList(lv2List);
                                }, 1);
                            }
                        }
                    });
                    catNav.removeClass("resizing");
                };
                if (!catNav.is(".hovered")) {
                    catNav.resizeMegadropdownList();
                }
            }
        };

        // CHECK MENU HEIGHT
        catNav.checkTopMenuLayout();

        // BIND DYNAMIC MENU SPACE CALC TO RESIZE AND SCROLL
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {
                // RESIZE DESKTOP MENU
                catNav.checkTopMenuLayout();
            }, 100);
        });

        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {

                // RESIZE DESKTOP MENU
                catNav.checkTopMenuLayout();
            }, 100);
        });

        // THIS RESETS MENU WHEN GOING FROM MEDIUM TO LARGE
        // FIRED FROM J.switch.toLargeUp
        catNav.resetMobileMenuStyles = function () {
            catNav.add(navBar).removeAttr("style");
        };

        // ADD CHECK FOR MOBILE STYLING WHEN IN LARGE-UP
        J.switch.addToLargeUp(catNav.resetMobileMenuStyles);
    }
};

J.pages.addToQueue("all-pages", catNav.initTopMenu);


// 
//    Module: cat-nav-mobile
//
// INIT MOBILE MENU -------------
var catNavMobile = {

    settings: {
        autoCloseInactiveMobileNodes: true,
        displayVatSelector: true,
        displayCurrencySelector: true,
        displayCultureSelector: true,
        displayCustomPages: true
    },

    checkMobileMenuLayout: function (isOpening) {
        var catNav = $("#cat-nav");
        if (!Foundation.utils.is_large_up()) {
            var winScrollTop = $(window).scrollTop();
            var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
            var navBar = catNav.children(".nav-bar");
            var navBarTopOffset = navBar.offset().top;
            var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
            var viewportHeight = $(window).height();
            var menuWrapper = $("#menu-wrapper");
            var menuHeight = menuWrapper.height();
            var menuTopOffset;
            // CHECK IF TOP MENU BAR SHOULD BE FIXED
            if (winScrollTop > (catNavTopOffset - menuHeight)) { // IS SCROLLED
                catNav.height(menuHeight);
                $("html").addClass("menu-scrolled");
                setTimeout(function () {
                    $("html").removeClass("menu-static");
                }, 10);
            }
            // RETURN TO NORMAL
            else {
                catNav.css("height", "auto");
                $("html").removeClass("menu-scrolled");
                setTimeout(function () {
                    $("html").addClass("menu-static");
                }, 10);
            }
            // SCROLL CAT NAVIGATION (TIMEOUT TO WAIT FOR DOM CHANGES)
            if ($("html.menu-open").length || isOpening) {
                setTimeout(function () {
                    // TAKE FIXED/NOT FIXED STATE INTO ACCOUNT
                    if ($("html.menu-scrolled").length) {
                        menuTopOffset = winScrollTop;
                    }
                    else {
                        menuTopOffset = menuWrapper.offset().top;
                    }
                    // IF LEFT AREA TOP BELOW CURRENT SCROLL POSITION - MOVE UP
                    if (navBarTopOffset > (menuTopOffset + menuHeight)) {
                        navBar.css("top", (menuTopOffset + menuHeight + 10) + "px");
                    }
                    // IF LEFT AREA TOP ABOVE SCROLL POSITION AND THERE IS SPACE ENOUGH TO FIT IT, MOVE DOWN
                    else if ((viewportHeight - menuHeight) > navBarHeight) {
                        navBar.css("top", (menuTopOffset + menuHeight + 10) + "px");
                    }
                    // IF END OF LONG LEFT AREA ABOVE BOTTOM OF PAGE - MOVE DOWN
                    else {
                        var posDiff = (winScrollTop + viewportHeight) - (navBarTopOffset + navBarHeight);
                        if (posDiff > 150) {
                            navBar.css("top", (navBarTopOffset + posDiff - 10) + "px");
                        }
                    }
                }, 5);
            }
        }
    },

    displayCustomPages: function () {
        if (catNavMobile.settings.displayCustomPages) {
            var customPagesList = [];
            $(".page-list-wrapper").find("a").each(function (key, value) {
                var item = {
                    url: $(this).attr("href"),
                    title: $(this).text()
                };
                customPagesList.push(item);
            });
            if (customPagesList.length > 0){
                var template = J.views['cat-nav-mobile/list-pages'];
                var html = template(customPagesList);
                $("ul#category-navigation").after("<div id='pages-list-placeholder'></div>");
                $("#pages-list-placeholder").append(html);
            }
        }
    },

    displayVatSelector: function () {
        if (catNavMobile.settings.displayVatSelector) {
            var origVatSelector = $(".vat-selector-wrapper");
            var origVatSelectorID = origVatSelector.find("input").attr("id");
            var vatSelector = origVatSelector.clone();
            vatSelector.find("input").attr("id", "").attr("name", "").attr("onclick", "").on("click", function () {
                $("#"+origVatSelectorID).click();
            });
            $("ul#category-navigation").after(vatSelector);
        }
    },

    displayCurrencySelector: function () {
        if (catNavMobile.settings.displayCurrencySelector) {
            var currencySelector = $(".currency-selector-wrapper").clone();
            $("ul#category-navigation").after(currencySelector);
        }
    },

    displayCultureSelector: function () {
        if (catNavMobile.settings.displayCultureSelector) {
            var cultureSelector = $(".culture-selector-wrapper").clone();
            $("ul#category-navigation").after(cultureSelector);
        }
    },

    init: function () {
        var catNav = $("#cat-nav");
        // EXPANDING CLICKS IN MEDIUM-DOWN
        catNav.on("click", "ul, li", function (event) {
            if (!Foundation.utils.is_large_up()) {
                event.stopPropagation();
                var clickedNode = $(this);
                if (clickedNode.is("li.has-subcategories")) {
                    clickedNode.toggleClass("open");
                    if (catNavMobile.settings.autoCloseInactiveMobileNodes) {
                        // CLOSE ALL OPEN NODES NOT DIRECT PARENT OF CLICKED NODE
                        catNav.find(".open").not(clickedNode).not(clickedNode.parentsUntil("#cat-nav")).removeClass("open");
                    }
                    else {
                        // ONLY CLOSE OPEN NODES THAT ARE DIRECT DESCENDANTS OF CLOSED NODE
                        clickedNode.find(".open").removeClass("open");
                    }
                }
            }
        });
        // STOP CLICK PROPAGATION FOR LINKS
        catNav.find('ul.category-navigation').on("click", "a", function (event) {
            event.stopPropagation();
        });

        catNavMobile.displayVatSelector();
        catNavMobile.displayCurrencySelector();
        catNavMobile.displayCultureSelector();
        catNavMobile.displayCustomPages();

        // BIND DYNAMIC MENU SPACE CALC TO RESIZE AND SCROLL
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {
                catNavMobile.checkMobileMenuLayout();
            }, 100);
        });
        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {
                catNavMobile.checkMobileMenuLayout();
            }, 100);
        });
    }
};

J.pages.addToQueue("all-pages", catNavMobile.init);





// 
//    Module: mobile-menu
//
var mobileMenu = {
    wrapper: "#menu-content",
    menuOverlay: "#menu-overlay",
    searchElement: "#search-box-wrapper",
    cartElement: ".cart-area-wrapper",
    menuActivator: "#menu-activator",
    searchActivator: "#search-activator",
    cartActivator: "#cart-activator",
    init: function () {
        J.translations.push(
            {
                search: {
                    sv: "Sök",
                    en: "Search",
                    da: "Søg",
                    nb: "Søke",
                    fi: "Haku"
                }
            },
            {
                menu: {
                    sv: "Meny",
                    en: "Menu",
                    da: "Menu",
                    nb: "Meny",
                    fi: "Valikko"
                }
            }
        );
        // DOM Manipulation
        if (J.data.jetshopData && J.checker.isLoggedIn) {
            $("a.mypages-text").removeClass("button").appendTo(".page-list-wrapper");
        } else {
            $("a.mypages-text").hide();
        }
        $("#header a.login-text").removeClass("button").appendTo(".page-list-wrapper");
        $(".small-cart-body").prepend("<span class='title'>" + J.translate("cart") + "</span>");
        // Bind functions
        $(mobileMenu.cartActivator + " span").html(J.translate("cart"));
        $(mobileMenu.searchActivator + " span").html(J.translate("search"));
        $(mobileMenu.menuActivator + " span").html(J.translate("menu"));
        $(mobileMenu.menuActivator).add(mobileMenu.menuOverlay).on("click", function () {
            if (!$("html.menu-open").length) {
                $("html").removeClass("cart-open").removeClass("search-open");
                if (typeof catNavMobile.checkMobileMenuLayout !== "undefined") {
                    catNavMobile.checkMobileMenuLayout(true);
                }
            }
            setTimeout(function () {
                $("html").toggleClass("menu-open");
            }, 30);
        });
        $(mobileMenu.searchActivator).on("click", function () {
            $("html").toggleClass("search-open");
            mobileMenu.measureWidth();
            if ($("html.search-open").length) {
                $("html").removeClass("cart-open").removeClass("menu-open");
                $(".search-box-input > input").focus();
            }
        });
        $(mobileMenu.cartActivator).on("click", function () {
            $("html").toggleClass("cart-open");
            if ($("html.cart-open").length) {
                $("html").removeClass("menu-open").removeClass("search-open");
            }
        });
        J.switch.addToLargeUp(mobileMenu.closeAll);
        if (typeof catNavMobile === "undefined") {
            console.error("Module 'mobile-menu' need module 'cat-nav-mobile'");
        } else {
            catNavMobile.checkMobileMenuLayout();
        }
    },
    closeAll: function () {
        $("html").removeClass("menu-open").removeClass("search-open").removeClass("cart-open");
    },
    measureWidth: function () {
        if (J.deviceWidth === "small" || J.deviceWidth === "medium") {
            if ($("html.search-open").length) {
                var menuWrapperWidth = ($("#menu-content").outerWidth(true));
                var menuIconWidth = $("#menu-activator").outerWidth(true);
                var cartIconWidth = $(".cart-area-wrapper").outerWidth(true);
                var searchIconWidth = $("#search-activator").outerWidth(true);
                var searchWrapperWidth = menuWrapperWidth - menuIconWidth - cartIconWidth - 1;
                var searchInputWidth = searchWrapperWidth - searchIconWidth - 16; //TODO Why these numbers?
                $("#search-box-wrapper").width(searchWrapperWidth);
                $(".search-box-wrapper").width(searchInputWidth);
            } else {
                $("#search-box-wrapper").width("auto");
                $(".search-box-wrapper").width(0);
            }
        } else {
            $("#search-box-wrapper").width("auto");
            $(".search-box-wrapper").width("auto");
        }
    }
};

J.pages.addToQueue("all-pages", mobileMenu.init);

$(window).resize(function () {
    mobileMenu.measureWidth();
});


// 
//    Module: breadcrumbs
//
var breadcrumb = {
    init: function () {
        if (!J.checker.isStage && !J.checker.isCheckoutPage){
            J.api.startpage.list(breadcrumb.render, null, true, 30);
        }
    },
    render: function (data) {
        if (data.length) {
            for (var key in data){
                if (data[key]["IsActive"]){
                    var pageName = data[key]["PageName"];
                    $("#path-nav").prepend('<a class="breadcrumb-link" href="' + J.config.urls.CountryRootUrl + '">'+ pageName +'</a><span class="breadcrumb-spacer"> &gt; </span>');
                }
            }
        }
    }
};

J.pages.addToQueue("all-pages", breadcrumb.init);


// 
//    Module: instagram-feed
//
var instagramFeed = {
  settings: {
    channel1: {
      userId: '4399522518',
      accessToken: '4399522518.1a62238.2271110925eb431baf58ceba499873ce',
      clientId: '1a622385baed4130bfbdf8194c734ccd',
      instaFeedTranslations: {
        instaFeedHeaderText: {
          sv: "Följ oss på Instagram - @halsokraft",
          en: "Follow us on Instagram - @halsokraft"
        }
      }
    },
    channel2: {
      userId: '4399522518',
      accessToken: '4399522518.1a62238.e0a9ed3ac79544c1a7b05d29981c828b',
      clientId: '1a622385baed4130bfbdf8194c734ccd',
      instaFeedTranslations: {
        instaFeedHeaderText: {
          sv: "Följ oss på Instagram - @halsokraft",
          en: "Follow us on Instagram - @halsokraft"
        }
      }
    },
    postCount: '4',
    imageSize: 'standard_resolution',
  },
  variables: {},
  elements: {},
  init: function () {

    if (J.data.channelInfo.Active == 1) {
      J.translations.push(instagramFeed.settings["channel" + J.data.channelInfo.Active].instaFeedTranslations);
      $("#startpage_list").append(J.views['instagram-feed/instagram-feed']);
      instagramFeed.initInstafeed();
    }
  },
  initInstafeed: function () {

    // var pageId = '17841404395549940';
    // var accessToken = 'EAAJYiZBFXOaYBAGLZCA2PJbcwM9OC82NMFzidwZAp5ZAMgJSQdiEgK0p2wDryArHe1154jxDvNYD5ZCcpluzXRS0H5ihVzl2sfwzSqqEgW4q7GrprZBwynoDH3ZCKbuuyaRG9n6YM93zPYK2mqNHiCWpY1rV8zD6qoQaaG55ZA8WUQZDZD';

    var pageId = '17841438878851888';
    var accessToken = 'EAANBWw32k4ABALxEfvoKI1qr7xxKjMGqzxGNjf0AY7JdOBKlUbUFnSCpznZASEMmsFFdlMt1Y6Mgxf0O2ZBtNnn5ZAGcHMDHNz5mjQ5cin8NLSD2ttChcjnoAtLIvd1B70lSz4WOTjFPb102WuZBwVMQvKEbUMaaamefAhrfKwZDZD';

    var url = 'https://graph.facebook.com/' + pageId + '/media?limit=4&fields=thumbnail_url,media_url,permalink,like_count&access_token=' + accessToken;
    if ($('.instagram-feed-wrapper').length > 0) {
      $.get(url, function (response) {
        // console.log(response);
        $.each(response.data, function (key, value) {
          var imgUrl = value.thumbnail_url ? value.thumbnail_url : value.media_url;

          $('<li><a href="' + value.permalink + '" target="_blank" style="background-image: url(' + imgUrl + ')"> <span class="insta-likes"><i class="fa fa-heart" aria-hidden="true"></i>' + value.like_count + '</span></a></li>').appendTo('#instafeed');
        });
      });
    }

  }
};
J.pages.addToQueue("start-page", instagramFeed.init);




