"use strict"

var dynamicFilterGuiTop = {
  config: {
    enableXhr: true,
    productsPerPage: 20,
    useBackClickMemory: true,
    listFilters: {
      isClickAwayOk: true, // sets whether click outside will trigger Ok or Cancel (default is cancel).
      fullScreenFiltersWidth: 500, // Minimum screen width of device below which the options list is rendered in floating popup fashion.
    },
    productTemplates: {
      wrapper: {
        html: '<div class="dynamic-product-content" />',
        selector: 'div.dynamic-product-content'
      },
      productWrapper: {
        selector: 'ul.product-list',
        html: '<ul class="product-list" />'
      },
      placement: 'ul.product-list, span.filter-no-results',
      placementAdvanced: 'div.product-advanced-wrapper',
    },
    templates: {
      default: {
        // After which elm do you want the filter so show?
        placement: 'div.category-header-wrapper',
        // What should we wrap the filter around, Must be entered with selector and html
        wrapper: {
          html: '<div class="filter-wrapper" />',
          selector: 'div.filter-wrapper'
        },
        // Needs to have default key, exceptions is listed as own keys and is optional
        // This setting uses J.switch.width to define which template to use, if not listed here default will be used
        template: {
          default: 'dynamic-filter-gui-top/dynamic-filter-options-desktop',
          /*
           Uncomment this if you want to have different template for mobile devices

           small: 'dynamic-filter-gui-top/dynamic-filter-options-mobile',
           medium: 'dynamic-filter-gui-top/dynamic-filter-options-mobile'
           */
        }
      }
    }
  },
  data: {
    filterOptions: undefined,
    spanFilterElement: undefined,
    mobileMenuOpened: false,
    productPageToFetch: 1
  },
  init: function () {
    J.translations.push({
      filterHeading: {
        sv: 'Filter',
        nb: 'Filter',
        da: 'Filter',
        en: 'Filters',
        fi: 'Suodin'
      }
    });
    $(document).on('dynamicFilter:core::initialized', dynamicFilterGuiTop.bindEvents);

    if (dynamicFilterGuiTop.config.enableXhr) {
      $(document).on('dynamicFilter:core::initialized', dynamicFilterGuiTop.renderDynamicFilterObjects);
    }

    $(document).trigger('dynamicFilter:core::initialize');
  },
  bindEvents: function () {
    $(document)
      .on('dynamicFilter:filterOptions::success', dynamicFilterGuiTop.renderFilterOptions)
      .on('dynamicFilter:filterOptions::updated', dynamicFilterGuiTop.watchers.filterOptionsUpdated)
      .on('dynamicFilter:filterOptions::removed', dynamicFilterGuiTop.watchers.filterOptionsRemoved)
      .on('dynamicFilter:products::started', dynamicFilterGuiTop.watchers.productsStarted)
      .on('dynamicFilter:products::success', dynamicFilterGuiTop.watchers.productsSuccess)
      .on('dynamicFilter:products::error', dynamicFilterGuiTop.watchers.productsError)
      .on('dynamicFilter:products::completed', dynamicFilterGuiTop.watchers.productsCompleted)
      .on('dynamicFilter:products::initialize', dynamicFilterGuiTop.watchers.initializeDynamicFilterProducts)
      .on('dynamicFilter:products::reset', dynamicFilterGuiTop.watchers.resetProducts)
      .on('dynamicFilter:sortOrder::updated', dynamicFilterGuiTop.watchers.updatedSortOrder)
      .on('dynamicFilter:sortOrder::reseted', dynamicFilterGuiTop.watchers.updatedSortOrder)

    // We start with triggering fetchFilterOptions
    $(document).trigger('dynamicFilter:filterOptions::fetch');

    if (dynamicFilterGuiTop.config.enableXhr) {
      $(document).trigger('dynamicFilter:products::initialize');
    }
  },
  watchers: {
    filterOptionsUpdated: function () {
      // TODO: Talk to kenneth about this problems with mobile / aggregate filter and so on.
      if (dynamicFilterGuiTop.helpers.isMobile()) {
        $(document).trigger('dynamicFilter:filterOptions::fetch');
        if (!dynamicFilterGuiTop.data.mobileMenuOpened || dynamicFilterGuiTop.config.enableXhr) {
          $(document).trigger('dynamicFilter:products::reset');
        }
      } else if (dynamicFilterGuiTop.config.enableXhr) {
        $(document).trigger('dynamicFilter:filterOptions::fetch');
        $(document).trigger('dynamicFilter:products::reset');
      } else {
        dynamicFilterGuiTop.triggers.fetchProducts();
      }
    },
    resetProducts: function () {
      var $placement = $(dynamicFilterGuiTop.config.productTemplates.placement);
      $placement.html('');

      dynamicFilterGuiTop.data.productPageToFetch = 1;
      dynamicFilterGuiTop.watchers.dynamicFilterProductsInitialized()
    },
    initializeDynamicFilterProducts: function () {
      var $wrapper = $(dynamicFilterGuiTop.config.productTemplates.wrapper.selector);

      if (!$wrapper.exists()) {
        $wrapper = $(dynamicFilterGuiTop.config.productTemplates.wrapper.html)
        if (J.pageType === "category-advanced-page") {
          $wrapper.insertBefore(dynamicFilterGuiTop.config.productTemplates.placementAdvanced + ':eq(0)');
        } else {
          $wrapper.insertAfter(dynamicFilterGuiTop.config.productTemplates.placement);
        }
      }

      var $productWrapper = $(dynamicFilterGuiTop.config.productTemplates.productWrapper.selector);

      if ($productWrapper.length < 1) {
        $productWrapper = $(dynamicFilterGuiTop.config.productTemplates.productWrapper.html);
        $productWrapper.appendTo($wrapper);
        $productWrapper.closest('section.category-page-wrapper').addClass('dynamic-filter-products');
        $(dynamicFilterGuiTop.config.productTemplates.placementAdvanced).remove();
      } else {
        // Use the default product-list. Has to be cleared if that's not initilalized before.
        if (!$productWrapper.closest('section.category-page-wrapper').hasClass('dynamic-filter-products')) {
          // Clear the template if their's not dynamic filtered products.
          $productWrapper.html('');
          $productWrapper.closest('section.category-page-wrapper').addClass('dynamic-filter-products');
        }
      }

      dynamicFilterGuiTop.watchers.dynamicFilterProductsInitialized()
    },
    dynamicFilterProductsInitialized: function () {
      var options = {
        xhr: dynamicFilterGuiTop.config.enableXhr,
        productsPerPage: dynamicFilterGuiTop.config.productsPerPage,
        page: dynamicFilterGuiTop.data.productPageToFetch
      }

      // IF BACK CLICK - USE STORED OPTIONS
      if (dynamicFilterGuiTop.backClickMemory.isBackClick) {
        options = dynamicFilterGuiTop.backClickMemory.backClickLoadOptions;
      }

      $(document).trigger('dynamicFilter:products::fetch', options);
    },
    productsStarted: function (event) {
      dynamicFilterGuiTop.helpers.showSpinner();
    },
    productsError: function (event, error) {
      console.log('DF::GUI', error)
    },
    productsSuccess: function (event, products) {
      dynamicFilterGuiTop.renderProducts(products);
    },
    productsCompleted: function (event) {
      dynamicFilterGuiTop.helpers.hideSpinner();
    },
    updatedSortOrder: function (event) {
      if (dynamicFilterGuiTop.config.enableXhr) {
        $(document).trigger('dynamicFilter:products::reset');
      } else {
        dynamicFilterGuiTop.triggers.fetchProducts();
      }
    }
  },
  triggers: {
    clearFilters: function (event) {
      $(document).trigger('dynamicFilter:filterOptions::clear');
    },
    removeFilterOption: function (array) {
      $(document).trigger('dynamicFilter:filterOptions::remove', [array]);
    },
    updateFilterOptions: function (array) {
      $(document).trigger('dynamicFilter:filterOptions::update', [array])
    },
    allFiltersApplied: function (event) {
      if (dynamicFilterGuiTop.config.enableXhr) {
        dynamicFilterGuiTop.helpers.hideFilter(event);
      } else {
        var options = {
          xhr: dynamicFilterGuiTop.config.enableXhr,
          productsPerPage: dynamicFilterGuiTop.config.productsPerPage,
          page: dynamicFilterGuiTop.data.productPageToFetch
        }
        $(document).trigger('dynamicFilter:products::fetch', options)
      }
    },
    fetchProducts: function () {
      var options = {
        xhr: dynamicFilterGuiTop.config.enableXhr,
        productsPerPage: dynamicFilterGuiTop.config.productsPerPage,
        page: dynamicFilterGuiTop.data.productPageToFetch
      }
      $(document).trigger('dynamicFilter:products::fetch', options)
    }
  },
  renderDynamicFilterObjects: function () {
    var template = J.views['dynamic-filter-gui-top/dynamic-filter-spinner'];
    var html = template();
    $('body').append(html);
  },
  events: {
    removeFilterOption: function () {
      var element = $(this);

      var object = {
        key: element.data('key'),
        value: [element.data('value')]
      }
      switch (element.data('type')) {
        case 'multiLevelList':
          object.type = 'MultiLevelListValues'
          object.key = 'ALL' // This is a single label, remove all selected
          break;
        case 'list':
          object.type = 'ListValues';
          break;
        case 'bool':
          object.type = 'BoolValues';
          break;
        case 'span':
          object.type = 'SpanValues';
          object.key = element.data('name')
          break;
      }

      var array = new Array();
      array.push(object);
      dynamicFilterGuiTop.triggers.removeFilterOption(array)
    }
  },
  renderProducts: function (data) {

    // get placement and create an jQuery obj
    var $productWrapper = $(dynamicFilterGuiTop.config.productTemplates.productWrapper.selector);
    var $wrapper = dynamicFilterGuiTop.helpers.getElement(dynamicFilterGuiTop.config.productTemplates.wrapper);

    // CHANGE ALL URLS TO HTTPS IF ON HTTPS PAGE
    // IF NOT, DOCUMENT.REFERRER (BACK CLICK MEMORY) WILL NOT WORK
    // (THIS IS SUPPOSEDLY FIXED IN BACKEND NOW, LEAVING IT IN FOR THE TIME BEING)
    //if(location.protocol == "https:") {
    //    if(data.ProductItems.length && data.ProductItems[0].ProductUrl.match("http:")){
    //        $.each(data.ProductItems, function(index, prodData) {
    //            prodData.ProductUrl = prodData.ProductUrl.replace("http:", "https:");
    //        });
    //    }
    //}


    var productItemTemplate = J.views['dynamic-filter-gui-top/dynamic-filter-product-items'];
    if (J.pageType === "category-advanced-page") {
      productItemTemplate = J.views['dynamic-filter-gui-top/dynamic-filter-product-items-advanced'];
    }
    data.Language = JetshopData.Language;
    var productsHtml = productItemTemplate(data)
    $productWrapper.append(productsHtml);

    // Set the match height of the product-name and product-list-description when products are appended.
    if (J.pageType !== "category-advanced-page") {
      $productWrapper.find('.product-name').matchHeight(J.config.sameHeightConfig);
      $productWrapper.find('.product-list-description').matchHeight(J.config.sameHeightConfig);
    }

    var actionWrapper = $('div.dynamic-filter-product-actions');
    if (actionWrapper.exists()) {
      actionWrapper.remove();
    }

    // Create action wrapper with load more products etc.
    var totalPages = data.TotalPages;

    // Detect if was back-click memory load
    if (dynamicFilterGuiTop.config.useBackClickMemory && dynamicFilterGuiTop.backClickMemory.isBackClick) {
      totalPages = Math.ceil(data.TotalProducts / dynamicFilterGuiTop.config.productsPerPage);
    }

    var actionObject = {
      pageToFetch: dynamicFilterGuiTop.data.productPageToFetch + 1,
      totalPages: totalPages
    };
    var template = J.views['dynamic-filter-gui-top/dynamic-filter-product-actions'];

    var html = template(actionObject);
    $wrapper.append(html);

    // Bind events to action buttons
    dynamicFilterGuiTop.bindFilterProductActions();

    // Back click memory function - scroll to position and disable
    if (dynamicFilterGuiTop.config.useBackClickMemory && dynamicFilterGuiTop.backClickMemory.isBackClick) {
      dynamicFilterGuiTop.backClickMemory.isBackClick = false;
      setTimeout(function () {
        dynamicFilterGuiTop.backClickMemory.scrollToPosition();
      }, 100);
    }
  },
  renderFilterOptions: function (event, filterOptions) {
    dynamicFilterGuiTop.data.filterOptions = filterOptions;

    if (filterOptions.length === 0) {
      return;
    }

    var sortedData = {
      default: []
    }

    $.each(dynamicFilterGuiTop.data.filterOptions, function (key, val) {
      // Sort and store after configured types, type not configured - store it as default
      if (dynamicFilterGuiTop.config.templates[val.Type] === undefined) {
        // Filter Type not found in config - store it as default
        sortedData.default.push(val);
      } else if (dynamicFilterGuiTop.config.templates[val.Type].hideFor !== undefined && dynamicFilterGuiTop.config.templates[val.Type].hideFor.indexOf(J.switch.width) > -1) {
        sortedData.default.push(val);
        return;
      } else {

        if (sortedData[val.Type] === undefined) {
          sortedData[val.Type] = [];
        }
        sortedData[val.Type].push(val)
      }
    })

    // Loop config and merge data with templates and place it in the DOM
    var $wrapper = false;
    $.each(dynamicFilterGuiTop.config.templates, function (type, setting) {

      if (sortedData[type] === undefined || sortedData[type] < 1) {
        log('no filter data for type ' + type, 1);
        return;
      }

      // Fetch wrapper for current type
      $wrapper = dynamicFilterGuiTop.helpers.getElement(setting.wrapper);

      if (!$wrapper) {
        // No wrapper, something went really wrong here
        log('no wrapper for type ' + type, 4)
        return;
      }

      // Fetch template for current type
      var $template = dynamicFilterGuiTop.helpers.getTemplate(setting.template);

      if (!$template) {
        // No template, no go then!
        log('no template for type ' + type, 2);
        return;
      }

      // get placement and create an jQuery obj
      var $placement = $(setting.placement);

      if ($placement.length < 1) {
        // Didn't find placement?!
        log('no placeholder for wrapper in type ' + type, 2)
        return;
      }

      // Do the magic here, insert wrapper after placement
      if ($(setting.wrapper.selector).length == 0) {
        $wrapper.insertAfter($placement);
      }

      var templateData = {
        FilterOptions: sortedData[type],
        SelectedFilter: dynamicFilterGuiTop.data.filterOptions
      };

      // Render the template with data
      $wrapper.html($template(templateData));
      // Handling for selected multilevel filter selected value - remove if empty
      if ($wrapper.find('div.filter-choices-multilevel-selected > span').length == 0) {
        $wrapper.find('span.filter-choice-multilevel-items').remove();
      }
    })

    // If only one filter choices item exists then remove the wrapper
    var $filterChoices = $wrapper.find('div.filter-choices');
    if ($filterChoices.find('span.filter-choices-item').length === 1) {
      $filterChoices.remove();
    } else { // Bind events if there exists more than one.
      dynamicFilterGuiTop.bindEventsForSelectedOptions();
    }

    dynamicFilterGuiTop.bindFilters();
    dynamicFilterGuiTop.bindMobileEvents($wrapper);
  },
  bindFilterProductActions: function () {
    $('button.action--dynamicFilter-fetch-products').on('click', function () {
      dynamicFilterGuiTop.data.productPageToFetch = dynamicFilterGuiTop.data.productPageToFetch + 1;
      dynamicFilterGuiTop.triggers.fetchProducts();
    });
  },
  bindFilters: function () {
    $('div.filter-option-element').each(function () {
      var type = $(this).data('type');
      switch (type) {
        case "multiLevelList":
          dynamicFilterGuiTop.bindMultiLevelListFilters($(this))
          break;
        case "list":
          dynamicFilterGuiTop.bindListFilters($(this))
          break;
        case "bool":
          dynamicFilterGuiTop.bindBoolFilters($(this))
          break;
        case "span":
          dynamicFilterGuiTop.bindSpanFilters($(this))
          break;
      }
    });
  },
  bindEventsForSelectedOptions: function () {
    $('span.action-reset-filter').on('click', dynamicFilterGuiTop.triggers.clearFilters)
    $('span.action--remove-filter-choice').on('click', dynamicFilterGuiTop.events.removeFilterOption)
  },
  bindMobileEvents: function (wrapper) {
    wrapper.find('button.action--reset-filter').on('click', dynamicFilterGuiTop.triggers.clearFilters);
    wrapper.find('button.action--show-filter').on('click', dynamicFilterGuiTop.helpers.showFilter);
    wrapper.find('button.action--apply-filter').on('click', dynamicFilterGuiTop.triggers.allFiltersApplied);
  },
  bindCustomFilterEvents: function (element) {
    element.find('div.filter-option-box-button').on('click', function () {
      element.toggleClass('open');
      if (dynamicFilterGuiTop.helpers.isMobile()) {
        $('html').toggleClass('stop-scroll');
      }
    });

    element.find('button.btnClose').on('click', function () {
      element.removeClass('open');
      if (dynamicFilterGuiTop.helpers.isMobile()) {
        $('html').removeClass('stop-scroll');
      }
    });

    // hide options on click outside.
    $(document).on('click', function (e) {
      if (!element.is(e.target)                  // if the target of the click isn't the container...
        && element.has(e.target).length === 0) { // ... nor a descendant of the container
        if (element.hasClass('open')) {
          element.removeClass('open');
        }
      }
    });
  },
  bindSpanFilters: function (element) {
    dynamicFilterGuiTop.bindCustomFilterEvents(element);

    var inputElement = element.find('.filter-option-box-content-input > div');

    // Get the right filter option
    var spanFilterOption = $.grep(dynamicFilterGuiTop.data.filterOptions, function (filterOption) {
      return filterOption.Type == "span"
    })[0];

    // Create object to use
    var spanValues = {
      min: undefined,
      max: undefined,
      minStart: undefined,
      maxStart: undefined,
    }

    // Map filteroption Values to the spanValues object
    $.each(spanFilterOption.Values, function (i, value) {
      if (value.Name === "pricemin") {
        spanValues.min = parseInt(value.Value);
        spanValues.minStart = value.SelectedValue === null ? parseInt(value.Value) : parseInt(value.SelectedValue);
      } else if (value.Name === "pricemax") {
        spanValues.max = parseInt(value.Value);
        spanValues.maxStart = value.SelectedValue === null ? parseInt(value.Value) : parseInt(value.SelectedValue);
      }
    });

    // JS will give you a hard time is you try to init noUiSlider twice
    dynamicFilterGuiTop.data.spanFilterElement = noUiSlider.create(inputElement[0], {
      start: [spanValues.minStart, spanValues.maxStart],
      connect: true,
      // TODO: Modify this to take care of the currency and culture aswell!
      tooltips: [wNumb({decimals: 0, thousand: '.'}), wNumb({decimals: 0, thousand: '.'})],
      range: {
        'min': [spanValues.min],
        'max': [spanValues.max]
      }
    });

    if (spanValues.min === spanValues.max) {
      inputElement.attr('disabled', ' disabled');
    }

    element.find('button.btnOk').on('click', function () {
      // Get values frmo the nouislider
      var priceMin = Math.round(dynamicFilterGuiTop.data.spanFilterElement.get()[0]);
      var priceMax = Math.round(dynamicFilterGuiTop.data.spanFilterElement.get()[1]);

      if (priceMin === spanValues.minStart && priceMax === spanValues.maxStart) {
        element.removeClass('open');
        return true
      }

      // Create the array and populate the data to the array.
      var array = new Array();

      // Create the priceMinObject
      if (priceMin !== spanValues.minStart) {
        var priceMinObj = {
          type: 'SpanValues',
          key: 'pricemin',
          values: [priceMin]
        }
        array.push(priceMinObj);
      }
      if (priceMax !== spanValues.maxStart) {
        var pricMaxObj = {
          type: 'SpanValues',
          key: 'pricemax',
          values: [priceMax]
        }
        array.push(pricMaxObj);
      }

      // Trigger action to core
      dynamicFilterGuiTop.triggers.updateFilterOptions(array)
    });
  },
  bindBoolFilters: function (element) {
    dynamicFilterGuiTop.bindCustomFilterEvents(element);

    var $input = element.find('input');

    var boolStartValue = $input.is(':checked')

    element.find('button.btnOk').on('click', function () {
      if (boolStartValue === $input.is(':checked')) {
        element.removeClass('open')
        return true
      }
      var array = new Array();
      var object = {
        type: 'BoolValues',
        key: element.data('key'),
        values: [$input.is(':checked')]
      }
      array.push(object);
      dynamicFilterGuiTop.triggers.updateFilterOptions(array)
    });
  },
  bindListFilters: function (element) {
    var $select = element.find('select');
    var name = element.data('name');
    var search = true;

    if (dynamicFilterGuiTop.helpers.isMobile()) {
      search = false;
    }

    $select.SumoSelect({
      search: search,
      triggerChangeCombined: true,
      forceCustomRendering: true,
      placeholder: name,
      captionFormat: name + ': {0} ' + J.translate('FilterSelected'),
      captionFormatAllSelected: name + ': ' + J.translate('FilterAllSelected'),
      noMatch: J.translate('FilterNoMatch') + ' "{0}"',
      okCancelInMulti: true,
      isClickAwayOk: dynamicFilterGuiTop.config.listFilters.isClickAwayOk,
      floatWidth: dynamicFilterGuiTop.config.listFilters.fullScreenFiltersWidth,
      searchText: J.translate('FilterSearch') + ' ' + name,
      locale: [J.translate('FilterOk'), J.translate('FilterClose'), J.translate('FilterSelectAll')]
    });

    element.find('p.btnOk').on('click', function () {
      var array = new Array();
      var object = {
        type: 'ListValues',
        key: element.data('key'),
        values: $select.val()
      }
      array.push(object);
      dynamicFilterGuiTop.triggers.updateFilterOptions(array)
    });
  },
  bindMultiLevelListFilters: function (element) {
    var $select = element.find('select');
    var name = element.data('name');
    var SumoElm = $select.SumoSelect({
      search: true,
      triggerChangeCombined: true,
      forceCustomRendering: true,
      placeholder: name,
      captionFormat: name + ': {0} ' + J.translate('FilterSelected'),
      captionFormatAllSelected: name + ': ' + J.translate('FilterAllSelected'),
      okCancelInMulti: true,
      noMatch: J.translate('FilterNoMatch') + ' "{0}"',
      searchText: J.translate('FilterSearch') + ' ' + name,
      locale: [J.translate('FilterOk'), J.translate('FilterClose'), J.translate('FilterSelectAll')]
    });

    // Special case, we need that close button on single select sumo. Ugly but works
    element.find('p.no-match').after('<div class="MultiControls MultiLevelList"><p tabindex="0" class="btnCancel">' + J.translate('FilterClose') + '</p></div>')
    element.find('.optWrapper.isFloating').addClass('multiple')

    element.find('.MultiLevelList.MultiLevelList p.btnCancel').on('click', function () {
      SumoElm.sumo.hideOpts()
    })

    element.find('select').on('change', function () {
      var array = new Array();
      var object = {
        type: 'MultiLevelListValues',
        key: element.data('key'),
        values: [$select.val()]
      }
      array.push(object);
      dynamicFilterGuiTop.triggers.updateFilterOptions(array)
    });
  },
  helpers: {
    isMobile: function () {
      return J.switch.width === 'small'
    },
    showSpinner: function () {
      $('div.df-css').addClass('show');
    },
    hideSpinner: function () {
      $('div.df-css').removeClass('show');
    },
    hideFilter: function (event) {
      dynamicFilterGuiTop.data.mobileMenuOpened = false;
      var element = $(event.currentTarget);
      element.closest('div.filter-wrapper').removeClass('open');
      $('html').removeClass('stop-scroll');
    },
    showFilter: function () {
      dynamicFilterGuiTop.data.mobileMenuOpened = true;
      $(this).closest('div.filter-wrapper').addClass('open');
    },
    getElement: function (wrapper) {
      if (wrapper === undefined) {
        // selector is undefined, return false
        return false;
      }
      var $Elm = false;

      // Check if selector is found
      if ($(wrapper.selector).length == 1) {
        // Found - Use selector from wrapper
        $Elm = $(wrapper.selector);
      } else {
        // Not Found - Use html from wrapper
        $Elm = $(wrapper.html)
      }
      return $Elm;
    },
    getTemplate: function (template) {
      if (template === undefined) {
        // template is undefined, return false
        return false;
      }

      var $template = false;

      // Check if we have a setting for current J.switch.width, no? use default
      if (template[J.switch.width] !== undefined) {
        $template = J.views[template[J.switch.width]];
      } else {
        $template = J.views[template.default];
      }

      return $template;
    }
  },
  backClickMemory: {
    isBackClick: false,
    backClickLoadOptions: {},
    scrollPosition: 0,
    init: function () {

      if (dynamicFilterGuiTop.config.useBackClickMemory) {

        var currentLocation = location.href;
        var docRef = document.referrer;
        var bcReferrer = getCookie("bc-referrer");
        var bcLastVisited = getCookie("bc-last-visited");

        if (!bcReferrer) {
          bcReferrer = "";
        }
        if (!bcLastVisited) {
          bcLastVisited = "";
        }

        if (currentLocation == bcLastVisited) {
          //log("Was page reload");
        }
        else if (currentLocation == bcReferrer && docRef != bcLastVisited) {
          //log("Was back click");
          setCookie("bc-referrer", docRef);
          setCookie("bc-last-visited", currentLocation);
          dynamicFilterGuiTop.backClickMemory.isBackClick = true;
        }
        else {
          //log("Was NOT back click");
          setCookie("bc-referrer", docRef);
          setCookie("bc-last-visited", currentLocation);
        }

        // ON CAT PAGE - STORE POSITION AND PRODUCTS ON PAGE UNLOAD
        if (J.checker.isCategoryPage || J.checker.isCategoryAdvancedPage) {

          // CHECK IF CATEGORY PAGE HAS LOADED PRODS ON LEAVING
          $(window).on("beforeunload", function () {
            dynamicFilterGuiTop.backClickMemory.storeCategoryData();
          });

          // GET THE SAVED CATEGORY PRODUCTS & SCROLL DOWN
          if (typeof (Storage) !== "undefined" && dynamicFilterGuiTop.backClickMemory.isBackClick) {
            var backClickData = {};
            var backClickDataExists = (localStorage.getItem("backClickData") !== null);
            var currentCatBackClickDataExists = false;
            var scrollPos;
            var loadedProdsQty;
            var currentCatId = J.data.categoryId.toString();
            var productsPerPage = dynamicFilterGuiTop.config.productsPerPage;
            var pagesToLoad;

            // GET CAT DATA FROM STORAGE
            if (backClickDataExists) {
              backClickData = JSON.parse(localStorage.getItem("backClickData"));
              currentCatBackClickDataExists = (typeof backClickData[currentCatId] !== "undefined");
              if (currentCatBackClickDataExists) {
                scrollPos = backClickData[currentCatId]["scrollPos"];
                loadedProdsQty = backClickData[currentCatId]["loadedProdsQty"];

                if (scrollPos && loadedProdsQty) {

                  pagesToLoad = Math.ceil(loadedProdsQty / productsPerPage);

                  dynamicFilterGuiTop.backClickMemory.backClickLoadOptions = {
                    xhr: dynamicFilterGuiTop.config.enableXhr,
                    productsPerPage: loadedProdsQty,
                    page: 1
                  }

                  dynamicFilterGuiTop.backClickMemory.backClickLoadOptions.scrollPosition = scrollPos;

                  // SET REGULAR PAGE NUMBER CORRECTLY
                  dynamicFilterGuiTop.data.productPageToFetch = pagesToLoad;
                }
              }
            }
          }
        }
      }
    },
    scrollToPosition: function (hasRunOnce) {
      var prodList = $(".dynamic-filter-products ul.product-list");
      if (prodList.length) {
        var prodListOffset = prodList.offset().top;
        var scrollPos = parseInt(dynamicFilterGuiTop.backClickMemory.backClickLoadOptions.scrollPosition)
        var scrollTop = prodListOffset + scrollPos;
        $(window).scrollTop(scrollTop);
      }
    },
    storeCategoryData: function () {
      //log("store");
      if (typeof (Storage) !== "undefined") {
        var prodList = $(".dynamic-filter-products ul.product-list");

        if (prodList.length) {
          var backClickData = {};
          var backClickDataExists = (localStorage.getItem("backClickData") !== null);
          var scrollTop = $(window).scrollTop();
          var prodListOffset = prodList.offset().top;
          var loadedProdsQty = prodList.find(".product-wrapper, .product-advanced-row").length;
          var currentCatId = J.data.categoryId.toString();
          var scrollPos = scrollTop - prodListOffset;

          // GET CAT DATA FROM STORAGE
          if (backClickDataExists) {
            backClickData = JSON.parse(localStorage.getItem("backClickData"));
          }

          backClickData[currentCatId] = {};
          backClickData[currentCatId]["scrollPos"] = scrollPos.toString();
          backClickData[currentCatId]["loadedProdsQty"] = loadedProdsQty;

          localStorage.setItem("backClickData", JSON.stringify(backClickData));
        }
      }
    }
  }
};

J.pages.addToQueue("all-pages", dynamicFilterGuiTop.backClickMemory.init());
J.pages.addToQueue("category-page", dynamicFilterGuiTop.init);
J.pages.addToQueue("category-advanced-page", dynamicFilterGuiTop.init);
