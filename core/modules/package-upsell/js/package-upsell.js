var packUp = {
  config: {
    activatePackage: true,
    activateUpsell: true,
    hideItemSubname: false,
    hideItemStockStatus: false,
    hideItemPrice: false,
    hideItemQuantity: false,
    hideSavings: false,
    useVerbosePricing: true,
    appendPackUpContainerTo: ".product-description",
    useExistingBuyBtnText: true,
    decimalPlaces: { // HOW MANY DECIMALS IN PRICES PER CHANNEL
      channel_1: 0,
      channel_2: 0,
      channel_3: 0,
      channel_4: 0,
      channel_5: 0
    }
  },
  container: null,
  isPackage: false,
  isUpsell: false,
  data: {
    package: null,
    upsell: null
  },
  init: function () {

    // PACKAGE
    if (typeof JetshopData.Package !== "undefined" && packUp.config.activatePackage) {
      packUp.data.package = JSON.parse(JSON.stringify(JetshopData.Package));
      packUp.isPackage = true;
    }

    // UPSELL
    if (typeof JetshopData.UpSellProducts !== "undefined" && packUp.config.activateUpsell) {
      packUp.data.upsell = JSON.parse(JSON.stringify(JetshopData.UpSellProducts));
      packUp.isUpsell = true;
    }

    // INIT BASE FUNCTIONS
    if (packUp.isPackage || packUp.isUpsell) {

      // RENDER CONTAINERS
      var baseTemplate = J.views['package-upsell/base-template'];
      var baseHtml = baseTemplate();
      $(packUp.config.appendPackUpContainerTo).append(baseHtml);
      packUp.container = $(".package-upsell-wrapper");

      packUp.base.setClasses();
      packUp.setTranslations();
      packUp.currencyDisplay.init();
      packUp.helpers.registerHandlebarHelpers();
      packUp.totalPrice.init();
      packUp.addToCart.init();
      packUp.events.init();
      packUp.helpers.overlay.init();
    }

    if (packUp.isPackage) {
      $("html").addClass("package-active");
      packUp.package.init();
      packUp.package.validation.checkProductAvailability();
    }

    if (packUp.isUpsell) {
      $("html").addClass("upsell-active");
      packUp.upsell.init();
    }

  },
  base: {
    setClasses: function () {
      if (packUp.config.hideItemSubname) {
        packUp.container.addClass("hide-subname");
      }
      if (packUp.config.hideItemStockStatus) {
        packUp.container.addClass("hide-stock-status");
      }
      if (packUp.config.hideItemPrice) {
        packUp.container.addClass("hide-item-price");
      }
      if (packUp.config.hideItemQuantity) {
        packUp.container.addClass("hide-item-qty");
      }
      if (packUp.config.hideSavings) {
        packUp.container.addClass("hide-savings");
      }
      if (packUp.config.useVerbosePricing) {
        packUp.container.addClass("verbose-pricing");
      }
    }
  },
  package: {
    init: function () {
      packUp.package.parseData();
      packUp.package.render();
      packUp.totalPrice.calculateTotals();
    },
    parseData: function () {
      var packageData = packUp.data.package;

      // SORT ITEMS IN PACKAGE ACCORDING TO SORT ORDER
      packageData.ProductsInPackage.sort(function (a, b) {
        return a.SortOrder - b.SortOrder;
      });

      $.each(packageData.ProductsInPackage, function (index, item) {
        item.hasAttrs = item.Attributes !== null;

        packUp.helpers.addPackagePricing(item);

        if (item.hasAttrs) {
          item.StockStatus = "";
          if (item.Attributes.Options.length === 1) {
            item.attrData = {
              isSingleAttr: true,
              attrLv1Name: item.Attributes.Options[0].Name,
              lv1Attrs: item.Attributes.Variations
            }

            $.each(item.attrData.lv1Attrs, function (index, data) {
              packUp.helpers.addPackagePricing(data);
            });
          }
          else {
            item.attrData = {
              isSingleAttr: false,
              attrLv1Name: item.Attributes.Options[0].Name,
              attrLv2Name: item.Attributes.Options[1].Name,
              lv1Attrs: []
            }

            // STORE LV1 NAMES
            $.each(item.Attributes.Options[0].Values, function (index, attrName) {
              item.attrData.lv1Attrs.push({
                value: attrName,
                lv2Attrs: []
              })
            });

            // SEPARATE AND STORE THE VARIATIONS UNDER CORRECT LV1
            $.each(item.Attributes.Variations, function (variationIndex, variation) {
              var lv1AttrValue = variation.Variation[0];
              packUp.helpers.addPackagePricing(variation);

              $.each(item.attrData.lv1Attrs, function (lv1Index, lv1Data) {
                if (lv1Data.value === lv1AttrValue) {
                  lv1Data.lv2Attrs.push(variation);
                  return false;
                }
              });
            });
          }
        }
      });
    },
    render: function () {
      var template = J.views['package-upsell/package-products'];
      var html = template(packUp.data.package);
      packUp.container.find(".package-products").append(html);
    },
    validation: {
      checkProductAvailability: function () {
        if (!packUp.data.package.AllProductsAreAvailable) {
          $(".add-to-cart-wrapper").addClass("disabled");
          $(".error-messages .not-buyable-msg").addClass("active");
        }
      }
    }
  },
  upsell: {

    init: function () {
      packUp.upsell.parseData();
      packUp.upsell.render();
      packUp.totalPrice.calculateTotals();
    },
    parseData: function () {

      var upsellData = packUp.data.upsell;
      upsellData.allProdsAreDiscounted = true;

      // SORT UPSELL ITEMS ACCORDING TO SORT ORDER
      upsellData.UpSellProducts.sort(function (a, b) {
        return a.SortOrder - b.SortOrder;
      });

      $.each(upsellData.UpSellProducts, function (index, item) {

        item.hasAttrs = item.Attributes !== null;
        packUp.helpers.addPackagePricing(item);

        // CHECK IF ALL UPSELL PRODS ARE DISCOUNTED
        if (!item.hasPackUpDiscount) {
          upsellData.allProdsAreDiscounted = false;
        }

        if (item.hasAttrs) {
          item.StockStatus = "";
          if (item.Attributes.Options.length === 1) {
            item.attrData = {
              isSingleAttr: true,
              attrLv1Name: item.Attributes.Options[0].Name,
              lv1Attrs: item.Attributes.Variations
            }

            $.each(item.attrData.lv1Attrs, function (index, data) {
              packUp.helpers.addPackagePricing(data);
            });
          }
          else {
            item.attrData = {
              isSingleAttr: false,
              attrLv1Name: item.Attributes.Options[0].Name,
              attrLv2Name: item.Attributes.Options[1].Name,
              lv1Attrs: []
            }

            // STORE LV1 NAMES
            $.each(item.Attributes.Options[0].Values, function (index, attrName) {
              item.attrData.lv1Attrs.push({
                value: attrName,
                lv2Attrs: []
              })
            });

            // SEPARATE AND STORE THE VARIATIONS UNDER CORRECT LV1
            $.each(item.Attributes.Variations, function (variationIndex, variation) {
              var lv1AttrValue = variation.Variation[0];
              packUp.helpers.addPackagePricing(variation);

              $.each(item.attrData.lv1Attrs, function (lv1Index, lv1Data) {
                if (lv1Data.value === lv1AttrValue) {
                  lv1Data.lv2Attrs.push(variation);
                  return false;
                }
              });
            });
          }
        }
      });
    },
    render: function () {
      var template = J.views['package-upsell/upsell-products'];
      var html = template(packUp.data.upsell);
      packUp.container.find(".upsell-products").append(html);
    },
  },

  totalPrice: {
    init: function () {
      packUp.totalPrice.render();
    },
    render: function () {
      var template = J.views['package-upsell/total-price'];
      var html = template();
      packUp.container.find(".total-price").append(html);

    },
    calculateTotals: function () {
      var totalPriceContainers = $('.package-upsell-wrapper .total-price');
      var baseProdPrice = 0;
      var totalOutPrice = 0;
      var totalStdPrice = 0;
      var totalDscPrice = 0;
      var totalPkgPrice = 0;
      var savingsInCurrency;
      var savingsInPercent;
      var parsePrice = packUp.currencyDisplay.parsePriceToString;
      var basePriceContainer = $(".purchase-block-price-amount span[itemprop='price']");
      var dscPriceContainer = totalPriceContainers.find(".original-price .dsc-price");
      var stdPriceContainer = totalPriceContainers.find(".original-price .std-price");
      var pkgPriceContainer = totalPriceContainers.find(".current-price .pkg-price");
      var outPriceContainer = totalPriceContainers.find(".current-price .out-price");
      var savingsCurrContainer = totalPriceContainers.find(".savings .currency");
      var savingsPrctContainer = totalPriceContainers.find(".savings .percentage");

      // IF ONLY UPSELL, GET CURRENT PRICE FROM DOM
      if (!packUp.isPackage) {
        baseProdPrice = packUp.currencyDisplay.parsePriceStringToNumber(basePriceContainer.text());

        totalDscPrice += baseProdPrice;
        totalStdPrice += baseProdPrice;
        totalOutPrice += baseProdPrice;
        totalPkgPrice += baseProdPrice;
      }


      // GET ALL PACKAGE/UPSELL ITEM PRICES
      packUp.container.find(".package-item, .upsell-item.buy-active").each(function () {
        var item = $(this);
        var qty = parseInt(item.attr("data-quantity"));
        var stdPrice = parseFloat(item.attr("data-std-price"));
        var dscPrice = parseFloat(item.attr("data-dsc-price"));
        var outPrice = parseFloat(item.attr("data-out-price"));
        var pkgPrice = parseFloat(item.attr("data-pkg-price"));

        if (dscPrice) {
          totalDscPrice += qty * dscPrice;
        }
        else {
          totalDscPrice += qty * stdPrice;
        }

        totalStdPrice += qty * stdPrice;
        totalOutPrice += qty * outPrice;
        totalPkgPrice += qty * pkgPrice;
      });

      // CALCULATE SAVINGS
      savingsInCurrency = totalOutPrice - totalPkgPrice;
      savingsInPercent = Math.round(savingsInCurrency / totalOutPrice * 100);

      // CHECK FOR ACTIVE DISCOUNTS
      if (totalOutPrice === totalPkgPrice) {
        totalPriceContainers.addClass("no-discount").removeClass("has-discount");
      }
      else {
        totalPriceContainers.addClass("has-discount").removeClass("no-discount");
      }

      // RENDER IN ELEMENTS
      if (totalDscPrice && totalDscPrice !== totalStdPrice) {
        packUp.helpers.changeValue(dscPriceContainer, parsePrice(totalDscPrice));
      }
      else {
        packUp.helpers.changeValue(dscPriceContainer, "");
      }
      packUp.helpers.changeValue(stdPriceContainer, parsePrice(totalStdPrice));
      packUp.helpers.changeValue(pkgPriceContainer, parsePrice(totalPkgPrice));
      packUp.helpers.changeValue(outPriceContainer, parsePrice(totalOutPrice));
      packUp.helpers.changeValue(savingsCurrContainer, parsePrice(savingsInCurrency));
      packUp.helpers.changeValue(savingsPrctContainer, "(" + savingsInPercent + " %)");
    }
  },

  addToCart: {
    init: function () {

      // IF ONLY UPSELL - HIJACK PLATFORM ADD TO CART FUNCTION TO MAKE SURE ORG PROD IS ADDED TO CART
      if (packUp.isUpsell && !packUp.isPackage) {
        var oldAddToCart = JetShop.StoreControls.Services.General.AddCartItem;
        JetShop.StoreControls.Services.General.AddCartItem = function () {
          $(window).trigger("baseProdAdded");
          return oldAddToCart.apply(this, arguments);
        };
      }
      packUp.addToCart.render();
    },
    render: function () {
      var template = J.views['package-upsell/add-to-cart'];
      var html = template();
      packUp.container.find(".add-to-cart-wrapper").append(html);


      // COPY EXISTING ADD TO CART BTN TEXT
      if (packUp.config.useExistingBuyBtnText) {
        var addBtnText = $(".purchase-block .buy-button:first").text().trim();
        packUp.container.find(".add-to-cart-wrapper .buy-button span").text(addBtnText);
      }
    },
    validation: {
      validateAll: function (callback) {
        packUp.addToCart.validation.checkItemAttributes();
        packUp.addToCart.validation.checkBaseProductAddToCartBtnStatus();
        packUp.addToCart.validation.checkAddToCartStatus();

        if (!packUp.container.find(".add-to-cart-wrapper").hasClass("disabled")) {
          if (callback) {
            callback();
          }
        }
      },
      checkItemAttributes: function () {
        var addToCartWrapper = packUp.container.find(".add-to-cart-wrapper");
        var validItems = $(".package-item, .upsell-item.buy-active");
        var validationErrorFound = false;

        validItems.each(function () {
          var item = $(this);
          var visibleAttributes = item.find(".attr-lv1 select, .attr-lv2.visible select");
          visibleAttributes.each(function () {
            var attrSelect = $(this);
            if (!attrSelect.val()) {
              attrSelect.closest(".attribute-select-wrapper").addClass("error");
              validationErrorFound = true;
            }
          });
        });

        if (validationErrorFound) {
          addToCartWrapper.attr("data-attr-error", "true").addClass("disabled").find(".attr-msg").addClass("active");
        }
        else {
          addToCartWrapper.attr("data-attr-error", "false").find(".attr-msg").removeClass("active");
        }
        packUp.addToCart.validation.checkAddToCartStatus();

      },
      checkBaseProductAddToCartBtnStatus: function () {
        if (packUp.isUpsell && !packUp.isPackage) {

          if ($(".purchase-block-buy > div").css("visibility") === "hidden") {
            packUp.container.find(".add-to-cart-wrapper").attr("data-base-prod-disabled", "true");
          }
          else {
            packUp.container.find(".add-to-cart-wrapper").attr("data-base-prod-disabled", "false");
          }
        }

      },
      checkAddToCartStatus: function () {
        var addToCartWrapper = packUp.container.find(".add-to-cart-wrapper");

        if (addToCartWrapper.attr("data-base-prod-disabled") === "true"
          || addToCartWrapper.attr("data-attr-error") === "true"

        ) {
          addToCartWrapper.addClass("disabled");
        }
        else {
          addToCartWrapper.removeClass("disabled");
        }
      }
    },
    initiateAddToCartSequence: function () {

      // ONLY UPSELL - WAIT FOR ORG PROD TO BE ADDED
      if (packUp.isUpsell && !packUp.isPackage) {

        // LISTEN TO OK:D BASE PURCHASE
        $(window).on("baseProdAdded", function () {
          packUp.addToCart.addItems();
        });

        // CLICK BASE BUY BTN
        $(".purchase-block-buy a").click();

        // STOP LISTENING AFTER TIMEOUT
        setTimeout(function () {
          $(window).off("baseProdAdded");
        }, 200);
      }
      else {
        packUp.addToCart.addItems();
      }
    },
    addItems: function () {
      var itemsToAdd = [];
      var validItems = $(".package-item, .upsell-item.buy-active");

      validItems.each(function () {
        var item = $(this);
        var attrWrapper = item.find(".attributes-wrapper");
        var prodId = item.data("prod-id");
        var qty = item.data("quantity");
        var attrId;
        var activeSelect;

        if (attrWrapper.length) {
          if (attrWrapper.hasClass("double-attr")) {
            activeSelect = attrWrapper.find(".attribute-select-wrapper.attr-lv2.visible select");
            attrId = activeSelect.find("option:selected").data("attr-id");
          }
          else {
            activeSelect = attrWrapper.find(".attribute-select-wrapper.attr-lv1 select");
            attrId = activeSelect.find("option:selected").data("attr-id");
          }
        }

        var itemToAdd = {
          CartId: J.cart.cartId,
          ProductId: prodId,
          Quantity: qty
        }

        if (attrId) {
          itemToAdd.AttributeId = attrId;
        }
        itemsToAdd.push(itemToAdd);

      });

      if (itemsToAdd.length) {
        packUp.addToCart.addMultipleItemsToCart(itemsToAdd, function () {
          Services.cartService.reload();
          if (typeof dynamicCart !== "undefined") {
            dynamicCart.open(true);
          }
        }, null);
      }
    },
    addMultipleItemsToCart: function (itemArray, callback, callbackOptions) {

      var products_json = JSON.stringify(itemArray);
      var restUrl = J.config.urls.ServicesUrl + "/rest/v2/json/shoppingcart/multiple/" + J.cart.cartId;

      if (J.checker.isLoggedIn) {
        restUrl += "?pricelistid=" + J.data.priceListId;
      }

      packUp.helpers.overlay.show();
      $.ajax({
        type: "POST",
        cache: false,
        url: restUrl,
        data: products_json,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
          packUp.helpers.overlay.hide();
          // log(data);
          if (callback) {
            callback(data, callbackOptions);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.error('Ajax Error - Add Multiple Items To Cart');
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
          packUp.helpers.overlay.hide();
        }
      });
    }
  },
  events: {
    init: function () {
      packUp.events.bindAttributeSelectors();
      packUp.events.bindItemLinks();
      packUp.events.bindUpsellActivators();
      packUp.events.bindBaseAttributes();
      packUp.events.bindBuyBtn();
    },
    bindAttributeSelectors: function () {
      packUp.container.on("change", "select", function (event) {
        var changedSelect = $(this);
        var attributeWrapper = changedSelect.closest(".attributes-wrapper");
        var selectWrapper = changedSelect.closest(".attribute-select-wrapper");
        var isSingleAttrProd = attributeWrapper.hasClass("single-attr");

        // VALIDATION
        if (selectWrapper.hasClass("error")) {
          selectWrapper.removeClass("error");
          packUp.addToCart.validation.checkItemAttributes();
          packUp.addToCart.validation.checkAddToCartStatus();
        }

        if (isSingleAttrProd) {
          packUp.helpers.executeAttributeChanges(changedSelect);
        }
        else {
          if (changedSelect.hasClass("attr-lv1")) {
            var selectIndex = changedSelect.find("option:selected").index();
            if (selectIndex > 0) {
              changedSelect.closest(".attribute-select-wrapper").addClass("valid");
              attributeWrapper.find(".attributes-lv2 .attribute-select-wrapper")
                .removeClass("visible")
                .removeClass("disabled")
                .filter(":eq(" + (selectIndex - 1) + ")").addClass("visible");

              // IF 2ND LV ATTR IS SELECTED FROM BEFORE, ACTIVATE IT
              attributeWrapper.find(".attributes-lv2 .attribute-select-wrapper.visible select").change();
            }
            else {
              // NOTHING SELECTED
              changedSelect.closest(".attribute-select-wrapper").removeClass("valid");
              attributeWrapper.find(".attributes-lv2 .attribute-select-wrapper.visible").addClass("disabled");

            }
          }
          else {
            packUp.helpers.executeAttributeChanges(changedSelect);
          }
        }
      });
    },
    bindItemLinks: function () {
      packUp.container.on("click", "a.item-link", function (event) {
        event.preventDefault();
        var clickedLink = $(this);
        var item = clickedLink.closest(".item");
        var prodId = item.data("prod-id");

        if (prodId) {

          var popupContainer = $("<div id='popup-container' />");
          var spinnerContainer = $("<div class='spinner-container' />");
          var spinner = $("<i class='fa fa-spin fa-spinner' />");

          spinner.appendTo(spinnerContainer);
          spinnerContainer.appendTo(popupContainer);

          $.fancybox({
            // openEffect: 'elastic',
            // closeEffect: 'elastic',
            content: popupContainer,
            title: "",
            type: "html",
            helpers: {
              title: {
                type: 'inside'
              }
            }
          });

          J.api.product.get(function (data) {
            var template = J.views['package-upsell/item-popup'];
            var popupCode = template(data.ProductItems[0]);
            popupContainer.html(popupCode);
            $.fancybox.update();

          }, null, true, 5, prodId)
        }
      });
    },
    bindUpsellActivators: function () {
      packUp.container.on("change", ".activator input", function () {
        var checkBox = $(this);
        var item = checkBox.closest(".upsell-item");
        if (checkBox.is(":checked")) {
          item.removeClass("buy-inactive").addClass("buy-active");
        }
        else {
          item.removeClass("buy-active").addClass("buy-inactive");

          // REMOVE THIS PRODUCTS EVENTUAL VALIDATION ERRORS
          if ($(".add-to-cart-wrapper").attr("data-attr-error") === "true") {
            packUp.addToCart.validation.validateAll();
          }
        }
        packUp.totalPrice.calculateTotals();
      });
    },
    bindBaseAttributes: function () {
      if (packUp.isUpsell && !packUp.isPackage) {
        $("#main-area").on("change", ".product-attributes select", function () {
          packUp.totalPrice.calculateTotals();
          packUp.addToCart.validation.checkBaseProductAddToCartBtnStatus();
          packUp.addToCart.validation.checkAddToCartStatus();

        });
      }
    },
    bindBuyBtn: function () {
      packUp.container.on("click", "a.buy-button", function (event) {
        event.preventDefault();
        if (!$(this).closest(".add-to-cart-wrapper").hasClass("disabled")) {
          packUp.addToCart.validation.validateAll(packUp.addToCart.initiateAddToCartSequence);
        }
      });
    }
  },

  currencyDisplay: {
    settings: {
      amountFirst: true,
      //spacerString: "&nbsp;",
      spacerString: "&nbsp;",
      useDecimalComma: true,
      symbol: J.data.currency.symbol,
      decimalPlaces: {}
    },
    init: function () {

      // UPDATE BASE SETTINGS WITH CURRENT VALUES
      var currencyDisplayFormat = J.data.currency.display;
      packUp.currencyDisplay.settings.decimalPlaces = packUp.config.decimalPlaces;
      var settings = packUp.currencyDisplay.settings;

      // WITH OR W/O SPACES
      if (currencyDisplayFormat.substr(1, 1) != " ") {
        settings.spacerString = "";
      }

      // AMOUNT FIRST
      if (currencyDisplayFormat.substr(0, 1) != "n") {
        settings.amountFirst = false;
      }

      // DECIMAL POINT
      if (!J.data.currency.separator.match(",")) {
        settings.useDecimalComma = false;
      }

      // DECIMAL PLACES
      settings.decimalPlaces = packUp.currencyDisplay.settings.decimalPlaces["channel_" + J.data.channelInfo.Active];

      // RETURN MODIFIED DATA
      packUp.currencyDisplay.settings = settings;

    },
    parsePriceStringToNumber: function (priceString) {
      var priceAsNumber;
      priceString = priceString.replace(/\s|€|\$/g, "").replace(/[a-zA-Z]*/g, "");

      if (packUp.currencyDisplay.settings.useDecimalComma) {
        priceString = priceString.replace(".", "").replace(",", ".");
      }
      else {
        priceString = priceString.replace(",", "");
      }
      priceAsNumber = parseFloat(priceString);
      return priceAsNumber;
    },
    parsePriceToString: function (price) {
      var displaySettings = packUp.currencyDisplay.settings;
      var spacerString = displaySettings.spacerString;
      var floatPrice;
      var priceString;

      if (typeof price === "string" && price !== "") {
        floatPrice = parseFloat(price);
      }
      else if (typeof price === "number") {
        floatPrice = price;
      }
      else {
        return "";
      }

      // DECIMAL PLACES
      priceString = floatPrice.toFixed(displaySettings.decimalPlaces);

      // DECIMAL & THOUSAND MARKER
      if (displaySettings.useDecimalComma) {
        priceString = priceString.replace(/\./g, ",");
        priceString = priceString.replace(/\B(?=(?:\d{3})+(?!\d))/g, ".");
      }
      else {
        priceString = priceString.replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
      }

      // MAKE FINAL STRING
      if (displaySettings.amountFirst) {
        priceString = priceString + spacerString + displaySettings.symbol;
      }
      else {
        priceString = displaySettings.symbol + spacerString + priceString;
      }
      return priceString;
    }
  },
  helpers: {

    htmlDecode: function (value) {
      if (value.match(/&lt;/)) {
        return $('<div/>').html(value).text();
      } else {
        var code = value.replace(/\n/g, '<br>');
        return code;
      }
    },
    changeValue: function (element, value) {
      var orgContent = element.html();
      if (orgContent !== value) {
        element.addClass("changing");
        setTimeout(function () {
          element.html(value).removeClass("changing");
        }, 100);
      }
    },
    addPackagePricing: function (dataObj) {
      var stdPrice;
      var dscPrice;
      var pkgPrice;
      var outPrice;
      var hasPackUpDiscount = true;

      if (JetshopData.VatIncluded) {
        stdPrice = dataObj.PriceBeforePackageIncVat;
        dscPrice = dataObj.DiscountPriceBeforePackageIncVat;
        pkgPrice = dataObj.PackagePriceIncVat;
        if (dataObj.DiscountPriceBeforePackageIncVat) {
          outPrice = dataObj.DiscountPriceBeforePackageIncVat;
        }
        else {
          outPrice = dataObj.PriceBeforePackageIncVat;
        }
      }
      else {
        stdPrice = dataObj.PriceBeforePackage;
        dscPrice = dataObj.DiscountPriceBeforePackage;
        pkgPrice = dataObj.PackagePrice;
        if (dataObj.DiscountPriceBeforePackage) {
          outPrice = dataObj.DiscountPriceBeforePackage;
        }
        else {
          outPrice = dataObj.PriceBeforePackage;
        }
      }

      // NO PACKAGE PRICE - MAKE IT THE OUT PRICE
      if (!pkgPrice) {
        pkgPrice = outPrice;
        hasPackUpDiscount = false;
      }

      dataObj.stdPrice = stdPrice;
      dataObj.dscPrice = dscPrice;
      dataObj.pkgPrice = pkgPrice;
      dataObj.outPrice = outPrice;
      dataObj.hasPackUpDiscount = hasPackUpDiscount;
    },
    executeAttributeChanges: function (changedSelect) {
      if (changedSelect.val()) {
        var selectedOption = changedSelect.find("option:selected");
        var stockStatus = selectedOption.data("stock-status");
        var dscPrice = selectedOption.data("dsc-price");
        var stdPrice = selectedOption.data("std-price");
        var outPrice = selectedOption.data("out-price");
        var pkgPrice = selectedOption.data("pkg-price");
        var attrImgSrc = selectedOption.data("img-src");

        // VALIDATE
        changedSelect.closest(".attribute-select-wrapper").addClass("valid");

        // IMG
        packUp.helpers.changeItemImg(changedSelect, attrImgSrc);

        // STOCK STATUS
        packUp.helpers.changeItemStockStatus(changedSelect, stockStatus);

        // PRICES
        packUp.helpers.changeItemPrices(changedSelect, stdPrice, dscPrice, outPrice, pkgPrice);
        packUp.totalPrice.calculateTotals();
      }
      else {
        changedSelect.closest(".attribute-select-wrapper").removeClass("valid");
      }
    },
    changeItemImg: function (element, imgName) {
      var imgContainer = element.closest(".item").find(".image");
      var img = imgContainer.find("img");
      var currentImgSrc = img.attr("src");
      var newImgSrc = "/pub_images/small/" + imgName;

      if (imgName && currentImgSrc !== newImgSrc) {

        imgContainer.addClass("loading");

        // FAILPROOF IMG LOAD
        var tempImg = $("<img />");
        tempImg.one("load", function () {
          img.attr("src", newImgSrc);
          setTimeout(function () {
            imgContainer.removeClass("loading");
          }, 50);
        }).attr("src", newImgSrc);

        if (tempImg[0].complete) {
          tempImg.trigger("load");
        }
      }
    },
    changeItemStockStatus: function (element, stockStatus) {
      var stockStatusElement = element.closest(".item").find(".stock-status .value");
      if (stockStatusElement.text() !== stockStatus) {
        packUp.helpers.changeValue(stockStatusElement, stockStatus);
      }

    },
    changeItemPrices: function (element, stdPrice, dscPrice, outPrice, pkgPrice) {
      var item = element.closest(".item");

      item.attr("data-std-price", stdPrice);
      item.attr("data-dsc-price", dscPrice);
      item.attr("data-out-price", outPrice);
      item.attr("data-pkg-price", pkgPrice);

      var stdPriceString = packUp.currencyDisplay.parsePriceToString(stdPrice);
      packUp.helpers.changeValue(item.find(".original-price .std-price"), stdPriceString);

      var dscPriceString = packUp.currencyDisplay.parsePriceToString(dscPrice);
      packUp.helpers.changeValue(item.find(".original-price .dsc-price"), dscPriceString);

      var outPriceString = packUp.currencyDisplay.parsePriceToString(outPrice);
      packUp.helpers.changeValue(item.find(".current-price .out-price"), outPriceString);

      var pkgPriceString = packUp.currencyDisplay.parsePriceToString(pkgPrice);
      packUp.helpers.changeValue(item.find(".current-price .pkg-price"), pkgPriceString);

    },
    registerHandlebarHelpers: function () {

      Handlebars.registerHelper("priceToString", function (price) {
        return packUp.currencyDisplay.parsePriceToString(price);
      });
      Handlebars.registerHelper("htmlDecode", function (data) {
        return packUp.helpers.htmlDecode(data);
      });
      Handlebars.registerHelper("replace", function (string, oldPattern, newPattern) {
        return string.replace(oldPattern, newPattern);
      });
    },
    overlay: {
      overlayTimeout: null,
      init: function () {
        $("body > form").append("<div id='packup-overlay' class='hidden'><i class='fa fa-spinner fa-spin'></i></div>");
      },
      show: function () {
        var overlay = $("#packup-overlay");
        clearTimeout(packUp.helpers.overlay.overlayTimeout);
        overlay.removeClass("hidden").addClass("active");

        // FALLBACK OVERLAY HIDING IN CASE OF API ERROR
        packUp.helpers.overlay.overlayTimeout = setTimeout(function () {
          console.log("Customer list overlay: API response timed out - disabling");
          packUp.helpers.overlay.hide();
        }, 8000);
      },
      hide: function () {
        var overlay = $("#packup-overlay");
        overlay.removeClass("active");
        clearTimeout(packUp.helpers.overlay.overlayTimeout);
        packUp.helpers.overlay.overlayTimeout = setTimeout(function () {
          overlay.addClass("hidden");
        }, 200);
      }
    },

  },
  setTranslations: function () {
    J.translations.push(
      {
        packUpBuyButton: {
          sv: JetshopData.Translations.FilterBuyButton,
          en: JetshopData.Translations.FilterBuyButton,
          da: JetshopData.Translations.FilterBuyButton,
          nb: JetshopData.Translations.FilterBuyButton,
          fi: JetshopData.Translations.FilterBuyButton
        }
      },
      {
        packUpPackageListTitle: {
          sv: "Produkter i paketet:",
          en: "Products included in package:",
          da: "Products included in package: (DA)",
          nb: "Products included in package: (NB)",
          fi: "Products included in package: (FI)"
        }
      },
      {
        packUpUpsellListTitle: {
          sv: "Köp tillsammans:",
          en: "Buy together at a discount:",
          da: "Products included in package: (DA)",
          nb: "Products included in package: (NB)",
          fi: "Products included in package: (FI)"
        }
      },
      {
        packUpUpsellAllProdsDiscountedListTitle: {
          sv: "Köp till med rabatt:",
          en: "Buy together at a discount:",
          da: "Products included in package: (DA)",
          nb: "Products included in package: (NB)",
          fi: "Products included in package: (FI)"
        }
      },
      {
        packUpStockStatus: {
          sv: JetshopData.Translations.FilterStockStatus,
          en: JetshopData.Translations.FilterStockStatus,
          da: JetshopData.Translations.FilterStockStatus,
          nb: JetshopData.Translations.FilterStockStatus,
          fi: JetshopData.Translations.FilterStockStatus
        }
      },
      {
        packUpSelect: {
          sv: "-- Välj --",
          en: "-- Select --",
          da: "-- Select -- (DA)",
          nb: "-- Select -- (NB)",
          fi: "-- Select -- (FI)"
        }
      },
      {
        packUpUnselectedAttributesErrorMsg: {
          sv: "Välj alternativ för samtliga produkter ovan.",
          en: "Make selections for all products above.",
          da: "Make selections for all products above. (DA)",
          nb: "Make selections for all products above. (NB)",
          fi: "Make selections for all products above. (FI) "
        }
      },
      {
        packUpUnavailableProductErrorMsg: {
          sv: "Denna produkt går tyvärr ej att köpa för tillfället.",
          en: "Sorry, this product is not available at the moment.",
          da: "Sorry, this product is not available at the moment. (DA)",
          nb: "Sorry, this product is not available at the moment. (NB)",
          fi: "Sorry, this product is not available at the moment. (FI)"
        }
      },
      {
        packUpPackageUnbuyable: {
          sv: "Detta paket går tyvärr ej att köpa för tillfället.",
          en: "Sorry, this package is not available at the moment.",
          da: "Sorry, this package is not available at the moment. (DA)",
          nb: "Sorry, this package is not available at the moment. (NB)",
          fi: "Sorry, this package is not available at the moment. (FI)"
        }
      },
      {
        packUpRegularPrice: {
          sv: "Ord. pris",
          en: "Regular price",
          da: "Regular price (DA)",
          nb: "Regular price (NB)",
          fi: "Regular price (FI)"
        }
      },
      {
        packUpPackagePrice: {
          sv: "Paketpris",
          en: "Package price",
          da: "Package price (DA)",
          nb: "Package price (NB)",
          fi: "Package price (FI)"
        }
      },
      {
        packUpTotalPrice: {
          sv: "Totalpris",
          en: "Total price",
          da: "Total price (DA)",
          nb: "Total price (NB)",
          fi: "Total price (FI)"
        }
      },
      {
        packUpNoDiscountPrice: {
          sv: "Pris",
          en: "Price",
          da: "Price (DA)",
          nb: "Price (NB)",
          fi: "Price (FI)"
        }
      },
      {
        packUpUpsellPackagePrice: {
          sv: "Paketpris",
          en: "Package price",
          da: "Package price (DA)",
          nb: "Package price (NB)",
          fi: "Package price (FI)"
        }
      },
      {
        packUpGoToProductPage: {
          sv: "Till produktsidan",
          en: "Go to product page",
          da: "Go to product page (DA)",
          nb: "Go to product page (NB)",
          fi: "Go to product page (FI)"
        }
      },
      {
        packUpSavings: {
          sv: "Du sparar",
          en: "You save",
          da: "You save (DA)",
          nb: "You save (NB)",
          fi: "You save (FI)"
        }
      },
      {
        packUpPriceEach: {
          sv: "/st.",
          en: "/ea.",
          da: "/st. (DA)",
          nb: "/st. (NB)",
          fi: "/st. (FI)"
        }
      },
      {
        packUpProductNotAvailable: {
          sv: "Denna produkt går ej att köpa för tillfället.",
          en: "This item is not available at the moment.",
          da: "This item is not available at the moment. (DA)",
          nb: "This item is not available at the moment. (NB)",
          fi: "This item is not available at the moment. (FI)"
        }
      }
    );
  },

};

J.pages.addToQueue("product-page", packUp.init);
