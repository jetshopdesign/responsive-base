# Package & upsell products
This module enables the package product and upsell product functionality. Since the two are very similar, and to be able to use both at once, we made a single module for both of them. 

## Package product
Packages are a way of offering a discount that is activated when the end user buys a specific set of products.
For example you could set up a "Sports" packages consisting of three products. A shirt, a pair of shorts and some shoes. If the customer buys all three products he/she would receive a discount in the checkout.

So, to create a Package we must first create a "placeholder product", this is created just like a regular product, its price does not matter since this product's primary use will be to give the package a page and make it searchable in the shop. After the initial save of the product you can navigate to the new Package tab that is on the Product page. On this page you can add products to the recently created product. This will mark it as "placeholder product" and will no longer be buyable since it's no longer a regular product. 

## Upsell
Upsell is a way of offering discounts to the end user on a select set of products if they buy a mandatory product. It is administered much in the same way as Packages, but they can be added to a package or a regular product. Upsell products are administered in the same way as packages with the same set of features. 

## General requirements and limitations
- A package must contain at least two different products.

- A package can't contain other packages.

- A package can't be used as an upsell to another product.

- The total price of the package is calculated from the sum of all products minus their package discounts.

- A package can contain upsells, but they must share the same discount type as the package products (Percentage or Amount).

- Package & Upsell discounts will be prioritized over other discounts. They will though be combined with discounts that are set directly on the product from the product view.

- Products that are included in the calculation of package/upsell discounts will be “consumed” so they can not be used in another discount (Eg: if you add two packages to the cart and they share the same product the price calculator will try to work out the best discount and apply that one).


## Using this module
To enable this module in responsive-base, just add it to the modules.json file like any other module. Package/upsell must be enabled for the store (contact support if these tabs are not present on the admin product pages).

## Config options
 - hideItemSubname etc (boolean): If true, will hide the described item element. It will still be in the DOM, but will not be visible. 
 
 - useVerbosePricing (boolean): When set to true, the original price will be shown on a separate line above the package/upsell discounted price, both for items and the total sum. This way the original standard price and the original discount price are both visible for the customer. If set to false, only the original non-package/upsell price is shown (you cannot see if the price is currently discounted outside of the package/upsell offer). This is more compact but some stores might prefer the verbose option.

- useExistingBuyBtnText (boolean): Get the buy btn text from the existing buy btn on the page. Set to false if you want a different buy btn text for package/upsell.

The other config options should be self-explanatory.


## Mixins & variables
If you are implementing on an old version of RB you might need the following definitions. Just insert it at the top of the SCSS file if you get compilation errors.

$js-button-disabled-color: #ddd;
$js-button-disabled-background-color: #888888;
$js-button-disabled-border-color: $js-button-disabled-background-color;
$js-button-disabled-opacity: 0.4;

@mixin js-button-global-disabled-style {
  color: $js-button-disabled-color;
  background-color: $js-button-disabled-background-color;
  border-color: $js-button-disabled-border-color;
  opacity: $js-button-disabled-opacity;
  cursor: default;
}