# Voyado integration
Integrates my pages, prices etc with Voyado customer club.


## Dynamic filter
If you are using the dynamic filters and implementing this module on a pre-1.5.2 responsive base store, you will need to slightly modify the price code structure of the following files: 
``` 
/core/modules/dynamic-filter-gui-top/views/dynamic-filter-product-items.hbs
/core/modules/dynamic-filter-gui-top/views/dynamic-filter-product-items-advanced.hbs
``` 

Replace the price-promotion div with the following code: 
``` 
<div class="{{#if DiscountedPriceIsMemberPrice}}price-member{{else}}price-promotion{{/if}}">
``` 

That's it.









