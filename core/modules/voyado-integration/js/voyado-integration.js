var voyado = {
    config: {
        showNotLoggedInCartMemberPrice: true,
        showDiscountOffers: true,
        showPointCards: true,
        discountOffersExternalDataPreferred: false,
        discountOfferFallbackImageUrl: "/stage/images/custom/discount-offer-fallback.png",
    },
    customerData: JetshopData.Customer,
    init: function () {

        // CHECK FOR LOGGED IN BUT NO CUSTOMER DATA OBJECT BUG (RELOGIN)
        if (J.checker.isLoggedIn && !voyado.customerData) {
            alert("Please log in again.");
            location.assign($(".login-text:first").attr("href"));
        }
        else {
            voyado.helpers.addTranslations();
            voyado.helpers.registerHandlebarHelpers();
            voyado.dynamicCart.init();
            if (voyado.helpers.checkIfMyPages()) {
                voyado.myPages.init();
            }
        }
    },
    dynamicCart: {
        init: function () {

            if (!J.checker.isLoggedIn) {

                // BIND CART UPDATE
                $(window).on("cart-updated", function () {
                    voyado.dynamicCart.checkForMemberProds();
                });
            }

        },
        checkForMemberProds: function () {
            $.each(J.cart.ProductsCart, function (prodIndex, prodData) {
                if (prodData.DiscountedPriceIsMemberPrice) {
                    var template = J.views['voyado-integration/dynamic-cart-item-alert'];
                    var loginUrl = $(".login-text:first").attr("href");
                    var memberPriceString = prodData.MemberPriceText;

                    if (!J.checker.isVatIncluded) {
                        memberPriceString = prodData.MemberPriceNoVatText;
                    }
                    var html = template({
                        loginUrl: loginUrl,
                        showNotLoggedInCartMemberPrice: voyado.config.showNotLoggedInCartMemberPrice,
                        memberPriceString: memberPriceString
                    });
                    $("#dc-content .dc-item-row:eq(" + prodIndex + ")").append(html).find(".item").append(html);
                }
            });
        }
    },
    myPages: {
        init: function () {

            if (J.checker.isLoggedIn) {
                voyado.myPages.fixBasicLayout();
            }

            // CAMPAIGNS ON "HOME" PAGE
            if ($("html.page-responsivemypages").length) {
                voyado.myPages.campaigns.init();
            }


        },
        fixBasicLayout: function () {
            var greetingPanel = $("#ctl00_main_myPagesHeader_MyPagesHeaderGreetingPanel");
            var myPagesNav = $("#my-pages-nav");
            var navigationWrapper = $("<div id='my-pages-nav-wrapper'></div>");
            var mainContentWrapper = $("<div id='my-pages-main-content-wrapper'></div>");
            var landingPageItemWrapper = $("<div id='my-pages-landing-page-item-wrapper'></div>");


            // VOYADO LANDING PAGE
            if ($("#my-pages-landing-page-wrapper").length) {

                var landingPageItems = $(".my-pages-landing-page-item");

                landingPageItems.appendTo(landingPageItemWrapper);
                landingPageItemWrapper.addClass("item-qty-" + landingPageItems.length);
                myPagesNav.add(landingPageItemWrapper).appendTo(navigationWrapper);

                $(".my-pages-landing-page-text").appendTo(mainContentWrapper);
            }
            else {
                myPagesNav.nextUntil().appendTo(mainContentWrapper);
                myPagesNav.appendTo(navigationWrapper);
            }

            // INSERT ELEMENTS
            greetingPanel.after(navigationWrapper);
            navigationWrapper.after(mainContentWrapper);

        },
        campaigns: {
            init: function () {
                voyado.api.getCampaigns(voyado.myPages.campaigns.parse, null, false, 5); // CHANGE BACK TO CACHE AFTER DEVELOPMENT
            },
            parse: function (campaignData) {

                // log(campaignData);

                if (voyado.config.showDiscountOffers) {
                    voyado.myPages.campaigns.discountOffers.parse(campaignData.DiscountOffers);
                }

                if (voyado.config.showPointCards) {
                    voyado.myPages.campaigns.pointCards.parse(campaignData.PointCards);
                }

            },
            discountOffers: {
                parse: function (discountOffers) {
                    var validDiscountOffers = [];
                    var selectData = voyado.helpers.selectDiscountOfferData;

                    $.each(discountOffers, function (offerIndex, offerData) {

                        if (voyado.helpers.checkIfDiscountOfferIsActive(offerData)) {
                            var extData = offerData.ExternalDiscountOffer;
                            var intData = offerData.InternalDiscountOffer;
                            var offer = {};
                            offer.linkUrl = selectData(intData.CampaignUrl, extData.Link);
                            offer.imgUrl = selectData(intData.CampaignImageUrl, extData.ImageUrl);
                            offer.title = selectData(intData.CampaignName, extData.Heading);
                            offer.description = selectData(intData.Description, extData.Description);
                            offer.endDate = extData.DiscountEndDate.substr(0, 10);
                            validDiscountOffers.push(offer);
                        }
                    });

                    if (validDiscountOffers.length) {
                        voyado.myPages.campaigns.discountOffers.render(validDiscountOffers);
                    }
                    else {
                        console.log("No active discount offers to show.");
                    }
                },
                render: function (validDiscountOffers) {

                    // console.log("validDiscountOffers: ");
                    // console.log(validDiscountOffers);

                    var template = J.views['voyado-integration/discount-offers'];
                    var html = template(validDiscountOffers);
                    $("#my-pages-main-content-wrapper").prepend(html);


                    voyado.myPages.campaigns.discountOffers.conclude();

                },
                conclude: function () {

                    $(".voyado-discount-offers li").each(function () {
                        var offer = $(this);
                        var img = offer.find(".offer-image img");
                        var imgSrc = img.data("src");

                        // FALLBACK IMAGE
                        img.on("error", function () {
                            img.attr("src", voyado.config.discountOfferFallbackImageUrl);
                        }).attr("src", imgSrc);

                    })
                }

            },

            pointCards: {
                parse: function (pointCardsData) {
                    var validPointCards = [];

                    $.each(pointCardsData, function (pointCardIndex, pointCardData) {
                        // if (pointCardData.IsActive) {
                        if (pointCardData.LastStampDate) {
                            pointCardData.parsedLastStampDate = pointCardData.LastStampDate.substr(0, 10);
                        }
                        pointCardData.slotsFilled = pointCardData.NumberOfSlots - pointCardData.NumberOfSlotsRemaining;
                        validPointCards.push(pointCardData);
                        // }
                    });

                    if (validPointCards.length) {
                        voyado.myPages.campaigns.pointCards.render(validPointCards);
                    }
                    else {
                        console.log("No active point cards to show.");
                    }
                },
                render: function (validPointCards) {

                    // console.log("validPointCards: ");
                    // console.log(validPointCards);
                    var itemWrapper = $("#my-pages-landing-page-item-wrapper");
                    var template = J.views['voyado-integration/point-cards'];
                    var html = template(validPointCards);
                    itemWrapper.prepend(html).removeAttr("class").addClass("item-qty-" + itemWrapper.children().length);
                    voyado.myPages.campaigns.pointCards.conclude();
                },
                conclude: function () {

                    $(".voyado-point-cards li").each(function () {
                        var pointCard = $(this);

                        // FURTHER FUNCTIONALITY NOT IMPLEMENTED
                    })
                }
            }

        },
    },
    api: {
        getCampaigns: function (callback, callbackOptions, enableCache, cacheMinutes) {
            // customerLists.helpers.overlay.show();
            var service = "/customercampaigns?encryptedCustomerIdentifier=" + voyado.customerData.Identifier;
            J.api.helpers.getter(service, function (data) {
                // customerLists.helpers.overlay.hide();
                if (callback) {
                    callback(data);
                }
            }, callbackOptions, enableCache, cacheMinutes);
        }


    },
    helpers: {
        checkIfMyPages: function () {
            if ($("html[class^='page-responsivemypages']").length) {
                return true;
            }
            else {
                return false;
            }
        },
        selectDiscountOfferData: function (internalData, externalData) {
            var returnData = null;

            if (voyado.config.discountOffersExternalDataPreferred) {
                if (externalData) {
                    returnData = externalData;
                }
                else if (internalData) {
                    returnData = internalData;
                }
            }
            else {

                if (internalData) {
                    returnData = internalData;
                }
                else if (externalData) {
                    returnData = externalData
                }
            }
            return returnData;
        },
        checkIfDiscountOfferIsActive: function (offerData) {
            var extData = offerData.ExternalDiscountOffer;
            var intData = offerData.InternalDiscountOffer;
            var offerIsActive = false;

            if (extData && intData && !extData.Redeemed) {
                offerIsActive = true;
            }
            return offerIsActive;
        },

        addTranslations: function () {

            J.translations.push({
                voyadoDynamicCartItemMemberPriceAlert: {
                    sv: "Den här produkten har ett medlemspris!",
                    en: "This item has a member price!"
                },
                voyadoDynamicCartItemMemberPriceInfo: {
                    sv: "Medlemspris",
                    en: "Member price"
                },
                voyadoDynamicCartItemMemberPriceLogin: {
                    sv: "Logga in/bli medlem",
                    en: "Log in/become a member"
                },
                voyadoDiscountOffersHeader: {
                    sv: "Kunderbjudanden",
                    en: "Discount offers"
                },
                voyadoDiscountOfferValidUntil: {
                    sv: "Giltigt t.o.m.",
                    en: "Valid until"
                },
                voyadoDiscountOfferReadMore: {
                    sv: "Läs mer",
                    en: "Read more"
                },
                voyadoPointCardsHeader: {
                    sv: "Aktiva bonuskort",
                    en: "Active point cards"
                },
                voyadoPointCardsLastStampDate: {
                    sv: "Senaste stämpel",
                    en: "Last stamp"
                },
                voyadoPointCardsNoMoreSlots: {
                    sv: "Kortet är fullt!",
                    en: "Your point card is full!"
                },
            });
        },
        registerHandlebarHelpers: function () {
            Handlebars.registerHelper('times', function (n, block) {
                var accum = "";
                for (var i = 0; i < n; ++i)
                    accum += block.fn(_.extend({}, {i: i, iPlus1: i + 1}));
                return accum;
            });
        }
    }
};

J.pages.addToQueue("all-pages", voyado.init);

