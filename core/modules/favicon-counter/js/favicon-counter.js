var faviconCounter = {
    config: {
        animation: "slide", //slide, fade, pop, popFade, none
        position: "down",      //Badge position (up, down, left, upleft)
        bgColor : "#f30",
        textColor : "#FFF",
        fontStyle: "bold", //normal, italic, oblique, bold, bolder, lighter, 100, 200, 300, 400, 500, 600, 700, 800, 900
        fontFamily: "sans-serif" // CSS values
    },
    cartItems: 0,

    init: function () {

        // FIX MULTIPLE FAVICON ELEMENTS
        var faviconIconLinks = $("head link[rel='icon']");
        if(faviconIconLinks.length) {
            faviconCounter.config.element = faviconIconLinks.get();
        }

        window.faviconObj = new Favico(faviconCounter.config);

        // Bind event
        $(window).on('cart-updated', function () {
            faviconCounter.onCartUpdate();
            faviconCounter.updateIcon();
        });

        faviconCounter.updateIcon();
    },
    updateIcon: function () {
        window.faviconObj.badge(faviconCounter.cartItems);
    },
    onCartUpdate: function () {
        faviconCounter.cartItems = J.cart.ItemCount;
    }
};

J.pages.addToQueue("all-pages", faviconCounter.init);
