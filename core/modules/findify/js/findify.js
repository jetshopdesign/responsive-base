var findify = {
	initAllPages: function() {
		$(window).on('cart-updated', function() {
			findify.render.updateCart();
		});
	},
	initProductPage: function() {
		findify.render.productPage();
	},
	initConfirmationPage: function() {
		findify.render.orderConfirmation();
	},
	render: {
		updateCart: function() {
			var line_items = [];
			$.each(J.cart.ProductsCart, function(i, product) {
				line_items.push({
					item_id: product.ProductId,
					unit_price: findify.helpers.formatPrice(product.ItemPrice),
					quantity: product.Quantity
				});
			});

			// Update cart event
			var request = {
				line_items: line_items
			};

			var interval = setInterval(function() {
				if(typeof window.findifyAnalyticsInstance !== "undefined") {
					findifyAnalyticsInstance.sendEvent('update-cart', request);
					clearInterval(interval);
				}
			}, 250);
		},
		productPage: function() {
			// View page event
			var request = { 
				item_id: J.data.productId
			};
			
			var interval = setInterval(function() {
				if(typeof window.findifyAnalyticsInstance !== "undefined") {
					findifyAnalyticsInstance.sendEvent('view-page', request);
					clearInterval(interval);
				}
			}, 250);
    	},
		orderConfirmation: function() {
			var lineItems = [];
			var totalLineItemPrices = 0;
			$.each(dataLayer[0].transactionProducts, function(i, product) {
				lineItems.push({
					item_id: product.id,
					unit_price: findify.helpers.formatPrice(product.price),
					quantity: product.qty
				});

				totalLineItemPrices += (findify.helpers.formatPrice(product.price) * product.qty);
			});

			// Purchase event
			var request = {
				order_id: dataLayer[0].transactionId,
				currency: dataLayer[0].transactionCurrency,
				revenue: findify.helpers.formatPrice(dataLayer[0].transactionValue),
				total_shipping: findify.helpers.formatPrice(dataLayer[0].transactionShipping),
				// total_tax: findify.helpers.formatPrice(dataLayer[0].transactionVat),
				total_discount: totalLineItemPrices + findify.helpers.formatPrice(dataLayer[0].transactionShipping) - findify.helpers.formatPrice(dataLayer[0].transactionValue),
				line_items: lineItems
			};

			console.log(request);

			var interval = setInterval(function() {
				if(typeof window.findifyAnalyticsInstance !== "undefined") {
					findifyAnalyticsInstance.sendEvent('purchase', request);
					clearInterval(interval);
				}
			}, 250);
		}
	},
	helpers: {
		formatPrice: function(price) {
			price = parseFloat(price.toString().replace(",","."));
			return parseFloat((Math.round(price*100)/100).toFixed(2));
		}
	}
};

J.pages.addToQueue("all-pages", findify.initAllPages);
J.pages.addToQueue("product-page", findify.initProductPage);
J.pages.addToQueue("orderconfirmation-page", findify.initConfirmationPage);