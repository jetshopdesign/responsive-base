var subCategories = {
    config: {
        showImages: false
    },
    catId: J.data.categoryId,
    callbackObject: {
        success: function (data, callbackOptions, textStatus, jqXHR) {
            subCategories.render(data, callbackOptions);
        }
    },
    init: function (catId) {
        J.api.category.get(subCategories.callbackObject, subCategories.catId, true, 10, subCategories.catId, true);
    },
    render: function (data) {
        data = data[0];
        data.HasImages = false;
        if (data["SubCategories"].length > 0) {
            this.attachPlaceholder();
            this.spinnerShow();
            // Do we have images?
            if (this.config.showImages) {
                for (var i = 0; i < data["SubCategories"].length; i++) {
                    if (data["SubCategories"][i]["ImageUrl"]["Url"].length > 0) {
                        data.HasImages = true;
                    }
                }
            } else {
                data.HasImages = false
            }
            // Render using template
            var template = J.views['subcategories/subcategories'];
            var html = template(data);
            $("#subcategories-placeholder").append(html);
            // Show once all images are downloaded to client
            if (data.HasImages) {
                var $images = $('#subcategories-placeholder').find('img');
                var loaded_images_count = 0;
                $images.load(function () {
                    loaded_images_count++;
                    if (loaded_images_count == $images.length) {
                        subCategories.show();
                    }
                });
            } else {
                subCategories.show();
            }
        }
    },
    show: function () {
        subCategories.spinnerHide();
        $("#subcategories-container").show();
        $('.subcategories-image').matchHeight(J.config.sameHeightConfig);
        $('.subcategories-name').matchHeight(J.config.sameHeightConfig);
    },
    spinnerShow: function () {
        $(".category-header-wrapper").after('<div id="subcategories-spinner"><i class="fa fa-spin fa-spinner fa-2x"></i></div>');

    },
    spinnerHide: function () {
        $("#subcategories-spinner").hide();
    },
    attachPlaceholder: function () {
        $(".category-header-wrapper").after('<div id="subcategories-placeholder"></div>');
    }
};

J.pages.addToQueue("category-page", subCategories.init);
J.pages.addToQueue("category-advanced-page", subCategories.init);
