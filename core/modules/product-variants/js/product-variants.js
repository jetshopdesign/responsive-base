var productVariants = {
    config: {
        insertAfterElementSelector: ".purchase-block",
        separationCriterion: "articleNumber", // "articleNumber", "productName"
        articleNumberSeparator: "-", // The character that separates model from variant
        useLastSeparator: true, // Use the last instance of separator character. False means first instance is used.
        hideProductsWithoutImage: true, // Use this for hiding produts without images.
        rowQuantities: { // How many items in every rendered row
            small: 3,
            medium: 4,
            large: 5,
            xlarge: 5
        },
        translations: {
            variantHeader: {
                sv: "Färgvarianter",
                nb: "Fargevarianter",
                da: "Farvevarianter",
                fi: "Värivaihtoehtoja",
                de: "Farbvarianten",
                en: "Color variants",
                et: "Värvi variandid"
            }
        }
    },
    currentModelIdentifier: "",
    searchData: {},
    init: function () {
        productVariants.addTranslations();
        productVariants.setSearchString();
        productVariants.findVariants();
    },
    setSearchString: function () {
        if (productVariants.config.separationCriterion == "articleNumber") {
            var articleNumber = $('.product-article-number-value').attr('content').trim();
            productVariants.currentModelIdentifier = productVariants.getModelIdentifier(articleNumber);
        }
        else if(productVariants.config.separationCriterion == "productName") {
            productVariants.currentModelIdentifier = $(".product-page-header").text().trim().toLowerCase();
        }
    },
    findVariants: function () {
        if(productVariants.currentModelIdentifier !== "") {
            J.api.search(productVariants.callbackObject, "", true, 5, productVariants.currentModelIdentifier, true, 500, 1);
        }
    },
    callbackObject: {
        success: function (data, callbackOptions, textStatus, jqXHR) {
            if(data.TotalProducts) {
                productVariants.searchData = data;

                // ADD DATA FOR RENDER
                productVariants.searchData.variantHeader = J.translate("variantHeader");
                productVariants.searchData.rowQuantities = productVariants.config.rowQuantities;

                productVariants.filterSearchData();
                if(productVariants.searchData.ProductItems.length > 0) {
                    productVariants.renderVariants();
                }
            }
        }
    },
    filterSearchData: function () {
        var searchData = jQuery.extend(true, {}, productVariants.searchData);
        var i = searchData.ProductItems.length;

        // LOOP BACKWARDS AND GET RID OF PRODUCTS THAT ARE NOT VARIANTS OF CURRENT PRODUCT
        while(i--) {
            var prodData = searchData.ProductItems[i];
            var prodId = prodData.Id;
            var articleNumber = prodData.Articlenumber.trim();
            var mainName = prodData.Name.trim().toLowerCase();

            // DELETE CURRENT PROD FROM SEARCH LIST
            if(prodId === J.data.productId) {
                searchData.ProductItems.splice(i, 1);
            }
            // DELETE THE PRODUCT IF THE IMAGE SETTINGS IS SET TO TRUE AND THE PRODUCT HAS NO IMAGES
            else if (productVariants.config.hideProductsWithoutImage) {
                if (prodData.Images.length === 0) {
                    searchData.ProductItems.splice(i, 1);
                }
            }
            // ARTICLE NUMBER SEPARATION
            else if (productVariants.config.separationCriterion == "articleNumber") {

                // IF MODEL IDENTIFIER DOES NOT MATCH THIS PRODUCT'S IDENTIFIER, REMOVE
                if(productVariants.currentModelIdentifier !== productVariants.getModelIdentifier(articleNumber)) {
                    searchData.ProductItems.splice(i, 1);
                }
            }
            // PRODUCT NAME NUMBER SEPARATION
            else if (productVariants.config.separationCriterion == "productName") {

                // IF MODEL IDENTIFIER DOES NOT MATCH THIS PRODUCT'S IDENTIFIER, REMOVE
                if(productVariants.currentModelIdentifier !== mainName) {
                    searchData.ProductItems.splice(i, 1);
                }
            }
        }

        // PUT FILTERED DATA ARRAY BACK
        productVariants.searchData = searchData;

    },
    getModelIdentifier: function (articleNumber) {
        var modelIdentifier;
        if (productVariants.config.useLastSeparator) {
            modelIdentifier = articleNumber.substr(0, articleNumber.lastIndexOf(productVariants.config.articleNumberSeparator));
        }
        else {
            modelIdentifier = articleNumber.substr(0, articleNumber.indexOf(productVariants.config.articleNumberSeparator));
        }
        return modelIdentifier;
    },
    renderVariants: function () {
        var data = productVariants.searchData;
        if(data.TotalProducts) {
            var template = J.views['product-variants/product-variants'];
            var html = template(data);
            $(productVariants.config.insertAfterElementSelector).after(html);
        }
    },
    addTranslations: function(){
        J.translations.push(productVariants.config.translations);
    }
};

J.pages.addToQueue("product-page", productVariants.init);