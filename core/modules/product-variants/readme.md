# Product variants
This module enables a list of product variants to be displayed on the product page. This can be useful for when a product model is split over several separate products, for example when a specific shirt model that exists in several colors, where each color is a separate product. The module will make an API search for matching products, and filter out those that match the current model (and that is not the current product) and display them at a place of your choosing.

Please note that this module requires some interaction with the store owner to make sure they are aware of (and OK with) the method of defining models and variants, see below. The store owner must strictly implement the product naming scheme you agree on for the module to work properly.

## How to define what products belong together
There are two main ways of determining what products belong to the same model and are variants of each other:

### 1. Article number (`separationCriterion: "articleNumber"`)
With this method the module will analyze the article number of the current product, to find out what model it belongs to. A separator character (`config.articleNumberSeparator`) is used to define where to break down the article number into a model identifier. 

For example, the articlenumber "ABC-123". If you define the separator character as "-", the model identifier would be "ABC". An API search will be made with the search string "ABC". All returning product articlenumbers will be analyzed, and those that have the exact model identifier "ABC" (that are not the currently viewed product) will be considered variants and will be rendered. 

There is also an option to define if the algorithm should use the last instance of the separator character or the first. This way an article number could be "ABC-XYZ-123" and depending on the store owner's wishes either "ABC" could be the model number, or "ABC-XYZ"". This way it is easier to accommodate for various article number generation schemes.

### 2. Product name (`separationCriterion: "productName"`)
This option means that the product main name is the model identifier. All products with the same main name are considered as being variants of each other. This scheme will not work very well in stores with many products that have generic terms in the product name. For example, if the product name is "Calypso shorts" and there are hundreds of other product models with the term "shorts" in their name, the API search will yield an exorbitant number of hits, which is slow to transfer and process. 

Use this option only if the article number scheme will not work out for some reason or other.