var kcoV3Currency = {
  config: {
    cookieName: "kcov3-currency-autochanged",
    allowOnlyOneAutoChange: true
  },
  init: function () {
    if ($(".klarna-checkout-v3-header").length) {

      // CHECK SO THAT CURRENCY SELECTORS ARE SHOWING CORRECT CURRENCY (BUGGY .NET BACKEND CODE)
      var currencySelectors = $(".currency-selector-wrapper select");
      currencySelectors.each(function () {
        var currencySelector = $(this);

        // GIVE OPTIONS VALUES TO BE ABLE TO CHANGE THEM PROPERLY
        currencySelector.find("option").each(function () {
          var option = $(this);
          option.attr("value", option.text());
        });
        currencySelector.val(JetshopData.Currency);
      });


      // BIND CHECK TO KCO UPDATE
      Checkout.customerChangedEventHandlers.push(kcoV3Currency.countryUpdatedEvent);
    }
  },
  countryUpdatedEvent: function (data) {

    // ONLY MATCH IF NOT ALREADY MATCHED
    if (!kcoV3Currency.config.allowOnlyOneAutoChange || (getCookie(kcoV3Currency.config.cookieName) !== "true")) {

      var countryCode = data.buyerAddresses.countryCodeIso;
      var matchedCurrency;

      if (countryCode) {
        matchedCurrency = kcoV3Currency.countryList[countryCode];

        if (matchedCurrency === "") {
          matchedCurrency = "EUR";
        }
        else if (matchedCurrency === undefined) {
          matchedCurrency = "USD";
        }

        // WAIT A LITTLE TO AVOID KLARNA CONFLICT
        setTimeout(function () {
          kcoV3Currency.switchToCurrency(matchedCurrency);
        }, 500);

      }
    }
  },
  switchToCurrency: function (countryCurrency) {

    var activeCurrency = JetshopData.Currency;
    if (countryCurrency !== activeCurrency) {

      var channelCurrencies = JetshopData.ChannelInfo.Data[JetshopData.ChannelInfo.Active].Currencies;

      if ($.inArray(countryCurrency, channelCurrencies) > -1) {
        // SET COOKIE SO AS TO NOT AUTO CHANGE MORE THAN ONCE
        setCookie(kcoV3Currency.config.cookieName, "true", 0.02);
        Services.general.SetDisplayCurrency(countryCurrency);
      }
    }
  },
  countryList: { // IF IN LIST W/O SPECIFIED CURRENCY = EUR, NOT IN LIST = USD
    alb: "",
    and: "",
    arm: "",
    aus: "",
    aze: "",
    blr: "",
    bel: "",
    bih: "",
    bgr: "",
    hrv: "",
    cyp: "",
    cze: "",
    dnk: "DKK",
    est: "",
    fin: "",
    fra: "",
    geo: "",
    deu: "",
    grc: "",
    hun: "",
    isl: "",
    irl: "",
    ita: "",
    kaz: "",
    lva: "",
    lie: "",
    ltu: "",
    lux: "",
    mlt: "",
    mco: "",
    mda: "",
    mne: "",
    mkd: "",
    nld: "",
    nor: "NOK",
    pol: "",
    prt: "",
    rus: "",
    smr: "",
    srb: "",
    svk: "",
    svn: "",
    esp: "",
    swe: "SEK",
    che: "",
    tur: "",
    ukr: "GBP",
    gbr: "",
  }
};

$(document).ready(function () {
  if ($(".page-responsive-checkout").length) {
    kcoV3Currency.init();
  }
});





