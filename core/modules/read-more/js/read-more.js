var readMore = {
    elementsToTrim: '.category-header-subtitle',    // Can be several classes or ID's, separate with comma ","
    readMoreText: '(Läs mer)',
    readLessText: '(Läs mindre)',
    trimLength: 170,                                // Number of characters before trim
    trimMargin: 1.25,                                // Offset to not just trim a few words

    init: function () {
        readMore.translations()

        readMore.trim();
        readMore.bindClick();        
    },
    translations: function() {
        J.translations.push({
            'readMoreText': {
                'sv': '(Läs mer)',
                'en': '(Read more)'
            },
            'readLessText': {
                'sv': '(Läs mindre)',
                'en': '(Read less)'
            }
        })
    },
    trim: function () {
        $(readMore.elementsToTrim).each(function () {
            var $this = $(this),
                trimLength = readMore.trimLength,
                trimMargin = readMore.trimMargin;

            if ($this.text().length > (trimLength * trimMargin)) {
                var text = $this.html(),
                    trimPoint = $this.html().indexOf(" ", trimLength),
                    newContent =
                        '<span class="initial-text">'
                        + text.substring(0, trimPoint)    // Get the visible bit of the text
                        + '</span>'
                        + '<span class="read-more">'
                        + text.substring(trimPoint)     // Get the rest of the text
                        + '</span><span class="toggle"><a href="#">'
                        + J.translate('readMoreText')
                        + '</a></span>';

                $this.html(newContent);
            }
            $this.addClass('trimmed');
        });
    },
    bindClick: function () {
        $('.toggle a').on('click', function (e) {
            e.preventDefault();
            var $this = $(this),
                textElement = $this.closest(readMore.elementsToTrim);

            textElement.toggleClass('toggled');

            if (textElement.hasClass('toggled')) {
                textElement.find('.toggle a').text(J.translate('readLessText'));
            } else {
                textElement.find('.toggle a').text(J.translate('readMoreText'));
            }
        });
    }
};

J.pages.addToQueue('category-page', readMore.init);