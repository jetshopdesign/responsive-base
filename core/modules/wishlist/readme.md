#Wishlist (favorites)

##General info
The wishlist module is a streamlined, pared-down implementation of the list functionality. The main differences are:

- Works both when logged in and not logged in
- Data stored in local storage when not logged in
- When logged in, data is synced to a backend user list
- No multiple list handling (only one user list is ever used, the first available)
- Only saves product id:s, not attributes, comments etc (in the base version at least, this might change)
- You can share the list with others (through url-based product id list when not logged in, through user list id when logged in)


## Requirements
Needs Responsive Base with module and Handlebars support (which would be 0.8.5 with the compatibility script, I think). Supposed to be robust and standalone, but it is possible some feature will cause an error on old versions of RB. Should be easy to fix though, has long as modules and Handlebars work. 

The store needs at least one active list type for the module to work. Its name does not matter. The support team can set this up.

If FontAwesome icons are missing you will find the latest version of FA in the /files directory in this module. Just replace the font-awesome.min.css file in /stage/css together with all the font files in /fonts.

##Implementation
Activate the module. It should start working immediately if the requirements are met. 

##Settings
If needed, change the settings for the selectors that determine where the module elements are inserted. If needed, tweak the design. 

IMPORTANT: Remember to change and add the store name for the different languages in the settings.

Any questions can be directed to jan.sjogren@jetshop.se.

##Current limitations
- Products with specifications cannot be added directly from the favorites list interface. 
- Products with comments cannot be added directly from the favorites list interface. 
- Package products cannot be detected at the moment, and it will be possible to add them from the list interface (this should not be possible). This will be fixed backend, not sure when.

The idea is to make clients interested in any of the above unsupported features pay for their development.
