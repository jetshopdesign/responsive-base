var wishlist = {
    settings: {
      storeName: {
        sv: "Testbutiken",
        en: "The Test Store",
        nb: "Test",
        da: "Test",
        fi: "Test"
      },
      ui: {
        insertWishlistIconBefore: ".small-cart-wrapper",
      },
      startPage: {
        insertListBtnAfter: ".product-image a"
      },
      categoryPage: {
        insertListBtnAfter: ".product-image a"
      },
      categoryAdvancedPage: {
        insertListBtnAfter: ".product-advanced-image a"
      },
      productPage: {
        insertListBtnAfter: ".show-product-image img"
      },
      releware: {
        insertListBtnAfter: ".product-image a"
      },
      listing: {
        insertListBtnAfter: ".product-image"
      },
      decimalPlaces: { // HOW MANY DECIMALS IN PRICES PER CHANNEL
        channel_1: 0,
        channel_2: 0,
        channel_3: 0,
        channel_4: 0,
        channel_5: 0
      }
    },
    listContainer: null,
    data: {
      listData: {
        name: null,
        products: [],
      },
      isVatIncluded: J.checker.isVatIncluded,
      isLoggedIn: J.checker.isLoggedIn,
      didJustLogIn: null,
      navigatorShareApiSupported: (typeof navigator.share !== "undefined"),
      wishlistLinkUrl: "",
      customerId: null,
      apiList: [],
      apiListId: [],
      isExternalList: false,
      externalListId: null,
      storeName: null,
    },
    init: function () {

      // ADD CUSTOMER ID
      if (J.checker.isLoggedIn && JetshopData.Customer && typeof JetshopData.Customer.Identifier !== "undefined") {
        wishlist.data.customerId = JetshopData.Customer.Identifier;
      }

      // CHECK LOGGED IN STATUS
      wishlist.helpers.checkLoginStatus();

      // ADD GENERAL CLASS
      $("html").addClass("wishlist-active");

      // HELPERS ETC
      wishlist.translations();
      wishlist.helpers.overlay.init();
      wishlist.helpers.currencyDisplay.init();
      wishlist.data.urls = JetshopData.Urls;

      // STORE NAME
      wishlist.data.storeName = wishlist.settings.storeName[J.data.language];

      // CHECK FOR EXTERNAL LIST
      wishlist.externalList.checkUrlForExternalList();

      // INSERT WISHLIST CONTAINER
      wishlist.render.wishlistContainer();

      // INSERT WISHLIST ICON
      wishlist.render.wishlistIcon();

      // GET DATA FROM STORAGE
      wishlist.dataHandling.checkStoredData();

      // BIND RESIZE
      wishlist.ui.events.bindWindowResize();

      // INITIALIZE CORRECT PAGE TYPE FUNCTIONS
      if (J.checker.isProductPage) {
        wishlist.productPage.init();
      }
      else if (J.checker.isCategoryPage) {
        wishlist.categoryPage.init();
      }
      else if (J.checker.isCategoryAdvancedPage) {
        wishlist.categoryAdvancedPage.init();
      }
      else if (J.checker.isStartPage) {
        wishlist.startPage.init();
      }

    },
    categoryPage: {
      init: function () {
        wishlist.categoryPage.enableAddToListFunction();
      },
      enableAddToListFunction: function () {

        if (typeof dynamicFilterGuiTop !== "undefined") {

          if ($(".dynamic-filter-products .product-outer-wrapper").length) {
            wishlist.ui.insertAddToListBtns();
          }
          $(document).on("dynamicFilter:products::completed", function () {
            wishlist.ui.insertAddToListBtns();
            wishlist.ui.markWishlistedProducts();
          });
        }
        else {
          wishlist.ui.insertAddToListBtns();
          wishlist.ui.markWishlistedProducts();
        }
        wishlist.ui.events.bindAddToListBtns();
      },

    },
    categoryAdvancedPage: {
      init: function () {
        wishlist.categoryAdvancedPage.enableAddToListFunction();
      },
      enableAddToListFunction: function () {

        if (typeof dynamicFilterGuiTop !== "undefined") {

          if ($(".dynamic-product-content .product-advanced-row").length) {
            wishlist.ui.insertAddToListBtns();
          }
          $(document).on("dynamicFilter:products::completed", function () {
            wishlist.ui.insertAddToListBtns();
            wishlist.ui.markWishlistedProducts();
          });
        }
        else {
          wishlist.ui.insertAddToListBtns();
          wishlist.ui.markWishlistedProducts();
        }
        wishlist.ui.events.bindAddToListBtns();
      },

    },
    productPage: {
      init: function () {
        wishlist.ui.insertAddToListBtns();
        wishlist.ui.events.bindAddToListBtns();
        wishlist.ui.markWishlistedProducts();

      },
    },
    startPage: {
      init: function () {
        wishlist.ui.insertAddToListBtns();
        wishlist.ui.events.bindAddToListBtns();
        wishlist.ui.markWishlistedProducts();

      },
    },
    externalList: {
      checkUrlForExternalList: function () {
        var isList = getUrlParameters()["list"];
        var listId = getUrlParameters()["id"];
        var prods = getUrlParameters()["prods"];

        if (isList) {
          if (listId) {
            if (listId.length == 36) {

              // IS (PROBABLY) VALID LIST ID - SET VAR & CONTINUE
              wishlist.data.isExternalList = true;
              wishlist.data.externalListId = listId;
              wishlist.externalList.getExternalList();
            }
          }
          else if (prods) {
            var prodList = prods.split("+");
            if (prodList.length) {
              $.each(prodList, function (index, listProdId) {
                var id = parseInt(listProdId);
                if (typeof id === "number") {
                  wishlist.data.listData.products.push({
                    ProductId: id
                  });
                }
              });
              if (wishlist.data.listData.products.length) {
                wishlist.data.isExternalList = true;
                setTimeout(function () {
                  wishlist.dataHandling.getProductData();
                }, 0);
              }
            }
          }
          if (!wishlist.data.isExternalList) {
            wishlist.externalList.showInvalidExternalListAlert();
          }
        }
      },
      getExternalList: function () {
        wishlist.api.userLists.getSingle(wishlist.data.externalListId, wishlist.externalList.storeExternalListData, null, false, 0);
      },
      storeExternalListData: function (listData) {

        if (listData) {
          wishlist.data.listData.name = listData.Name;
          $.each(listData.Products, function (prodIndex, prodData) {
            wishlist.data.listData.products.push({
              ProductId: prodData.ProductId
            });
          });
          wishlist.dataHandling.getProductData();
        }
        else {
          wishlist.externalList.showInvalidExternalListAlert();
          wishlist.externalList.exitExternalListMode();
        }

      },
      showInvalidExternalListAlert: function () {
        var template = J.views['wishlist/alert-invalid-external-list-id'];
        var html = template();
        $.fancybox.open({
          content: html
        });
        $(".wishlist-alert .button").click(function () {
          $.fancybox.close();
        });
      },
      exitExternalListMode: function () {
        wishlist.listContainer.removeClass("is-external");
        wishlist.data.isExternalList = false;
        wishlist.dataHandling.resetListData();
        wishlist.render.wishlistContainer();
        wishlist.dataHandling.checkStoredData();
      }
    },
    ui: {
      openWishlist: function () {
        wishlist.listContainer.addClass("open");
        $("body").addClass("wishlist-container-open");

      },
      closeWishlist: function () {
        wishlist.listContainer.removeClass("open");
        $("body").removeClass("wishlist-container-open");

        setTimeout(function () {
          $(".exit-add-to-cart-mode").click();
          $(".exit-sharing-mode").click();

          if (wishlist.data.isExternalList) {
            wishlist.externalList.exitExternalListMode();
          }
        }, 300);


      },
      calculateMainAreaPositioning: function () {
        var header = wishlist.listContainer.find(".wishlist-header");
        var footer = wishlist.listContainer.find(".wishlist-footer");
        var productListContainer = wishlist.listContainer.find(".product-list-container");
        var productList = wishlist.listContainer.find(".wishlist-products");
        var sharingContainer = wishlist.listContainer.find(".sharing-container");
        var sharingContent = wishlist.listContainer.find(".sharing-content");

        var winHeight = $(window).height();
        var headerHeight = header.outerHeight();
        var footerHeight = footer.outerHeight();
        var productListHeight = productList.outerHeight();
        var sharingContentHeight = sharingContent.outerHeight();

        // console.log("winHeight: ", winHeight);
        // console.log("headerHeight: ", headerHeight);
        // console.log("footerHeight: ", footerHeight);
        // console.log("productListHeight: ", productListHeight);

        // PRODUCT LIST
        productListContainer.css("top", headerHeight);
        productListContainer.css("bottom", footerHeight);
        if ((winHeight - headerHeight - footerHeight) < productListHeight) {
          productListContainer.addClass("overflow");
        }
        else {
          productListContainer.removeClass("overflow");
        }

        // SHARING CONTENT
        sharingContainer.css("top", headerHeight);
        sharingContainer.css("bottom", footerHeight);
        if ((winHeight - headerHeight - footerHeight) < sharingContentHeight) {
          sharingContainer.addClass("overflow");
        }
        else {
          sharingContainer.removeClass("overflow");
        }
      },
      insertAddToListBtns: function (listElement) {

        var template = J.views['wishlist/wishlist-add-to-list-button'];
        var html = template();


        if (listElement) {
          $(listElement + " .product-outer-wrapper").each(function () {
            var prod = $(this);
            if (!prod.find(".add-to-list").length) {
              prod.find(wishlist.settings.listing.insertListBtnAfter).after(html);
            }
          });
        }
        else {
          if (J.checker.isProductPage) {
            $(wishlist.settings.productPage.insertListBtnAfter).after(html);
          }
          else if (J.checker.isCategoryPage) {
            $(".product-outer-wrapper").each(function () {
              var prod = $(this);
              if (!prod.find(".add-to-list").length) {
                prod.find(wishlist.settings.categoryPage.insertListBtnAfter).after(html);
              }
            });
          }
          else if (J.checker.isCategoryAdvancedPage) {
            $(".product-advanced-row").each(function () {
              var prod = $(this);
              if (!prod.find(".add-to-list").length) {
                prod.find(wishlist.settings.categoryAdvancedPage.insertListBtnAfter).after(html);
              }
            });
          }
          else if (J.checker.isStartPage) {
            $(".startpage-product-item").each(function () {
              var prod = $(this);
              if (!prod.find(".add-to-list").length) {
                prod.find(wishlist.settings.startPage.insertListBtnAfter).after(html);
              }
            });
          }
        }

        // RELEWARE
        $(".releware-recommendation-wrapper .product-outer-wrapper").each(function () {
          var prod = $(this);
          if (!prod.find(".add-to-list").length) {
            prod.find(wishlist.settings.releware.insertListBtnAfter).after(html);
          }
        });
        wishlist.ui.releware.init();

      },
      releware: {
        init: function () {
          var relewareWrappers = $(".releware-recommendation-wrapper");
          if (relewareWrappers.length) {
            relewareWrappers.each(function () {
              var relewareWrapper = $(this);
              var mutationTimeout;
              var runDependencies = function () {
                clearTimeout(mutationTimeout);
                mutationTimeout = setTimeout(function () {
                  observation.stop();
                  wishlist.ui.markWishlistedProducts();
                }, 500);
              }

              var observation = {
                targetNode: relewareWrapper[0],
                observer: null,
                config: {
                  attributes: false,
                  childList: true,
                  characterData: false,
                  subtree: true
                },
                run: function () {
                  if (!observation.observer) {
                    observation.observer = new MutationObserver(runDependencies);
                  }
                  observation.observer.observe(observation.targetNode, observation.config);
                },
                stop: function () {
                  observation.observer.disconnect();
                }
              }

              if (relewareWrapper.find(".releware-recommendation-header").text().match("%")) {
                observation.run();
              }
            });
          }
        },
      },
      updateListProdQtyIndicator: function () {
        var prodQty = wishlist.data.listData.products.length;
        var wishlistProdQtyIndicator = $(".wishlist-activator .product-quantity");
        if (prodQty > 0) {
          wishlistProdQtyIndicator.html(prodQty);
        }
        else {
          wishlistProdQtyIndicator.html("");
        }
      },
      checkForEmptyProductList: function () {
        if (wishlist.data.listData.products.length) {
          wishlist.listContainer.removeClass("no-products");
          wishlist.listContainer.find(".wishlist-footer .button").removeClass("button-disabled");
        }
        else {
          wishlist.listContainer.addClass("no-products");
          wishlist.listContainer.find(".wishlist-footer .button").not(".exit-add-to-cart-mode").addClass("button-disabled");
          wishlist.helpers.switchMode("add-to-cart-mode", "default-mode");
          wishlist.render.productList();
        }
      },
      checkForNoSelectedProducts: function () {
        if (wishlist.data.listData.products.length) {
          if ($(".wishlist-products .product-item.item-selected").length) {
            $(".wishlist-add-to-cart").removeClass("button-disabled");
          }
          else {
            $(".wishlist-add-to-cart").addClass("button-disabled");
          }
        }
      },
      disableValidationAlertsForProd: function (prodJqObj) {
        prodJqObj.removeClass("validation-error").find(".validation-error").removeClass("validation-error");

        if (!prodJqObj.siblings(".validation-error").length) {
          wishlist.ui.disableAllValidationAlerts();
        }
        wishlist.ui.calculateMainAreaPositioning();
      },
      disableAllValidationAlerts: function () {
        wishlist.listContainer.removeClass("validation-error").find(".validation-error").removeClass("validation-error");
        wishlist.ui.calculateMainAreaPositioning();
      },
      markWishlistedProducts: function () {
        $(".add-to-list").each(function () {
          var addToListIcon = $(this);
          var foundProdInWishlist = false;
          var prodWrapper;
          var prodId;

          prodWrapper = addToListIcon.closest(".product-outer-wrapper");
          prodId = parseInt(prodWrapper.attr("product-id"));
          if (!prodId) {
            prodId = prodWrapper.data("id");
          }

          if (!prodId) {
            if (addToListIcon.closest(".RWItemTemplate").length) {
              prodWrapper = addToListIcon.closest(".RWItemTemplate");
              prodId = parseInt(prodWrapper.attr("item_id"));
            }
            else if (J.checker.isCategoryPage) {

            }
            else if (J.checker.isCategoryAdvancedPage) {
              prodWrapper = addToListIcon.closest(".product-advanced-wrapper");
              prodId = prodWrapper.data("id");
              if (!prodId) {
                prodWrapper = addToListIcon.closest(".product-advanced-row");
                prodId = prodWrapper.data("id");
              }
            }
            else if (J.checker.isProductPage) {
              prodId = JetshopData.ProductId;
            }
            else if (J.checker.isStartPage) {
              prodId = addToListIcon.closest(".startpage-product-item").find(".product-name a").data("productid");
            }
          }

          // LOOP AGAINST LIST
          if (typeof prodId === "number") {
            $.each(wishlist.data.listData.products, function (prodIndex, prodData) {
              var listProdId = prodData.ProductId;
              if (prodId === listProdId) {
                addToListIcon.addClass("in-list").children("a").attr("title", J.translate("wishlistItemInListBtnTitle"));
                foundProdInWishlist = true;
                return false;
              }
            });
            if (!foundProdInWishlist) {
              addToListIcon.removeClass("in-list").children("a").attr("title", J.translate("wishlistAddToListBtnTitle"));
            }
          }
        });
      },
      showDeleteAllAlert: function () {
        var template = J.views['wishlist/alert-delete-all-products'];
        var html = template();
        $.fancybox.open({
          content: html
        });
        $(".wishlist-alert .button-ok").click(function () {
          wishlist.dataHandling.deleteAllProductsFromList();
          $.fancybox.close();
        });
        $(".wishlist-alert .button-cancel").click(function () {
          $.fancybox.close();
        });
      },
      showLoggedInInfoAlert: function () {
        var template = J.views['wishlist/alert-show-logged-in-info'];
        var html = template();
        $.fancybox.open({
          content: html
        });
        $(".wishlist-alert .button-ok").click(function () {
          $.fancybox.close();
        });
      },
      showNotLoggedInInfoAlert: function () {
        var template = J.views['wishlist/alert-show-not-logged-in-info'];
        var html = template();
        $.fancybox.open({
          content: html
        });
        $(".wishlist-alert .button-ok").click(function () {
          $.fancybox.close();
        });
      },
      showAddingProductsFromApiListAlert: function () {
        var template = J.views['wishlist/alert-adding-prods-from-api-list'];
        var html = template();
        $.fancybox.open({
          content: html
        });
        $(".wishlist-alert .button-ok").click(function () {
          $.fancybox.close();
        });
      },
      events: {
        bindWindowResize: function () {
          $(window).on("resize", function () {
            wishlist.ui.calculateMainAreaPositioning();
          });

        },
        bindWishlistIcon: function () {
          $(".button-wishlist").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            wishlist.ui.openWishlist();
          });
        },
        bindWishlistContainer: function () {

          // BG CLICK TO CLOSE
          wishlist.listContainer.click(function (event) {
            wishlist.ui.closeWishlist();
          });

          // DO NOT CLOSE ON CONTENT CLICK
          $(".wishlist-inner").click(function (event) {
            event.stopPropagation();
          });

          // CLOSE BTN
          $(".wishlist-close").click(function (event) {
            wishlist.ui.closeWishlist();
          });

          // LOGIN INFO
          $(".login-info").click(function (event) {
            if (J.checker.isLoggedIn) {
              wishlist.ui.showLoggedInInfoAlert();
            }
            else {
              wishlist.ui.showNotLoggedInInfoAlert();
            }
          });

          // ENTER ADD TO CART MODE
          $(".enter-add-to-cart-mode").click(function (event) {
            var clickedBtn = $(this);
            if (!clickedBtn.hasClass("button-disabled")) {
              wishlist.helpers.switchMode("default-mode", "add-to-cart-mode", $.fn.matchHeight._update);
            }
          });

          // EXIT ADD TO CART MODE
          $(".exit-add-to-cart-mode").click(function (event) {
            wishlist.helpers.switchMode("add-to-cart-mode", "default-mode", function () {
              $.fn.matchHeight._update();
              wishlist.ui.disableAllValidationAlerts();
            });
          });

          // ENTER SHARING MODE
          $(".enter-sharing-mode").click(function (event) {
            var clickedBtn = $(this);
            if (!clickedBtn.hasClass("button-disabled")) {
              wishlist.helpers.createWishlistLinkUrl();
              wishlist.render.sharing();
              wishlist.helpers.switchMode("default-mode", "sharing-mode", null);
            }
          });

          // EXIT SHARING MODE
          $(".exit-sharing-mode").click(function (event) {
            wishlist.helpers.switchMode("sharing-mode", "default-mode", null);
          });

          // WISHLIST LOGIN
          $(".list-login").click(function (event) {
            setCookie("wishlist-login", "true", 1);
          });

          // DELETE ALL PRODUCTS
          $(".clear-wishlist").click(function (event) {
            var clickedBtn = $(this);
            if (!clickedBtn.hasClass("button-disabled")) {
              wishlist.ui.showDeleteAllAlert();
            }
          });
        },
        bindAddToListBtns: function (containerSelectorString) {

          if (!containerSelectorString) {
            containerSelectorString = "#main-area";
          }

          $(containerSelectorString).on("click", ".button-add-to-list", function (event) {

            var clickedBtn = $(this);
            var prodWrapper;
            var prodId;

            prodWrapper = clickedBtn.closest(".product-outer-wrapper");
            prodId = parseInt(prodWrapper.attr("product-id"));
            if (!prodId) {
              prodId = prodWrapper.data("id");
            }

            if (!prodId) {

              if (clickedBtn.closest(".RWItemTemplate").length) { // RELEWARE
                prodWrapper = clickedBtn.closest(".RWItemTemplate");
                prodId = parseInt(prodWrapper.attr("item_id"));
              }
              else if (J.checker.isCategoryPage) {

              }
              else if (J.checker.isCategoryAdvancedPage) {
                prodWrapper = clickedBtn.closest(".product-advanced-wrapper");
                prodId = prodWrapper.data("id");
                if (!prodId) {
                  prodWrapper = clickedBtn.closest(".product-advanced-row");
                  prodId = prodWrapper.data("id");
                }
              }
              else if (J.checker.isProductPage) {
                prodId = J.data.productId;
              }
              else if (J.checker.isStartPage) {
                prodId = clickedBtn.closest(".startpage-product-item").find(".product-name a").data("productid");
              }

            }

            if (clickedBtn.parent().hasClass("in-list")) {
              wishlist.dataHandling.deleteProductFromList(prodId);
            }
            else {
              wishlist.dataHandling.addProductToList(prodId);
            }
          });
        },
        bindDeleteProductBtn: function () {
          $(".product-list-container").on("click", ".delete-product", function (event) {
            var clickedBtn = $(this);
            var item = clickedBtn.closest(".product-item");
            var prodId = item.data("id");

            item.addClass("fading");
            setTimeout(function () {
              item.remove();
              wishlist.dataHandling.deleteProductFromList(prodId);
            }, 300);
          });
        },
        bindSelectProductCheckbox: function () {
          $(".product-list-container").on("change", ".product-checkbox input[type='checkbox']", function (event) {
            var changedInput = $(this);
            var item = changedInput.closest(".product-item");

            if (changedInput.is(":checked")) {
              item.addClass("item-selected");
            }
            else {
              item.removeClass("item-selected");
              wishlist.ui.disableValidationAlertsForProd(item);
            }
            wishlist.ui.checkForNoSelectedProducts();

          });
        },
        bindAttributes: function () {
          $(".product-list-container").on("change", ".attribute-wrapper select", function (event) {
            var changedSelect = $(this);
            // var isInit = (changedSelect.attr("data-init") == "true");
            var isLv1 = changedSelect.hasClass("attribute-select-lv1");
            var prodItem = changedSelect.closest(".product-item");

            var prodImg = prodItem.find(".product-image img");
            var prodImgSrc = prodImg.attr("src");
            var attrImgSrc = changedSelect.find("option:selected").data("img-url");

            var attrPrice = changedSelect.find("option:selected").data("price");
            var attrDiscountedPrice = changedSelect.find("option:selected").data("discounted-price");
            var attrPriceString = wishlist.helpers.currencyDisplay.parsePriceToString(attrPrice);
            var attrDiscountedPriceString = wishlist.helpers.currencyDisplay.parsePriceToString(attrDiscountedPrice);

            var attrArtNo = changedSelect.find("option:selected").data("article-number");
            var attrArtNoContainer = prodItem.find(".article-number .value");

            var attrStockStatus = changedSelect.find("option:selected").data("stock-status-name");
            var attrStockStatusContainer = prodItem.find(".stock-status .value");


            // IMAGE
            if (attrImgSrc && attrImgSrc !== prodImgSrc) {
              prodImg.attr("src", attrImgSrc);
            }

            // PRICES
            if (attrPrice >= 0) {
              var priceData = {
                price: attrPriceString
              }
              if (attrDiscountedPrice >= 0) {
                priceData.discountedPrice = attrDiscountedPriceString;
              }

              var template = J.views['wishlist/wishlist-attribute-prices'];
              var html = template(priceData);
              prodItem.find(".product-price").html(html);

              // if (isInit) {
              //     prodItem.find(".product-price").data("org-content", html);
              // }
            }

            // ARTICLE NUMBER
            if (attrArtNo) {
              attrArtNoContainer.html(attrArtNo);
              // if (isInit) {
              //     attrArtNoContainer.attr("data-org-content", attrArtNo);
              // }
            }

            // STOCK STATUS
            if (attrStockStatus) {
              attrStockStatusContainer.html(attrStockStatus);
              // if (isInit) {
              //     attrStockStatusContainer.attr("data-org-content", attrStockStatus);
              // }
            }


            // RESOLVE LV2 VISIBILITY
            if (isLv1) {
              var selectOptionIndex = changedSelect.find("option:selected").index() - 1;
              var lv2Selects = prodItem.find(".attribute-select-lv2");
              if (lv2Selects.length) {
                lv2Selects.removeClass("visible").removeAttr("disabled");
                if (selectOptionIndex >= 0) {
                  lv2Selects.eq(selectOptionIndex).attr("data-init", "true").addClass("visible").change();
                }
                else {
                  lv2Selects.eq(0).addClass("visible").val("0").attr("disabled", "disabled");
                }
              }
            }

            // CHECK IF ALL ATTRIBUTES SELECTED
            var selectedValue = null;
            if (prodItem.find(".attribute-wrapper-lv2").length) {
              selectedValue = prodItem.find(".attribute-select-lv2.visible").val();
            }
            else {
              selectedValue = prodItem.find(".attribute-select-lv1").val();
            }
            // console.log("selectedValue: ", selectedValue);
            if (selectedValue) {
              wishlist.ui.disableValidationAlertsForProd(prodItem);
            }

            // REMOVE INIT ATTR IF WAS INITIALIZING EVENT
            // changedSelect.removeAttr("data-init");

          });
        },
        bindAddToCartBtn: function () {
          $(".wishlist-add-to-cart").on("click", function (event) {
            var clickedBtn = $(this);
            if (!clickedBtn.hasClass("button-disabled")) {
              wishlist.cartFunctions.validateSelectedProducts(wishlist.cartFunctions.addSelectedProducts);
            }
          });
        },
        bindSharingInteractions: function () {
          var sharingContainer = $(".sharing-container");

          // COPY LINK URL BUTTON
          sharingContainer.on("click", ".copy-wishlist-url", function (event) {
            event.preventDefault();
            // Select the email link anchor text
            //var emailLink = document.querySelector('.wish-list-url');
            var emailLink = $(this).siblings(".wish-list-url").text();

            // Create temporary inpur and set the value to emailLink
            var $temp = $("<input />");
            $("body").append($temp);
            $temp.val(emailLink).select();
            // Copy and remove the temp input
            try {
              document.execCommand("copy");
            } catch (err) {
              console.error('Copy command could not be executed: ', err);
            }
            $temp.remove();
          });

          // NAVIGATOR SHARE FUNCTIONALITY
          sharingContainer.on("click", ".api-share-btn", function (event) {
            if (typeof navigator.share !== "undefined") {
              navigator.share({
                title: J.translate("wishlistHeaderText"),
                text: J.translate("wishlistApiShareAppText") + " " + wishlist.data.storeName + ": ",
                url: wishlist.data.wishlistLinkUrl
              });
            }
          });
        },
      }
    },
    dataHandling: {
      resetListData: function () {
        wishlist.data.listData = {
          name: null,
          products: [],
        }
      },
      checkStoredData: function () {
        if (!wishlist.data.isExternalList) {
          var storageData = wishlist.dataHandling.getListDataFromStorage();
          if (storageData) {
            wishlist.data.listData = storageData;
          }

          // CHECK FOR STORED WISHLISTS
          if (J.checker.isLoggedIn) {
            wishlist.dataHandling.getUserLists();
          }
          // NOT LOGGED IN - ONLY USE STORED DATA
          else {
            wishlist.dataHandling.getProductData();
          }
        }
      },
      getUserLists: function () {
        wishlist.api.userLists.getAll(wishlist.dataHandling.checkUserLists, {}, false, 0);
      },
      checkUserLists: function (userListsDataObject) {

        var allUserListData = userListsDataObject.CustomerLists;

        // console.log("allUserListData: ", allUserListData);

        if (allUserListData.length) {
          wishlist.data.apiList = allUserListData[0];
          wishlist.data.apiListId = allUserListData[0].Id;
          wishlist.dataHandling.compareLists();
        }
        else {
          // CREATE NEW LIST
          wishlist.api.userLists.create("Wishlist", "", 1, function (userListData) {
            wishlist.data.apiList = userListData;
            wishlist.data.apiListId = userListData.Id;
            wishlist.dataHandling.compareLists();
          });
        }

      },
      compareLists: function () {
        var apiProductIds = [];
        var storedProductIds = [];
        var allProductIds = [];
        var newProductList = [];

        // LOOP API DATA
        $.each(wishlist.data.apiList.Products, function (index, apiProdData) {
          apiProductIds.push(apiProdData.ProductId);
          allProductIds.push(apiProdData.ProductId);

        });
        // console.log("apiProductIds: ", apiProductIds);

        // LOOP STORED DATA
        $.each(wishlist.data.listData.products, function (index, storedProdData) {
          storedProductIds.push(storedProdData.ProductId);
          allProductIds.push(storedProdData.ProductId);
        });
        // console.log("storedProductIds: ", storedProductIds);
        // console.log("allProductIds: ", allProductIds);

        // GET RID OF DUPLICATES
        allProductIds = wishlist.helpers.removeDuplicates(allProductIds);
        // console.log("allProductIds no duplicates: ", allProductIds);

        // ADD ALL PRODUCTS NOT PRESENT IN API LIST
        $.each(wishlist.data.listData.products, function (index, storedProdData) {
          var storedProdId = storedProdData.ProductId;
          var storedDescription = storedProdData.Description;
          var foundStoredIdInApiList = false;

          $.each(apiProductIds, function (index, apiProdId) {
            if (storedProdId === apiProdId) {
              foundStoredIdInApiList = true;
              return false;
            }
          });

          if (!foundStoredIdInApiList) {
            wishlist.dataHandling.addApiProduct(storedProdId, storedDescription);
          }
        });

        // ALERT ABOUT ADDING PRODS WHEN API LIST AND STORED LIST ARE NOT EQUAL
        if (wishlist.data.didJustLogIn
          && storedProductIds.length > 0
          && !(storedProductIds.length === allProductIds.length)) {
          wishlist.ui.showAddingProductsFromApiListAlert();
        }

        // CREATE NEW PRODUCT LIST IN MEMORY
        $.each(allProductIds, function (index, prodId) {
          newProductList.push({ProductId: prodId});
        });
        wishlist.data.listData.products = newProductList;

        // TRIGGER NEW DATA LOAD ETC
        wishlist.triggers.wishlistChanged(false);

      },
      getProductData: function () {
        var prodIds = [];

        if (wishlist.data.listData.products.length) {
          $.each(wishlist.data.listData.products, function (index, data) {
            prodIds.push(data.ProductId);
          });
          J.api.product.get(wishlist.dataHandling.parseProductData, prodIds, true, 5, prodIds);
        }
        else {
          wishlist.triggers.productDataFetched();
        }

      },
      parseProductData: function (prodData, prodIdArray) {
        // console.log("All prod data loaded");
        // console.log("prodData: ", prodData);
        // console.log("prodIdArray: ", prodIdArray);

        // ARRAY USED TO CHECK FOR INEXISTENT PRODUCTS
        var inexistentProdIds = prodIdArray;

        $.each(prodData.ProductItems, function (prodIndex, apiProdData) {

          if (apiProdData) {
            var prodId = apiProdData.Id;

            // ATTRIBUTES
            if (apiProdData.Attributes.length) {
              apiProdData.hasAttributes = true;
              if (apiProdData.Attributes[0].AttributesLevel2.length) {
                apiProdData.hasLv2Attributes = true;
              }
              else {
                apiProdData.hasLv2Attributes = false;
              }
            }
            else {
              apiProdData.hasAttributes = false;
            }

            // SPECIFICATIONS
            if (apiProdData.Specifications.length) {
              apiProdData.hasSpecifications = true;
              apiProdData.IsBuyable = false;
            }

            // COMMENTS
            if (apiProdData.Comments.length) {
              apiProdData.hasComments = true;
              apiProdData.IsBuyable = false;
            }

            // PACKAGE PRODUCTS (NOT DEVELOPED YET)
            if (apiProdData.IsPackageProduct) {
              apiProdData.isPackageProduct = true;
              apiProdData.IsBuyable = false;
            }

            $.each(wishlist.data.listData.products, function (storedProdIndex, storedProdData) {
              var storedId = storedProdData.ProductId;
              if (storedId == prodId) {
                storedProdData.prodData = apiProdData;
                return false;
              }
            });

            // REMOVE THIS PROD ID FROM ORG LIST, TO FIND ANY INEXISTENT PRODUCTS
            $.each(inexistentProdIds, function (arrayIndex, arrayProdId) {
              if (prodId === arrayProdId) {
                inexistentProdIds.splice(arrayIndex, 1);
                return false;
              }
            });

          }
        });

        // REMOVE INEXISTENT PRODUCTS FROM PROD LIST BEFORE THEY CAUSE PROBLEMS
        if (inexistentProdIds.length) {
          // console.log("inexistentProdIds: ", inexistentProdIds);
          $.each(inexistentProdIds, function (index, inexistentProdId) {
            $.each(wishlist.data.listData.products, function (prodIndex, prodData) {
              if (inexistentProdId === prodData.ProductId) {
                wishlist.data.listData.products.splice(prodIndex, 1);
                return false;
              }
            });
          });
        }

        wishlist.triggers.productDataFetched();

      },
      addProductToList: function (prodId, description) {
        var dataObject;
        if (prodId && typeof prodId === "string") {
          prodId = parseInt(prodId);
        }
        if (prodId) {
          dataObject = {
            ProductId: prodId
          }
          if (typeof description === "string" && description.length) {
            dataObject.Description = description;
          }

          // LOOK FOR DUPLICATE
          var foundDuplicate = false;
          $.each(wishlist.data.listData.products, function (prodIndex, prodData) {
            if (prodData.ProductId === dataObject.ProductId) {
              wishlist.data.listData.products[prodIndex] = dataObject;
              foundDuplicate = true;
              return false;
            }
          });
          if (!foundDuplicate) {
            wishlist.data.listData.products.push(dataObject);
          }
          wishlist.triggers.wishlistChanged();

          if (wishlist.data.isLoggedIn) {
            wishlist.dataHandling.addApiProduct(prodId, description);
          }

        }
        else {
          console.log("Invalid prod id sent to wishlist.dataHandling.addProductToList");
        }
      },
      addApiProduct: function (prodId, description) {
        wishlist.api.products.addProductToList(wishlist.dataHandling.getUserLists, null, wishlist.data.apiListId, description, 1, prodId, null);
      },
      deleteProductFromList: function (prodId) {
        $.each(wishlist.data.listData.products, function (prodIndex, prodData) {
          var listProdId = prodData.ProductId;
          if (listProdId === prodId) {
            wishlist.data.listData.products.splice(prodIndex, 1);
            if (wishlist.data.isLoggedIn) {
              wishlist.dataHandling.deleteApiProduct(prodId);
            }
            wishlist.triggers.wishlistChanged(true);
            return false;
          }
        });
      },
      deleteAllProductsFromList: function (prodId) {
        if (wishlist.data.isLoggedIn) {
          wishlist.dataHandling.deleteAllApiProducts();
        }
        wishlist.data.listData.products = [];
        wishlist.triggers.wishlistChanged(false);
      },
      deleteApiProduct: function (prodId) {

        // A ROUNDABOUT LOOPING WAY TO DELETE TO AVOID ATTRIBUTE PROBLEMS
        $.each(wishlist.data.apiList.Products, function (index, prodData) {
          var apiProdId = prodData.ProductId;
          var apiAttrId = prodData.AttributeId;
          if (prodId == apiProdId) {
            wishlist.api.products.deleteProductFromList(wishlist.data.apiListId, prodId, apiAttrId, wishlist.dataHandling.getUserLists);
          }
        });

      },
      deleteAllApiProducts: function () {

        $.each(wishlist.data.listData.products, function (index, data) {
          var prodId = data.ProductId;
          wishlist.dataHandling.deleteApiProduct(prodId);
        });
      },
      getListDataFromStorage: function () {
        var storageData = JSON.parse(localStorage.getItem("wishlist"));
        return storageData;
      },
      writeListDataToStorage: function () {
        if (!wishlist.data.isExternalList) {
          // console.log("storing wishlist.data.listData: ", wishlist.data.listData);

          // STRIP ALL FETCHED PRODUCT DATA FROM DATA TO STORE TO SAVE SPACE
          var dataToStore = {
            name: wishlist.data.listData.name,
            products: []
          }

          $.each(wishlist.data.listData.products, function (index, prodData) {
            dataToStore.products.push({
              ProductId: prodData.ProductId
            });
          });


          localStorage.setItem("wishlist", JSON.stringify(dataToStore));
        }
      },

    },
    cartFunctions: {
      validateSelectedProducts: function (callback) {
        var validationOk = true;
        $(".wishlist-products .product-item.item-selected").each(function () {
          var prod = $(this);

          // ATTRIBUTES
          var selectedValue = null;
          if (prod.hasClass("has-attributes")) {
            if (prod.find(".attribute-wrapper-lv2").length) {
              selectedValue = prod.find(".attribute-select-lv2.visible").val();
            }
            else {
              selectedValue = prod.find(".attribute-select-lv1").val();
            }
            if (!selectedValue) {
              wishlist.listContainer.addClass("validation-error");
              prod.addClass("validation-error");
              prod.find(".attributes-wrapper").addClass("validation-error");
              validationOk = false;
            }
          }
        });

        if (validationOk) {
          if (callback) {
            callback();
          }
          wishlist.ui.disableAllValidationAlerts();
        }
        wishlist.ui.calculateMainAreaPositioning();
      },
      addSelectedProducts: function () {

        if (wishlist.data.listData.products.length) {
          var productsToAdd = [];

          // COLLECT PROD INFO
          $(".wishlist-products .product-item.item-selected").each(function () {
            var prod = $(this);
            var prodId = prod.data("id");
            var qty = 1;
            var attrId;

            var selectedAttrId = null;
            if (prod.hasClass("has-attributes")) {
              if (prod.find(".attribute-wrapper-lv2").length) {
                selectedAttrId = prod.find(".attribute-select-lv2.visible option:selected").data("id");
              }
              else {
                selectedAttrId = prod.find(".attribute-select-lv1 option:selected").data("id");
              }
              if (selectedAttrId) {
                attrId = selectedAttrId;
              }
            }

            var prodObj = {
              CartId: J.cart.cartId,
              ProductId: prodId,
              Quantity: qty
            }

            if (attrId) {
              prodObj.AttributeId = attrId;
            }
            productsToAdd.push(prodObj);
          });

          // ADD TO CART
          if (productsToAdd.length) {
            wishlist.api.products.addMultipleItemsToCart(productsToAdd, function () {
              Services.cartService.reload();

              //OPEN CART
              if (typeof dynamicCart !== "undefined") {
                dynamicCart.open(true);
              }

              // CLOSE WISH LIST
              wishlist.ui.closeWishlist();

              // DELETE FROM LIST
              if ($(".delete-products-after-add-to-cart input:checked").length) {
                $.each(productsToAdd, function (index, data) {
                  wishlist.dataHandling.deleteProductFromList(data.ProductId);
                });
              }
            });

          }
        }
      },

    },
    triggers: {
      wishlistChanged: function (wasDeletion) {
        if (wasDeletion) {
          wishlist.triggers.productDataFetched(wasDeletion);
        }
        else {
          wishlist.dataHandling.getProductData();
        }
        wishlist.ui.markWishlistedProducts();
      },
      productDataFetched: function (wasDeletion) {
        wishlist.dataHandling.writeListDataToStorage();
        wishlist.ui.updateListProdQtyIndicator();
        wishlist.render.productList();

        if (wasDeletion) {
          // RECHECK VALIDATION PROCESS IF IN ADD TO CART
          if (wishlist.listContainer.hasClass("validation-error")) {
            wishlist.cartFunctions.validateSelectedProducts();
          }
          wishlist.ui.checkForEmptyProductList();
          wishlist.ui.calculateMainAreaPositioning();
        }
        else {
          wishlist.ui.checkForEmptyProductList();
          $(".wishlist-products .attribute-select-lv1").change();
        }
        if (wishlist.data.isExternalList) {
          wishlist.ui.openWishlist();
          wishlist.helpers.switchMode("default-mode", "add-to-cart-mode", null);
        }
      }
    },
    render: {
      wishlistIcon: function () {
        var template = J.views['wishlist/wishlist-icon'];
        var html = template();
        $(wishlist.settings.ui.insertWishlistIconBefore).before(html);
        wishlist.ui.events.bindWishlistIcon();
      },
      wishlistContainer: function () {

        // IF SECOND RENDER, REMOVE OLD ONE
        $(".wishlist-container").remove();

        var template = J.views['wishlist/wishlist-container'];
        var html = template(wishlist.data);
        $("body").append(html);
        wishlist.listContainer = $(".wishlist-container");
        wishlist.ui.events.bindWishlistContainer();
        wishlist.ui.events.bindDeleteProductBtn();
        wishlist.ui.events.bindAttributes();
        wishlist.ui.events.bindSelectProductCheckbox();
        wishlist.ui.events.bindAddToCartBtn();
        wishlist.ui.calculateMainAreaPositioning();


        // CHECK IF WAS RECENT LOGIN THROUGH WISHLIST - OPEN WISHLIST
        if (wishlist.data.didJustLogIn && getCookie("wishlist-login") == "true") {
          wishlist.ui.openWishlist();
          // wishlist.helpers.switchMode("default-mode", "sharing-mode", null);
          setCookie("wishlist-login", "", 0);
        }
        else if (location.pathname !== JetshopData.Urls.MyPagesUrl) {
          setCookie("wishlist-login", "", 0);
        }

      },
      productList: function () {
        var template = J.views['wishlist/wishlist-products'];
        var html = template(wishlist.data);
        wishlist.listContainer.find(".product-list-container").html(html);
        $(".wishlist-products .product-name").matchHeight(J.config.sameHeightConfig);
        wishlist.ui.calculateMainAreaPositioning();
      },
      sharing: function () {
        var template = J.views['wishlist/wishlist-sharing'];
        var html = template(wishlist.data);
        wishlist.listContainer.find(".sharing-container").html(html);
        wishlist.ui.events.bindSharingInteractions();


      },

    },
    api: {
      userLists: {
        getAll: function (callback, callbackOptions, enableCache, cacheMinutes) {
          // //wishlist.helpers.overlay.show();
          var service = "customerlists?encryptedCustomerIdentifier=" + wishlist.data.customerId;

          if (J.checker.isLoggedIn) {
            service += "&pricelistid=" + J.data.priceListId;
          }

          J.api.helpers.getter(service, function (data) {
            // //wishlist.helpers.overlay.hide();
            // wishlist.data.userLists = data["CustomerLists"];

            // FILTER DATA
            // wishlist.helpers.filterListData();

            if (callback) {
              callback(data);
            }
          }, callbackOptions, enableCache, cacheMinutes);
        },
        getSingle: function (listIdentifier, callback, callbackOptions, enableCache, cacheMinutes) {
          var service = J.data.culture + "/"
            + J.data.currency.name
            + "/customerlists/" + listIdentifier + "?encryptedCustomerIdentifier=" + wishlist.data.customerId;

          // //wishlist.helpers.overlay.show();

          J.api.helpers.getter(service, function (data) {
            // //wishlist.helpers.overlay.hide();

            // EXTERNAL LIST - ADD NECESSARY BASE DATA FOR RENDER
            if (wishlist.data.isExternalList) {
              data.ListTypeId = 0;
              // customerLists.data.userLists.push(data);

              if (callbackOptions && callbackOptions.isFirstExternalCall) {
                wishlist.data.userLists = [{
                  Id: data.Id,
                  ListId: data.ListId,
                  Editable: data.Editable,
                  Name: data.Name,
                  Description: data.Description,
                  ListTypeId: 0
                }];

                wishlist.data.listTypes[0].userLists = wishlist.data.userLists;

              }
            }

            if (callback) {
              callback(data, callbackOptions);
            }
          }, callbackOptions, enableCache, cacheMinutes);
        },
        create: function (name, description, listTypeId, callback, callbackOptions) {

          // AVOID IDENTICAL NAMES ON NEW LISTS
          if (!name) {
            name = J.translate("customerListsNewListName");
            name = wishlist.helpers.assignNewListName(name);
          }

          var listData = JSON.stringify({
            "Name": name,
            "Description": description,
            "ListType": listTypeId
          });
          var service = JetshopData.Urls.ServicesUrl + "/rest/v2/json/customerlists?encryptedCustomerIdentifier=" + wishlist.data.customerId;

          //wishlist.helpers.overlay.show();
          $.ajax({
            type: "POST",
            cache: false,
            url: service,
            data: listData,
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
              //wishlist.helpers.overlay.hide();
              // log(data);
              if (callback) {
                callback(data, callbackOptions);
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.error('Ajax Error - CreateCustomerList');
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
              //wishlist.helpers.overlay.hide();
            }
          });


        },
        update: function (customerListData, callback) {
          var listIdentifier = customerListData.Id;
          var listData = JSON.stringify({
            "Name": customerListData.Name,
            "Description": customerListData.Description
          });
          var service = JetshopData.Urls.ServicesUrl
            + "/rest/v2/json/customerlists/" + listIdentifier
            + "?encryptedCustomerIdentifier=" + wishlist.data.customerId;


          //wishlist.helpers.overlay.show();

          $.ajax({
            type: "POST",
            cache: false,
            url: service,
            data: listData,
            dataType: "json",
            contentType: "application/json",
            success: function (userListData) {
              //wishlist.helpers.overlay.hide();

              // log("Update success!");
              // log(userListData);

              var userListIndex = userListData.ListTypeId - 1;
              wishlist.data.userLists[userListIndex] = userListData;
              wishlist.listPage.renderSingleList(userListData);

              if (callback) {
                callback(userListData);
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.error('Ajax Error - Customer List Update');
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
            }
          });


        },
        delete:

          function (listIdentifier, callback) {
            var service = JetshopData.Urls.ServicesUrl
              + "/rest/v2/json/customerlists/" + listIdentifier
              + "/delete?encryptedCustomerIdentifier=" + wishlist.data.customerId;

            //wishlist.helpers.overlay.show();

            $.ajax({
              type: "POST",
              cache: false,
              url: service,
              dataType: "json",
              contentType: "application/json",
              success: function (data) {

                //wishlist.helpers.overlay.hide();

                // log("deleted list with id " + listIdentifier);
                // log(data);

                if (callback) {
                  callback(data);
                }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                console.error('Ajax Error - Customer List Delete');
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
              }
            });


          },
        deleteAll: function () {
          // INTENDED AS A DEV FUNCTION
          var service = "/customerlists?encryptedCustomerIdentifier=" + wishlist.data.customerId;

          //wishlist.helpers.overlay.show();
          J.api.helpers.getter(service, function (data) {
            // log("data");
            // log(data);

            //wishlist.helpers.overlay.hide();
            // DELETE EVERY LIST
            $.each(data.CustomerLists, function (index, data) {
              var userListId = data.Id;
              wishlist.api.userLists.delete(userListId);
            });
          }, {}, false, 0);
        },
      },
      products: {
        addProductToList: function (callback, callbackData, listIdentifier, description, qty, prodId, attrId) {
          var productData = {
            Quantity: qty,
            ProductId: prodId
          }

          if (typeof description !== "undefined" || description !== null) {
            productData.Description = description
          }

          if (attrId) {
            productData.AttributeId = attrId
          }

          var jsonData = JSON.stringify(productData);

          var service = JetshopData.Urls.ServicesUrl
            + "/rest/v2/json/customerlists/" + listIdentifier
            + "/product?encryptedCustomerIdentifier=" + wishlist.data.customerId;
          //wishlist.helpers.overlay.show();

          $.ajax({
            type: "POST",
            cache: false,
            url: service,
            data: jsonData,
            dataType: "json",
            contentType: "application/json",
            success: function (productData) {
              //wishlist.helpers.overlay.hide();
              if (callback) {
                callback(productData, callbackData);
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.error('Ajax Error - Add product to list');
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
              //wishlist.helpers.overlay.hide();
            }
          });
        },

        deleteProductFromList: function (listIdentifier, prodId, attrId, callback, callbackData) {

          var service = JetshopData.Urls.ServicesUrl
            + "/rest/v2/json/customerlists/" + listIdentifier
            + "/product/" + prodId;

          if (attrId) {
            service += "/attribute/" + attrId;
          }

          service += "/delete?encryptedCustomerIdentifier=" + wishlist.data.customerId;
          //wishlist.helpers.overlay.show();
          $.ajax({
            type: "POST",
            cache: false,
            url: service,
            data: null,
            dataType: "json",
            contentType: "application/json",
            success: function () {

              //wishlist.helpers.overlay.hide();
              if (callback) {
                callback(callbackData);
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.error('Ajax Error - Customer List Update');
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
            }
          });
        },

        addMultipleItemsToCart: function (itemArray, callback, callbackOptions) {
          var products_json = JSON.stringify(itemArray);
          var restUrl = JetshopData.Urls.ServicesUrl + "/rest/v2/json/shoppingcart/multiple/" + J.cart.cartId;


          if (J.checker.isLoggedIn) {
            restUrl += "?pricelistid=" + J.data.priceListId;
          }

          //wishlist.helpers.overlay.show();
          $.ajax({
            type: "POST",
            cache: false,
            url: restUrl,
            data: products_json,
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
              //wishlist.helpers.overlay.hide();
              // log(data);
              if (callback) {
                callback(data, callbackOptions);
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.error('Ajax Error - Add Multiple Items To Cart');
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
              //wishlist.helpers.overlay.hide();
            }
          });
        }
      }
    },
    helpers: {
      removeDuplicates: function (array) {
        var prims = {"boolean": {}, "number": {}, "string": {}}, objs = [];
        return array.filter(function (item) {
          var type = typeof item;
          if (type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
          else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
        });
      },
      checkLoginStatus: function () {
        if (wishlist.data.isLoggedIn) {
          var wasLoggedIn = getCookie("wl-loggedin");
          if (wasLoggedIn == "true") {
            wishlist.data.didJustLogIn = false;
          }
          else {
            wishlist.data.didJustLogIn = true;
            setCookie("wl-loggedin", "true", 365);
          }
        }
        else {
          setCookie("wl-loggedin", "false", 365);
          wishlist.data.didJustLogIn = false;
        }
        // console.log("wishlist.data.didJustLogIn: ", wishlist.data.didJustLogIn);
      },
      assignNewListName: function (newListName) {
        if (wishlist.helpers.checkIfUserListNameExists(newListName)) {
          for (var i = 1; i < 100; i++) {
            var newNameAttempt = newListName + " (" + i + ")";
            if (!wishlist.helpers.checkIfUserListNameExists(newNameAttempt)) {
              newListName = newNameAttempt;
              break;
            }
          }
        }
        return newListName;
      },
      checkIfUserListNameExists: function (name) {
        var nameExists = false;
        $.each(wishlist.data.userLists, function (listIndex, listData) {
          var listName = listData.Name;
          if (name == listName) {
            nameExists = true;
            return false;
          }
        });
        return nameExists;
      },
      currencyDisplay: {
        settings: {
          amountFirst: true,
          spacerString:
            "&nbsp;",
          useDecimalComma:
            true,
          decimalPlaces:
            2,
          symbol:
          J.data.currency.symbol
        },
        init: function () {

          // UPDATE BASE SETTINGS WITH CURRENT VALUES
          var currencyDisplayFormat = J.data.currency.display;
          var settings = wishlist.helpers.currencyDisplay.settings;

          // WITH OR W/O SPACES
          if (currencyDisplayFormat.substr(1, 1) != " ") {
            settings.spacerString = "";
          }

          // AMOUNT FIRST
          if (currencyDisplayFormat.substr(0, 1) != "n") {
            settings.amountFirst = false;
          }

          // DECIMAL POINT
          if (!J.data.currency.separator.match(",")) {
            settings.useDecimalComma = false;
          }

          // DECIMAL PLACES
          settings.decimalPlaces = wishlist.settings.decimalPlaces["channel_" + JetshopData.ChannelInfo.Active];

        },
        parsePriceStringToNumber: function (priceString) {
          var priceAsNumber;
          priceString = priceString.replace(/\s|€|\$/g, "").replace(/[a-zA-Z]*/g, "");

          if (wishlist.helpers.currencyDisplay.settings.useDecimalComma) {
            priceString = priceString.replace(".", "").replace(",", ".");
          }
          else {
            priceString = priceString.replace(",", "");
          }
          priceAsNumber = parseFloat(priceString);
          return priceAsNumber;
        },
        parsePriceToString: function (price) {
          var displaySettings = wishlist.helpers.currencyDisplay.settings;
          var spacerString = displaySettings.spacerString;
          var floatPrice;
          var priceString;

          if (typeof price == "string") {
            floatPrice = parseFloat(price);
          }
          else if (typeof price == "number") {
            floatPrice = price;
          }
          else {
            return "(NaN)";
          }

          // DECIMAL PLACES
          priceString = floatPrice.toFixed(displaySettings.decimalPlaces);

          // DECIMAL & THOUSAND MARKER
          if (displaySettings.useDecimalComma) {
            priceString = priceString.replace(/\./g, ",");
            priceString = priceString.replace(/\B(?=(?:\d{3})+(?!\d))/g, ".");
          }
          else {
            priceString = priceString.replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
          }

          // MAKE FINAL STRING
          if (displaySettings.amountFirst) {
            priceString = priceString + spacerString + displaySettings.symbol;
          }
          else {
            priceString = displaySettings.symbol + spacerString + priceString;
          }

          return priceString;

        }
      },
      popupLocalMsg: {
        showSpinner: function (element) {
          var msg = element.siblings(".action-msg");
          msg.addClass("active");
        },
        showMsg: function (element) {
          var msg = element.siblings(".action-msg");
          msg.addClass("complete");
          setTimeout(function () {
            msg.removeClass("active").removeClass("complete");
          }, 1500);
        }
      },
      overlay: {
        overlayTimeout: null,
        init:

          function () {
            $("body > form").append("<div id='wishlist-overlay' class='hidden'><i class='fa fa-spinner fa-spin'></i></div>");
          },
        show: function () {
          var overlay = $("#wishlist-overlay");
          clearTimeout(wishlist.helpers.overlay.overlayTimeout);
          overlay.removeClass("hidden").addClass("active");

          // FALLBACK OVERLAY HIDING IN CASE OF API ERROR
          wishlist.helpers.overlay.overlayTimeout = setTimeout(function () {
            console.log("Customer list overlay: API response timed out - disabling");
            //wishlist.helpers.overlay.hide();
          }, 8000);
        },
        hide: function () {
          var overlay = $("#wishlist-overlay");
          overlay.removeClass("active");
          clearTimeout(wishlist.helpers.overlay.overlayTimeout);
          wishlist.helpers.overlay.overlayTimeout = setTimeout(function () {
            overlay.addClass("hidden");
          }, 200);
        }
      },
      switchMode: function (oldModeClass, newModeClass, callback) {
        wishlist.listContainer.addClass("fade-out");
        setTimeout(function () {
          wishlist.listContainer.removeClass(oldModeClass).addClass(newModeClass);
          wishlist.listContainer.removeClass("fade-out");
          wishlist.ui.calculateMainAreaPositioning();
          if (callback) {
            callback();
          }
        }, 200);
      },
      createWishlistLinkUrl: function () {
        var linkUrl = location.origin + "?list=1&";
        if (J.checker.isLoggedIn) {
          linkUrl += "id=" + wishlist.data.apiListId;
        }
        else {
          linkUrl += "prods=";
          $.each(wishlist.data.listData.products, function (index, prodData) {
            linkUrl += prodData.ProductId;
            if (index !== wishlist.data.listData.products.length - 1) {
              linkUrl += "+";
            }
          });
        }

        wishlist.data.wishlistLinkUrl = linkUrl;
      },
    },

    translations: function () {
      J.translations.push({
        wishlistHeaderText: {
          sv: "Favoriter",
          en: "Favorites",
          nb: "Favoritter",
          da: "Favoritter",
          fi: "Suosikit"
        },
        wishlistNotLoggedInMsgLoginBtn: {
          sv: "Logga in för att spara din lista",
          en: "Log in to save your list",
          nb: "Logg inn for å lagre din liste",
          da: "Log ind for at gemme dine favoritter",
          fi: "Kirjaudu sisään tallentaaksesi listasi "
        },
        wishlistLoggedInInfoBody: {
          sv: "När du är inloggad så sparas din lista automatiskt och du kan komma åt den från andra enheter om du loggar in.\n\nOm du delar en favoritlista när du är inloggad så kan du ändra i den i efterhand, och dina vänner ser alltid den senaste versionen.",
          en: "When you are logged in your list is saved automatically and you can access it from any device if you log in.\n\nIf you share your favorites when you are logged in you can change it later, and your friends will always see the latest version.",
          nb: "Når du er innlogget lagres listen din automatisk, og du kan få tilgang til den fra andre enheter hvis du logger inn.\n\nDersom du deler en favorittliste når du er innlogget kan du endre den i etterkant, og vennene dine ser alltid den siste versjonen.",
          da: "Når du er logget ind, gemmes din liste med favoritter automatisk, og dine favoritter er herefter tilgængelige på alle enheder, hvor du er logget ind.\n\nHvis du deler dine favoritter, når du er logget ind, kan dine venner altid se den seneste version, selv hvis du efterfølgende logger ind og ændrer listen.",
          fi: "Listasi tallentuu automaattisesti, jos olet kirjautunut sisään. Näin pääset käsiksi siihen myös muilta laitteilta.\n\nJos jaat suosikkilistasi sisäänkirjautuneena, voit tehdä siihen muutoksia jälkikäteen ja ystäväsi näkevät aina viimeisimmän version. "
        },
        wishlistNotLoggedInInfoBody: {
          sv: "Här kan du se dina favoriter, men bara på den här datorn eller enheten. Om du loggar in så sparas din lista automatiskt och du kan komma åt den överallt.\n\nOm du delar en favoritlista när du är inloggad så kan du ändra i den i efterhand, och dina vänner ser alltid den senaste versionen.",
          en: "Here you can see your favorites, but they are only accessible on this device. If you log in your list is automatically saved and you can retrieve it from any device.\n\nIf you share your favorites when you are logged in you can change it later, and your friends will always see the latest version.",
          nb: "Her kan du se dine favoritter, men bare på denne datamaskinen eller enheten. Dersom du logger inn, lagres listen din automatisk og du kan finne den overalt.\n\nHvis du deler en favorittliste mens du er innlogget, kan du endre den i etterkant, og vennene dine ser alltid den siste versjonen.",
          da: "Her kan du se dine favoritter, men kun på samme computer eller enhed. Hvis du logger ind, gemmes din liste automatisk, og den er herefter tilgængelig på alle enheder.\n\nHvis du deler dine favoritter, når du er logget ind, kan dine venner altid se den seneste version, selv hvis du efterfølgende logger ind og ændrer listen.",
          fi: "Näet suosikkisi täältä, mutta vain tältä tietokoneelta tai laitteelta. Jos kirjaudut sisään, tallentuu listasi automaattisesti ja pääset käsiksi siihen mistä tahansa.\n\nJos jaat suosikkilistasi sisäänkirjautuneena, voit tehdä siihen muutoksia jälkikäteen ja ystäväsi näkevät aina viimeisimmän version."
        },
        wishlistBtnTitle: {
          sv: "Se mina favoriter",
          en: "Show my favorites",
          nb: "Se mina favoritter",
          da: "Se mine favoritter",
          fi: "Katso suosikkini "
        },
        wishlistBtnText: {
          sv: "Favoriter",
          en: "Favorites",
          nb: "Favoritter",
          da: "Favoritter",
          fi: "Suosikit"
        },
        wishlistClose: {
          sv: "Stäng",
          en: "Close",
          nb: "Lukk",
          da: "Luk",
          fi: "Sulje"
        },
        wishlistOk: {
          sv: "OK",
          en: "OK",
          nb: "OK",
          da: "OK",
          fi: "OK"
        },
        wishlistCancel: {
          sv: "Avbryt",
          en: "Cancel",
          nb: "Avbryt",
          da: "Afbryd",
          fi: "Peruuta"
        },
        wishlistEnterAddToCartMode: {
          sv: "Välj och lägg i kundvagn",
          en: "Select and add to cart",
          nb: "Velg og legg i handlevogn",
          da: "Vælg og tilføj til indkøbskurv",
          fi: "Valitse ja lisää ostoskoriin"
        },
        wishlistAddToCartBtn: {
          sv: "Lägg markerade produkter i kundvagnen",
          en: "Add selected items to cart",
          nb: "Legg markerte produkter i handlevognen",
          da: "Tilføj de valgte produkter til indkøbskurven",
          fi: "Lisää merkityt tuotteet ostoskoriin"
        },
        wishlistShareList: {
          sv: "Dela din lista",
          en: "Share your list",
          nb: "Del din liste",
          da: "Del din liste",
          fi: "Jaa lista"
        },
        wishlistExitSharingMode: {
          sv: "Tillbaka",
          en: "Back",
          nb: "Tilbake",
          da: "Tilbage",
          fi: "Takaisin"
        },
        wishlistClearList: {
          sv: "Rensa listan",
          en: "Clear list",
          nb: "Slett listen",
          da: "Ryd listen",
          fi: "Tyhjennä lista"
        },
        wishlistExitAddToCartMode: {
          sv: "Tillbaka",
          en: "Back",
          nb: "Tilbake",
          da: "Tilbage",
          fi: "Takaisin"
        },
        wishlistEmptyProductListHeader: {
          sv: "Favoritlistan är tom.",
          en: "Your favorites list is empty.",
          nb: "Favorittlisten er tom",
          da: "Din favoritliste er tom",
          fi: "Suosikkilista on tyhjä"
        },
        wishlistDeleteProductTitle: {
          sv: "Ta bort från listan",
          en: "Delete from list",
          nb: "Fjern fra listen",
          da: "Fjern fra listen",
          fi: "Poista listalta"
        },
        wishlistProductArticleNumber: {
          sv: "Artikelnummer",
          en: "Article number",
          nb: "Produktnummer",
          da: "Artikelnummer",
          fi: "Tuotenumero"
        },
        wishlistProductStockStatus: {
          sv: "Lagerstatus",
          en: "Stock status",
          nb: "Lagerstatus",
          da: "Lagerstatus",
          fi: "Varaston tilanne"
        },
        wishlistAttributeSelect: {
          sv: "Välj",
          en: "Select",
          nb: "Velg",
          da: "Vælg",
          fi: "Valitse"
        },
        wishlistDeleteAllProductsAlertHeader: {
          sv: "Rensa listan",
          en: "Clear list",
          nb: "Fjern listen",
          da: "Ryd listen",
          fi: "Tyhjennä lista"
        },
        wishlistDeleteAllProductsAlertBody: {
          sv: "Du kommer nu att ta bort alla produkter från listan.",
          en: "You will now remove all items from the list.",
          nb: "Du er nå i ferd med å fjerne alle produkter fra listen.",
          da: "Du vil nu slette alle produkter på listen.",
          fi: "Olet poistamassa kaikkia tuotteita listalta."
        },
        wishlistAddToListBtnTitle: {
          sv: "Lägg i favoriter",
          en: "Add to favorites",
          nb: "Legg i favoritter",
          da: "Tilføj til favoritter",
          fi: "Lisää suosikkeihin "
        },
        wishlistItemInListBtnTitle: {
          sv: "Produkten är en favorit",
          en: "Item is a favorite",
          nb: "Produktet er en favoritt",
          da: "Produktet er en favorit",
          fi: "Tuote on merkattu suosikiksi"
        },
        wishlistItemNotBuyableText: {
          sv: "Den här produkten går för närvarande inte att beställa.",
          en: "This item is not available at the moment.",
          nb: "Dette produktet kan for øyeblikket ikke bestilles.",
          da: "Dette produkt er i øjeblikket ikke tilgængeligt.",
          fi: "Tätä tuotetta ei tällä hetkellä voi tilata. "
        },
        wishlistItemNotBuyableFromListText: {
          sv: "För att beställa den här produkten behöver du gå till dess produktsida för att göra ytterligare val.",
          en: "To order this item you need to visit its product page and make further selections.",
          nb: "For å bestille dette produktet må du gå til produktsiden for å ta ytterligere valg.",
          da: "For at bestille dette produkt skal du gå til produktsiden for yderligere valgmuligheder.",
          fi: "Tilataksesi tämän tuotteen, sinun on siirryttävä tuotesivulle tehdäksesi lisävalintoja. "
        },
        wishlistDeleteProdsAfterAddToCart: {
          sv: "Ta bort de markerade produkterna från listan när de hamnat i kundvagnen.",
          en: "Remove selected items from this list after they have been added to cart.",
          nb: "Fjern de markerte produktene fra listen når de havner i handlekurven.",
          da: "Fjern de valgte produkter fra listen, når de er tilføjet til indkøbskurven.",
          fi: "Poista merkatut tuotteet listalta, kun ne on siirretty ostoskoriin. "
        },
        wishlistSharingNotLoggedInHeader: {
          sv: "Du är inte inloggad",
          en: "You are not logged in",
          nb: "Du er ikke logget inn",
          da: "Du er ikke logget ind",
          fi: "Et ole kirjautunut sisään."
        },
        wishlistSharingNotLoggedInBody: {
          sv: "För att kunna dela din lista med andra behöver du först logga in.",
          en: "To be able to share your list with others, you must first log in.",
          nb: "For å kunne dele listen din med andre må du først logge inn.",
          da: "For at kunne dele din liste med andre, skal du logge ind.",
          fi: "Jakaaksesi listan muiden kanssa, tulee sinun ensin kirjautua sisään."
        },
        wishlistSharingInfoWithNavigatorShare: {
          sv: "Du kan enkelt dela din lista med andra via valfri app.",
          en: "Share list",
          nb: "Du kan enkelt dele listen din med andre via valgfri app",
          da: "Du kan let dele din liste med andre via en valgfri app.",
          fi: "Voit jakaa listan vaivatta muiden kanssa valitsemasi sovelluksen kautta."
        },
        wishlistSharingInfoWithoutNavigatorShare: {
          sv: "Du kan enkelt dela din lista med andra genom att kopiera länken nedan eller skicka ett mejl.",
          en: "Share list",
          nb: "Du kan enkelt dele listen din med andre ved å kopiere linken under eller sende en e-post.",
          da: "Du kan let dele din liste med andre ved at kopiere linket nedenfor eller via e-mail.",
          fi: "Voit jakaa listan vaivatta muiden kanssa kopioimalla linkin alta tai lähettämällä sähköpostin."
        },
        wishlistApiShareHeader: {
          sv: "Dela dina favoriter",
          en: "Share your favorites",
          nb: "Del dine favoritter",
          da: "Del dine favoritter",
          fi: "Jaa suosikkisi"
        },
        wishlistApiShareText: {
          sv: "Klicka för att dela via valfri app.",
          en: "Click to share through the app of your choice.",
          nb: "Klikk for å dele via valgfri app.",
          da: "Klik her for at dele via valgfri app.",
          fi: "Klikkaa jakaaksesi valinnaisen sovelluksen kautta."
        },
        wishlistApiShareBtn: {
          sv: "Dela listan",
          en: "Share list",
          nb: "Del listen",
          da: "Del listen",
          fi: "Jaa lista"
        },
        wishlistApiShareAppText: {
          sv: "Mina favoriter hos",
          en: "My favorites at",
          nb: "Mine favoritter hos",
          da: "Mine favoritter hos",
        },
        wishlistShareLinkHeader: {
          sv: "Dela länk",
          en: "Share link",
          nb: "Del link",
          da: "Del link",
          fi: "Jaa linkki"
        },
        wishlistShareLinkText: {
          sv: "Klicka för att kopiera länken nedan och klistra sedan in den i ett meddelande eller ett mejl.",
          en: "Click to copy the link below, and then paste it in a message or an e-mail.",
          nb: "Klikk for å kopiere linken under og lim den deretter inn i en melding eller en e-post",
          da: "Klik her for at kopiere nedenstående link og indsæt det efterfølgende i en besked eller e-mail.",
          fi: "Klikkaa kopioidaksesi linkin alta ja liitä se viestiin tai sähköpostiin."
        },
        wishlistShareLinkCopy: {
          sv: "Kopiera länk",
          en: "Copy link to clipboard",
          nb: "Kopier link",
          da: "Kopiér link",
          fi: "Kopioi linkki"
        },
        wishlistSendEmailHeader: {
          sv: "Dela via e-post",
          en: "Share via e-mail",
          nb: "Del via e-post",
          da: "Del via e-mail",
          fi: "Jaa sähköpostitse"
        },
        wishlistSendEmailText: {
          sv: "Klicka nedan för att skicka ett e-postmeddelande med en länk till din önskelista.",
          en: "Click below to send an email with a link to your wishlist.",
          nb: "Klikk under for å sende en e-post med en link til din ønskeliste.",
          da: "Klik på linket nedenfor for at sende en e-mail med et link til din ønskeliste.",
          fi: "Klikkaa alta lähettääksesi sähköpostin, joka sisältää linkin toivelistaasi"
        },
        wishlistSendEmailBtn: {
          sv: "Skicka e-post",
          en: "Send e-mail",
          nb: "Send e-post",
          da: "Send e-mail",
          fi: "Lähetä sähköposti"
        },
        wishlistAddingApiProdsAlertHeader: {
          sv: "Sparade favoriter",
          en: "Saved favorites",
          nb: "Lagrede favoritter",
          da: "Gemte favoritter",
          fi: "Tallennetut suosikit"
        },
        wishlistAddingApiProdsAlertBody: {
          sv: "Vi hittade sedan tidigare sparade favoriter, som vi nu lagt till i din lista.",
          en: "We found some previously saved favorites, which have been added to your list.",
          nb: "Vi fant tidligere lagrede favoritter, som vi nå har lagt til i din liste.",
          da: "Vi har fundet tidligere gemte favoritter, som vi nu har tilføjet til din liste.",
          fi: "Löysimme aiemmin tallennettuja suosikkeja, jotka olemme nyt lisänneet listaasi. "
        },
        wishlistAddToCartValidationErrorMsg: {
          sv: "Välj varianter för de markerade artiklarna.",
          en: "Make selections for the marked items.",
          nb: "Velg varianter for de markerte produktene.",
          da: "Vælg varianter for de markerede varer.",
          fi: "Valitse merkittyjen tuotteiden variantit."
        },
        wishlistInvalidExternalIdAlertHeader: {
          sv: "Ogiltig lista",
          en: "Invalid list",
          nb: "Ugyldig liste",
          da: "Ugyldig liste",
          fi: "Virheellinen lista"
        },
        wishlistInvalidExternalIdAlertBody: {
          sv: "Länken till listan du försöker ladda är inte giltig. Kanske har listans skapare tagit bort den, eller så har det smugit sig in något fel i länken.",
          en: "The link to the list you are trying to load is not valid. Maybe the list's creator has removed it, or maybe there is some error in the link.",
          nb: "Linken til listen du prøver å laste opp er ikke gyldig. Listens skaper har kanskje fjernet den, eller så har en feil snikt seg inn i linken",
          da: "Linket til listen, du forsøger at indlæse, er ikke gyldigt. Måske er det blevet fjernet eller også er der en fejl i linket.",
          fi: "Linkki listaan, jota yrität ladata, ei ole voimassa. Listan tekijä on ehkä poistanut sen, tai linkissä saattaa olla virhe."
        },
        wishlistExternalListHeaderText: {
          sv: "Delad lista",
          en: "Shared list",
          nb: "Delt liste",
          da: "Del liste",
          fi: "Jaettu lista"
        },
        wishlistExternalListBodyText: {
          sv: "Om du stänger den delade listan eller lämnar sidan behöver du ladda om den aktuella länken för att se den igen.",
          en: "If you close the shared list or leave the page you will have to reload the link URL to access it again.",
          nb: "Dersom du lukker den delte listen eller forlater siden, blir du nødt til å laste inn den aktuelle linken på nytt for å se den igjen",
          da: "Hvis du lukker den delte list, eller forlader siden, skal du blot genindlæse det aktuelle link for at se den igen.",
          fi: "Jos suljet jaetun listan tai poistut sivulta, tulee sinun ladata ajantasainen linkki nähdäksesi sen uudestaan."
        },
      })
    },

  }
;
J.pages.addToQueue("all-pages", wishlist.init);

// TODO
// FIXA HEADERLAYOUT I MEDIUM-DOWN
// FIXA PRODUKTBILDSLAYOUT, FLYTTA TILL HÖGER
