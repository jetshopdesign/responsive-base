
The standard-package of Slick are some what modified, for instance we use FontAwesome, omitting the Slick fonts supplied. 

___________________________________

## Caution

This module will hide elements on the start-page, and those divs will be shown after Slick are ready.

The following code will hide divs from CSS:

        .page-default {
          #startpage_list {
            display: none;
          }
          #main-area .content {
            display: none;
          }
          #footer {
            display: none;
          }
          #jetshop-branding {
            display: none;
          }
        }


Those divs will be shown in JS:

        showHiddenElements: function () {
            $("#startpage_list").show();
            $("#main-area").find(".content").show();
            $("#footer").show();
            $("#jetshop-branding").show();
        }


If a design are heavily modified and elements are added to start-page (i.e a top-bar) those additional elements need to be added to this logic as well.

Also, if the JS breaks, the function showHiddenElements will not be triggered, resulting in a empty start-page...

___________________________________


### Slick options have a separate readme.md
For all settings and methods in slick, please see file `readme-slick.md`


### Slick demo, examples, API
http://kenwheeler.github.io/slick/  


### Slick source-code
https://github.com/kenwheeler/slick


## Using switcher-definitions:
 
Example with two slides:
 
        <!-- BEGIN DEFINITIONS FOR IMAGE SWITCHER -------->
        <div id="switcher-definitions">
            <!-- START IMAGE ------------>                         
            <div class="switcher-image">
                <span class="link">kontakta-oss-i-48.aspx</span>
                <span class="image-name">switcher-image-1.jpg</span>
                <span class="image-name-mobile">mobile-switcher-image-1.jpg</span>
                <span class="text">Text för bild 1</span>
            </div>
            <!-- END IMAGE -------------->
            <!-- START IMAGE ------------>
            <div class="switcher-image">
                <span class="link">om-oss-i-45.aspx</span>
                <span class="image-name">switcher-image-2.jpg</span>
                <span class="image-name-mobile">mobile-switcher-image-2.jpg</span>
                <span class="text">Text för bild 2</span>
            </div>
            <!-- END IMAGE -------------->
        </div>
        <!-- END DEFINITIONS FOR IMAGE SWITCHER ---------->

 
      
One slide is defined by a block. These blocks can be duplicated:

        <!-- START IMAGE ------------>
            <div class="switcher-image">
              <span class="link">kontakta-oss-i-48.aspx</span>
              <span class="image-name">switcher-test-1.jpg</span>
              <span class="text">Text för bild 1</span>
            </div>
        <!-- END IMAGE -------------->      


The line `image-name-mobile` will be used for images shown on device-width small.
          
``<span class="image-name-mobile">mobile-switcher-image-2.jpg</span>``

Folder used for images when using switcher-definitions are `/pub_docs/files/image-switcher/`, this can be changed in config.

          
## Using startPageObjects:

When using startPageObjects the first row (4 items) will be used. These items have to be set to type "item", not "product".  

Settings outputted from startPagObjects are: Textfield, Image, Alt desc & Link
          
The second row can be used as mobile-slides if `slickSlider.config.useStartPageObjectsRowTwoAsMobileSlides` are set to true. In that case only the image-path are used from the second row, all other settings from the second row are omitted.
          
          
## slickSlider.config:

Option | Type |  Description
------ | ---- |  -----------
useStartPageObjects | boolean  | This will determine if we are using old-skool switcher-definitons, or a more modern approach with startPageObjects.
useStartPageObjectsRowTwoAsMobileSlides | boolean | This will use the second row of startPageObjects for device-width small (mobile).
imagesFolder | string | Folder used for paths when using switcher-definitions.
slick | object | Default settings for all slick-slides created.
slick.responsive | object | Use this to override settings per breakpoint.

>   For slick-sliders internal configuration, see file `readme-slick.md`


## Debugging          

``slickSlider.data`` will contain a JSON object used for rendering the slides. This is a good place to start debugging, if this object is empty the slider will not be shown.

>   If the slider are acting weird when using StartPageObjects, make sure that `J.components.startPageObjects()` are triggered _after_ `slickSlider.init()`


## Note

We are hiding the start-page content and it will be shown after Slick are ready. If the design are heavily customized, the SCSS-files & function `showHiddenElements` need to be updated.
