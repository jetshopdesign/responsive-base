var slickSlider = {
    data: [], // Will hold all slide data
    config: {
        useStartPageObjects: true,
        useStartPageObjectsRowTwoAsMobileSlides: true,
        imagesFolder: "/pub_docs/files/image-switcher/", // Only when using switcher-definitions
        slick: {
            prevArrow: '<span class="slick-arrow prev-arrow slick-prev"><i class="fa fa-fw fa-arrow-circle-left"></i></span>',
            nextArrow: '<span class="slick-arrow next-arrow slick-next"><i class="fa fa-fw fa-arrow-circle-right"></i></span>',
            lazyLoad: "ondemand",
            dots: true,
            mobileFirst: false,
            infinite: true,
            speed: 400,
            slidesToShow: 1,
            adaptiveHeight: false,
            autoplay: true,
            autoplaySpeed: 4000,
            responsive: [
                {
                    breakpoint: J.config.breakpoints.small,
                    settings: {}
                },
                {
                    breakpoint: J.config.breakpoints.medium,
                    settings: {}
                },
                {
                    breakpoint: J.config.breakpoints.large,
                    settings: {}
                },
                {
                    breakpoint: J.config.breakpoints.xlarge,
                    settings: {}
                }
            ]
        }
    },
    init: function () {
        if (slickSlider.config.useStartPageObjects) {
            console.info("Module slick-slider using startPageObjects");
        } else {
            console.info("Module slick-slider using switcher definitions");
        }
        if ($(".startpage-custom-item.startpage-object").length) {
            console.warn("J.components.startPageObjects() have triggered before slickSlider.init(). Slick-slide will not work correctly.")
        }

        if (slickSlider.config.useStartPageObjects) {
            //
            //  Using startPageObjects
            //
            var objects = $('#startpage_list div[class*="item-"]');
            objects.each(function (index) {
                var that = $(this);
                var classes = that.attr("class").toString().split(' ');
                var item = classes.filter(function (item) {
                    return item.indexOf("item-") > -1;
                });
                var rowNumber = item[0].split("-")[1];
                if (rowNumber == 1) {
                    slickSlider.utils.getDataFromStartPageObjects(that);
                    that.remove();
                }
                if (rowNumber == 2 && slickSlider.config.useStartPageObjectsRowTwoAsMobileSlides) {
                    if (that.find(".custom-item-image").length) {
                        var matchingIndex = index - slickSlider.data.length;
                        var imageMobile = that.find(".custom-item-image").attr("src");
                        if (typeof slickSlider.data[matchingIndex] != "undefined") {
                            slickSlider.data[matchingIndex]["imageMobile"] = imageMobile;
                        }
                        that.remove();
                    }
                }
            });

            if (slickSlider.data[0] != undefined && slickSlider.data[0].image !=undefined) {
                $("#main-area").prepend(slickSlider.utils.render());
                $(".slider").slick(slickSlider.config.slick);
            } else {
                slickSlider.utils.warningForMissingSlides()
            }
        } else {
            //
            //   Using switcher definitions
            //
            slickSlider.utils.getDataFromSwitcherDefinitions();
            if (slickSlider.data.length > 0) {
                $("#main-area").prepend(slickSlider.utils.render());
                $(".slider").slick(slickSlider.config.slick);
            } else {
                slickSlider.utils.warningForMissingSlides();
            }
        }
        slickSlider.utils.triggerStartPageObjects();
        slickSlider.utils.showHiddenElements();
        $(document).foundation();
    },
    utils: {
        triggerStartPageObjects: function () {
            if ($(".startpage-custom-item.startpage-object").length === 0) {
                J.components.startPageObjects();
                J.components.startPageObjects = function () {};
            }
        },
        showHiddenElements: function () {
            $("#startpage_list").show();
            $("#main-area").find(".content").show();
            $("#footer").show();
            $("#jetshop-branding").show();
        },
        getDataFromSwitcherDefinitions: function () {
            $("#switcher-definitions").find(".switcher-image").each(function () {
                var temp = {};
                var that = $(this);
                if (that.find(".link").length) {
                    temp.link = that.find(".link").html();
                }
                if (that.find(".image-name").length) {
                    temp.image = slickSlider.config.imagesFolder + that.find(".image-name").html();
                }
                if (that.find(".text").length) {
                    temp.text = that.find(".text").html();
                }
                if (that.find(".image-name-mobile").length) {
                    temp.imageMobile = slickSlider.config.imagesFolder + that.find(".image-name-mobile").html()
                }
                slickSlider.data.push(temp);
            });
        },
        getDataFromStartPageObjects: function ($object) {
            var temp = {};
            if ($object.find(".custom-item-link").length) {
                temp.link = $object.find(".custom-item-link").attr("href");
            }
            if ($object.find(".custom-item-image").length) {
                temp.image = $object.find(".custom-item-image").attr("src");
                if ($object.find(".custom-item-image").attr("alt")) {
                    temp.imageAlt = $object.find(".custom-item-image").attr("alt");
                }
            }
            if ($object.find(".custom-item-text").length) {
                temp.text = $object.find(".custom-item-text").html();
            }
            slickSlider.data.push(temp);
        },
        warningForMissingSlides: function () {
            console.warn("No slides are defined for module slick-slider");
        },
        render: function () {
            var template = J.views['slick-slider/slick-slider-startpage'];
            return template(slickSlider.data);
        }
    }
};

J.pages.addToQueue("start-page", slickSlider.init);
