## Js(Jetshop) Translations
Add support for translations from the Jetshop Admin

### Instructions
When developing a new store to a customer you should work as you've done before with 

    J.translations.push({
        FilterMobileShowFilter: {
            sv: 'Filtrera',
            en: 'Filter by'
            nb: 'Filter by'
        },
    })

When the store is ready for production all translations added within the project should be sent to Jetshop Support. This is changes are made to let the customers be able to change translation strings within Jetshop Admin by themselves.

When all strings are added to the database all the new strings should be added to the translations array within this module.

    translations: [
        'FilterYes',
        'FilterMaxPrice',
        'FilterMinPrice',
        'FilterMobileShowFilter',
        'FilterApplyFilter',
        'FilterReset',
        'FilterYourChoices',
        'FilterSelected',
        'FilterAllSelected',
        'FilterNoMatch',
        'FilterSelectAll',
        'FilterOk',
        'FilterClose',
        'FilterSearch',
        'FilterLoadMoreProducts'
    ],