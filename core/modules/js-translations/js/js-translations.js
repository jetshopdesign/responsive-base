var jsTranslations = {
    translations: [
      'AddingItemToCart',
      'Article',
      'Cart',
      'ContinueShopping',
      'FilterAllSelected',
      'FilterApplyFilter',
      'FilterBuyButton',
      'FilterClose',
      'FilterInfoButton',
      'FilterLoadMoreProducts',
      'FilterMaxPrice',
      'FilterMinPrice',
      'FilterMobileShowFilter',
      'FilterNoMatch',
      'FilterOk',
      'FilterReset',
      'FilterSearch',
      'FilterSelectAll',
      'FilterSelected',
      'FilterStockStatus',
      'FilterYes',
      'FilterYourChoices',
      'GettingCart',
      'IncludingVAT',
      'ItemNumber',
      'Menu',
      'OnlyAvailableInWarehouse',
      'OnlyAvailableInWebshop',
      'PlusVAT',
      'Price',
      'ProceedToCheckout',
      'Quantity',
      'ResponsiveMyPages_OrderCartRecreationItemErrorNotBuyable',
      'ResponsiveMyPages_OrderCartRecreationItemErrorOutOfStock',
      'Search',
      'Sort_ArticleNumber',
      'Sort_Bestseller',
      'Sort_Custom',
      'Sort_DateAdded',
      'Sort_Name',
      'Sort_Price',
      'Sort_SubName',
      'Total',
      'TotalItems',
      'ViewCart',
      'YourShoppingCart'
    ],
    init: function () {
        jsTranslations.add();
    },
    add: function () {
        $.each(jsTranslations.translations, function (index, translation) {
            var obj = {};
            var insideObj = {};
            insideObj[J.data.language] = JetshopData.Translations[translation];
            obj[translation] = insideObj;

            J.translations.push(obj);
        });
    }
};

J.pages.addToQueue("all-pages", jsTranslations.init);
