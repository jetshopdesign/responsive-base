Creates a instagram feed of a instagram user.

### Dependencies
Requires [InstaFeedJS](http://instafeedjs.com) (included in module)

### Installation
You need the login credentials to the user which feed you want to show.
 
* Login to the users account on instagram 
* Navigate to the Instagram developer platform [www.instagram.com/developer](www.instagram.com/developer)
  * If the user has not been in the developer area you need to sign up for the developer account.
* Create a client and enter the URL to the site in Redirect URI
  * The privacy policy can be the same as the sites terms for example ``Köpvillkor``
* For easier Authentication to the client would I recommend you to switch to the security tab and unmark the option ``Disable implicit OAuth``
* Register the client
* Now you need to authenticate the user to the newly created client. To do so navigate to [www.instagram.com/developer/authentication](www.instagram.com/developer/authentication)
  * A short guide on how to do it:
    * Copy the CLIENT-ID from the Client you recently created
    * Navigate to ``You need to change the values CLIENT-ID and REDIRECT-URI to the information about your client`` [https://api.instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=token&scope=public_content](https://api.instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=token&scope=public_content) you should put notice that ``&scope=public_content`` is added to the string from the documentation from Instagram. This is a premission which is neccessary to display a users feed.
    * When navigated to the URL you're being redirected to the Redirect URI you entered in the URI to a URI like [http://your-redirect-uri#access_token=ACCESS-TOKEN](http://your-redirect-uri#access_token=ACCESS-TOKEN) Copy the Access Token and enter it in the module settings.
* If you don't know the userID of your instagram user please visit [https://www.otzberg.net/iguserid/](https://www.otzberg.net/iguserid/)
* And now it's done!