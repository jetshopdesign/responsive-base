var instagramFeed = {
  settings: {
    channel1: {
      userId: '4399522518',
      accessToken: '4399522518.1a62238.2271110925eb431baf58ceba499873ce',
      clientId: '1a622385baed4130bfbdf8194c734ccd',
      instaFeedTranslations: {
        instaFeedHeaderText: {
          sv: "Följ oss på Instagram - @halsokraft",
          en: "Follow us on Instagram - @halsokraft"
        }
      }
    },
    channel2: {
      userId: '4399522518',
      accessToken: '4399522518.1a62238.e0a9ed3ac79544c1a7b05d29981c828b',
      clientId: '1a622385baed4130bfbdf8194c734ccd',
      instaFeedTranslations: {
        instaFeedHeaderText: {
          sv: "Följ oss på Instagram - @halsokraft",
          en: "Follow us on Instagram - @halsokraft"
        }
      }
    },
    postCount: '4',
    imageSize: 'standard_resolution',
  },
  variables: {},
  elements: {},
  init: function () {

    if (J.data.channelInfo.Active == 1) {
      J.translations.push(instagramFeed.settings["channel" + J.data.channelInfo.Active].instaFeedTranslations);
      $("#startpage_list").append(J.views['instagram-feed/instagram-feed']);
      instagramFeed.initInstafeed();
    }
  },
  initInstafeed: function () {

    // var pageId = '17841404395549940';
    // var accessToken = 'EAAJYiZBFXOaYBAGLZCA2PJbcwM9OC82NMFzidwZAp5ZAMgJSQdiEgK0p2wDryArHe1154jxDvNYD5ZCcpluzXRS0H5ihVzl2sfwzSqqEgW4q7GrprZBwynoDH3ZCKbuuyaRG9n6YM93zPYK2mqNHiCWpY1rV8zD6qoQaaG55ZA8WUQZDZD';

    var pageId = '17841438878851888';
    var accessToken = 'EAANBWw32k4ABALxEfvoKI1qr7xxKjMGqzxGNjf0AY7JdOBKlUbUFnSCpznZASEMmsFFdlMt1Y6Mgxf0O2ZBtNnn5ZAGcHMDHNz5mjQ5cin8NLSD2ttChcjnoAtLIvd1B70lSz4WOTjFPb102WuZBwVMQvKEbUMaaamefAhrfKwZDZD';

    var url = 'https://graph.facebook.com/' + pageId + '/media?limit=4&fields=thumbnail_url,media_url,permalink,like_count&access_token=' + accessToken;
    if ($('.instagram-feed-wrapper').length > 0) {
      $.get(url, function (response) {
        // console.log(response);
        $.each(response.data, function (key, value) {
          var imgUrl = value.thumbnail_url ? value.thumbnail_url : value.media_url;

          $('<li><a href="' + value.permalink + '" target="_blank" style="background-image: url(' + imgUrl + ')"> <span class="insta-likes"><i class="fa fa-heart" aria-hidden="true"></i>' + value.like_count + '</span></a></li>').appendTo('#instafeed');
        });
      });
    }

  }
};
J.pages.addToQueue("start-page", instagramFeed.init);




