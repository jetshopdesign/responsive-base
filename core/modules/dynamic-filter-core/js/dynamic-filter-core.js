"use strict";
var dynamicFilterCore = {
	config: {
		debug: true,
		invertedInStock: false,
		defaultFilter: {
			ListValues: {},
			BoolValues: {},
			SpanValues: {},
			MultiLevelListValues: {},
			OrderBy: null,
			SortDirection: null
		}
	},
	data: {
		filterOptions: {}
	},
	init: function() {
		$(document).on('dynamicFilter:core::initialize', dynamicFilterCore.bindEvents );
	},
	bindEvents: function(event, data) {
		$(document)
			.on('dynamicFilter:filterOptions::fetch', dynamicFilterCore.events.fetchFilterOptions)
			.on('dynamicFilter:products::fetch', dynamicFilterCore.events.fetchProducts )
			.on('dynamicFilter:filterOptions::update', dynamicFilterCore.events.updateFilterOptions )
			.on('dynamicFilter:filterOptions::clear', dynamicFilterCore.events.clearFilters )
			.on('dynamicFilter:filterOptions::remove', dynamicFilterCore.events.removeFilterOptions )
			.on('dynamicFilter:sortOrder::update', dynamicFilterCore.events.updateSortOrder )
			.on('dynamicFilter:sortOrder::reset', dynamicFilterCore.events.resetSortOrder );

		$(document).trigger('dynamicFilter:core::initialized');
	},
	events: {
		updateFilterOptions: function(event, filterOptions) {
			var hasChanged = false;
			$.each(filterOptions, function(index,filterOption) {
				switch (filterOption.type) {
					case "MultiLevelListValues":
						dynamicFilterCore.helpers.clearMultiLevelListAbove(filterOption.key);
						filterOption.values = filterOption.values[0]
						if (filterOption.values == -1) {
							filterOption.values = undefined
						}
						break;
					case "SpanValues":
						filterOption.values = filterOption.values[0]
						break;
					case "BoolValues":
						filterOption.values = filterOption.values[0]
						if (!filterOption.values) {
							filterOption.values = undefined
						}

						if (!filterOption.values && filterOption.key === "instock") {
							filterOption.values = false
						}
						break;
					default:
						break;
				}

				var diff = dynamicFilterCore.helpers.arrayDiff(JetshopData.SelectedFilters[filterOption.type][filterOption.key], filterOption.values);

				if (!diff) {
					// Diff failed because input isn't array / object
					if (JetshopData.SelectedFilters[filterOption.type][filterOption.key] === filterOption.values) {
						diff = []
					} else {
						diff = [filterOption.values]
					}
				}

				if (diff.length > 0) {
					JetshopData.SelectedFilters[filterOption.type][filterOption.key] = filterOption.values
					if (JetshopData.SelectedFilters[filterOption.type][filterOption.key] == null) {
						JetshopData.SelectedFilters[filterOption.type][filterOption.key] = undefined;
						delete JetshopData.SelectedFilters[filterOption.type][filterOption.key];
					}
					hasChanged = true;
				}
			})

			if (hasChanged) {
				dynamicFilterCore.triggers.filterOptionsUpdated();
			}
		},
		removeFilterOptions: function(event, filterOptions,triggerEvent) {
			triggerEvent = (triggerEvent == undefined ? true : false)
			$.each(filterOptions, function(index,filterOption) {
				// filter all items within the same key which is not the given filteroption
				switch(filterOption.type) {
					case "MultiLevelListValues":
						if (filterOption.key == 'ALL') {
							dynamicFilterCore.helpers.clearMultiLevelListAbove(filterOption.key)
						}
						JetshopData.SelectedFilters[filterOption.type][filterOption.key] = undefined
						delete JetshopData.SelectedFilters[filterOption.type][filterOption.key]
						break;
					case "SpanValues":
						JetshopData.SelectedFilters[filterOption.type][filterOption.key] = undefined
						delete JetshopData.SelectedFilters[filterOption.type][filterOption.key]
						break;
					case "BoolValues":
						if (dynamicFilterCore.config.invertedInStock && filterOption.key === 'instock') {
							JetshopData.SelectedFilters[filterOption.type][filterOption.key] = false
							break;
						}

						JetshopData.SelectedFilters[filterOption.type][filterOption.key] = undefined;
						delete JetshopData.SelectedFilters[filterOption.type][filterOption.key];

						break;
					case "ListValues":
						var newFilterOption = $.grep(JetshopData.SelectedFilters[filterOption.type][filterOption.key], function(value) {
							return filterOption.value != value;
						})
						JetshopData.SelectedFilters[filterOption.type][filterOption.key] = newFilterOption;
						break;
					default:
						dynamicFilterCore.helpers.log('Non defined type - removeFilterOptions', filterOption.Type)
				}
			});
			if (triggerEvent) {
				dynamicFilterCore.triggers.filterOptionsUpdated();
			}
		},
		fetchFilterOptions: function(event, options) {
			dynamicFilterCoreApi.filter(
				dynamicFilterCore.triggers.filterOptionsFetched,
				null,
				JetshopData.SelectedFilters,
				J.data.categoryId);
		},
		fetchProducts: function(event, options) {
			if (options.xhr) {
				dynamicFilterCoreApi.getFilteredProducts(
					dynamicFilterCore.triggers.productsFetched,
					null,
					JetshopData.SelectedFilters,
					J.data.categoryId,
					options.productsPerPage,
					options.page);
			} else {
				window.location.href = dynamicFilterCore.helpers.urlifySelectedFilters(true);
			}
		},
		clearFilters: function() {
			dynamicFilterCore.helpers.clearSelectedFilters();
			dynamicFilterCore.triggers.filterOptionsUpdated();
			dynamicFilterCore.helpers.pushUrl()
		},
		updateSortOrder: function(event, sortOn) {
			var sortBy = null
			switch(sortOn.sortBy)  {
				case "ArticleNr":
					sortBy = 0;
					break;
				case "Name":
					sortBy = 1;
					break;
				case "PublishedDate":
					sortBy = 2;
					break;
				case "Price":
					sortBy = 3;
					break;
				case "Subname":
					sortBy = 4;
					break;
				case "ExactNumber":
					sortBy = 5;
					break;
				case "Relevance":
					sortBy = 6;
					break;
				default:
					break;
			}
			JetshopData.SelectedFilters.OrderBy = sortBy
			JetshopData.SelectedFilters.SortDirection = sortOn.sortDirection.toUpperCase() === "ASC" ? 0 : 1
			$(document).trigger('dynamicFilter:sortOrder::updated');
		},
		resetSortOrder: function(event) {
			JetshopData.SelectedFilters.OrderBy = null
			JetshopData.SelectedFilters.SortDirection = null
			$(document).trigger('dynamicFilter:sortOrder::reseted');
		}
	},
	triggers: {
		filterOptionsFetched: {
			before: function (result, xhr) {
				$(document).trigger('dynamicFilter:filterOptions::start');
			},
			success: function (result, data, status, xhr) {
				dynamicFilterCore.helpers.pushUrl()
				dynamicFilterCore.data.filterOptions = data
				$(document).trigger('dynamicFilter:filterOptions::success', [data]);
			},
			error: function (result, xhr, status) {
				$(document).trigger('dynamicFilter:products::error',[{status: xhr.statusText, statusCode: xhr.status}]);
			},
			complete: function (result, xhr) {
				$(document).trigger('dynamicFilter:filterOptions::completed');
			}
		},
		productsFetched: {
			before : function (result, xhr) {
				$(document).trigger('dynamicFilter:products::started');
			},
			success: function (result, data, status, xhr) {
				dynamicFilterCore.helpers.pushUrl()
				$(document).trigger('dynamicFilter:products::success',data);
			},
			error: function (result, xhr, status) {
				$(document).trigger('dynamicFilter:products::error',[{status: xhr.statusText, statusCode: xhr.status}]);
			},
			complete: function (result, xhr) {
				$(document).trigger('dynamicFilter:products::completed');
			}
		},
		filterOptionsUpdated: function () {
			// Skicka endast om förändring skett i filterOptions
			$(document).trigger('dynamicFilter:filterOptions::updated', [JetshopData.SelectedFilters]);
		}
	},
	log: function (txt) {
		if (dynamicFilterCore.config.debug) {
			console.log('dynamicFilterCore Debug :: ',txt)
		}
	},
	helpers: {
		pushUrl: function () {
			var newUrl = dynamicFilterCore.helpers.urlifySelectedFilters()
			var oldUrl = new Url(window.location.href)
			if (typeof window.history.pushState === 'function' && oldUrl.toString() !== newUrl.toString()) {
				window.history.pushState({urlPath: newUrl.toString() },'',newUrl.toString());
			}
		},
		clearMultiLevelListAbove: function (key) {
			var filterType = 'MultiLevelListValues';
			if (!dynamicFilterCore.helpers.isArrayOrObject(JetshopData.SelectedFilters[filterType])) {
				return false;
			}

			var removeNext = false;

			if (key == 'ALL') {
				// Special case - remove all, remove label has been triggerd
				removeNext = true;
			}
			$.each(JetshopData.SelectedFilters[filterType], function(index,value) {
				if (removeNext) {
					var removeOption = {
						type: filterType,
						key: index,
						value: ''

					}
					dynamicFilterCore.events.removeFilterOptions(null,[removeOption], false);
				}
				if (index == key) {
					removeNext = true
				}
			})
		},
		arrayDiff: function(compreFrom, compareTo) {
			var a = [], diff = [];
			if (!dynamicFilterCore.helpers.isArrayOrObject(compreFrom)) {
				return false;
			}
			if (!dynamicFilterCore.helpers.isArrayOrObject(compareTo)) {
				return false;
			}

			for (var i = 0; i < compreFrom.length; i++) {
				a[compreFrom[i]] = true;
			}

			for (var i = 0; i < compareTo.length; i++) {
				if (a[compareTo[i]]) {
					delete a[compareTo[i]];
				} else {
					a[compareTo[i]] = true;
				}
			}

			for (var k in a) {
				diff.push(k);
			}

			return diff;
		},
		isArrayOrObject: function (checkVar) {
			var isType = typeof checkVar;
			if (checkVar === null) {
				return false
			}
			if (isType === 'array' || isType === 'object') {
				return true;
			}
			return false;
		},
		clearSelectedFilters: function() {
			JetshopData.SelectedFilters = jQuery.extend(true, {}, dynamicFilterCore.config.defaultFilter);
		},
		urlifySelectedFilters: function (removePageNum) {
			var url = new Url();

			if (removePageNum === undefined) {
				removePageNum = false;
			}

			if (removePageNum) {
				delete(url.query['pagenum']);
			}

			$.each(dynamicFilterCore.data.filterOptions, function(i, value) {
				if(value.GroupFilters) {
					$.each(value.GroupFilters, function(i, groupValue) {
						delete url.query[groupValue.Key];
					});
				}

				if(url.query[value.Key]) {
					delete url.query[value.Key];
				} else if(value.Key === '') {
					delete url.query['pricemin'];
					delete url.query['pricemax'];
				}
			});

			if (JetshopData.SelectedFilters.OrderBy === null) {
				delete url.query['sortby']
			} else {
				url.query['sortby'] = JetshopData.SelectedFilters.OrderBy
			}

			if (JetshopData.SelectedFilters.SortDirection === null) {
				delete url.query['sortdirection']
			} else {
				url.query['sortdirection'] = JetshopData.SelectedFilters.SortDirection
			}

			$.each(JetshopData.SelectedFilters.ListValues, function(key, value) {
				if(dynamicFilterCore.helpers.filterifyArray(value) !== "") {
					url.query[key] = dynamicFilterCore.helpers.filterifyArray(value);
				} else {
					delete url.query[key];
				}
			});

			$.each(JetshopData.SelectedFilters.BoolValues, function(key, value) {
				url.query[key] = value;
			});

			$.each(JetshopData.SelectedFilters.SpanValues, function(key, value) {
				url.query[key] = value;
			});

			$.each(JetshopData.SelectedFilters.MultiLevelListValues, function(key, value) {
				url.query[key] = value;
			});

			return url;
		},
		filterifyArray: function(arr) {
			var str = "";
			$(arr).each(function (i, value) {
				str += value;
				// If not the last item add "-or-" to string as a delimeter for multichoice
				if (i !== (arr.length - 1)) {
					str += "-or-";
				}
			})
			return str;
		},
	}
};

J.pages.addToQueue("category-page", dynamicFilterCore.init);
J.pages.addToQueue("category-advanced-page", dynamicFilterCore.init);

