var dynamicFilterCoreApi = {
    XHRPreloadStorage: [],
    filter: function (callback, callbackOptions, postData, id) {
        var service = "filters/" + id + "/";
        var url = J.api.helpers.baseUrl(true, false) + service;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(postData),
            url: url,
            contentType: 'text/plain',
            beforeSend: function(jqXHR) {
                callback.before(callbackOptions, jqXHR)
            },
            success: function (data, textStatus, jqXHR) {
                callback.success(callbackOptions, data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus) {
                callback.error(callbackOptions, jqXHR, textStatus);
            },
            complete: function (jqXHR) {
                callback.complete(callbackOptions, jqXHR);
            }
        })
    },
    getFilteredProducts: function (callback, callbackOptions, postData, id, rows, page) {
        var service = "categories/" + id + "/" + rows + "/" + page + "/?priceListId=" + JetshopData.PriceListId;
        var url = J.api.helpers.baseUrl(true, true) + service;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(postData),
            url: url,
            contentType: 'text/plain',
            beforeSend: function(jqXHR) {
                callback.before(callbackOptions, jqXHR)
            },
            success: function (data, textStatus, jqXHR) {
                callback.success(callbackOptions, data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus) {
                callback.error(callbackOptions, jqXHR, textStatus);
            },
            complete: function (jqXHR) {
                callback.complete(callbackOptions, jqXHR);
            }
        })
    },
}