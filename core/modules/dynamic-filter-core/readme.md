## Dynamic Filter Core
This modules is only helpers to add filter to Jetshop stores. This module requires a GUI implementation. For example see the `dynamic-filter-guiTop` module.

### Settings

var options = {
	xhr: true, // dynamicFilterGui.config.enableXhr,
	productsPerPage: 24, // dynamicFilterGui.config.productsPerPage,
	page: 1, // dynamicFilterGui.data.productPageToFetch
}

### Events

* `dynamicFilter:core::initialize`
Initialize Dynamic Filter Core and starts listning on events
When Dynamic Filter Core has been initialized it will send the even `dynamicFilter:core::initialized`

* `dynamicFilter:filterOptions::fetch`Fetches the current available filter options
This event have mulitple responses depending on XHR status
	
  * `dynamicFilter:filterOptions::start`This is sent before any call to backend, this event will always be sent 

  * `dynamicFilter:filterOptions::success` XHR vas successfull and filter has been fetched from API, response is sent on the first agrument as an object.
Will send the response as an argument, look at Jetshop API Calls for object structure

  * `dynamicFilter:filterOptions::error`XHR failed, will send XHR status text and status code as object

#### Example of response

	{
		status: "Bad request",
		statusCode: 400
	}

  * `dynamicFilter:filterOptions::completed` This event will always be sent after the API call is completed.

* `dynamicFilter:filterOptions::update`
Updates the current filter options, it requiers an object sent with the object
All values should be arrays even though there's only one value.

#### Example of SpanValues

	[
		{
			type: 'SpanValues',
			key: 'pricemax',
			values: [ '299' ]			
		}
		{
			type: 'SpanValues',
			key: 'pricemin',
			values: [ '199' ]
		}
	]

#### Example of ListValues

	[
		{
			type: 'ListValues',
			key: 'subname',
			values: [ 'grp_Flerfärgad', 'grp_Blå' ]
		}
	]

#### Example of BoolValues

	[
		{
			type: 'BoolValues',
			key: 'instock',
			values: [ true ]
		}
	]

#### Example of MultiLevelListValues

	[
		{
			type: 'MultiLevelListValues',
			key: 'pti_passar-snoskoter_lv1',
			values: [ 'Arctic Cat' ]
		}
	]

When filter has been updated, it will send the event 
`dynamicFilter:filterOptions::updated` and return the current filter

* `dynamicFilter:filterOptions::remove`
Removes a filter option
Will fire `dynamicFilter:filterOptions::removed` event if updated

* `dynamicFilter:filterOptions::clear`
Clears all choosen filter options, no event will be sent 

* `dynamicFilter:products::fetch`
This event fetches the current products for current category
It requires an settings object

This event have mulitple responses depending on XHR status.

  * `dynamicFilter:products::start`
	This is sent before any call to backend, this event will always be sent 

  * `dynamicFilter:products::success`
	XHR vas successfull and filter has been fetched from API, response is sent on the first argument as an object

  * `dynamicFilter:products::error`
	XHR failed, will send XHR status text and status code as object

#### Example of response

	{
		status: "Bad request",
		statusCode: 400
	}

  * `dynamicFilter:products::completed`
	This event will always be sent after the API call is completed.

* `dynamicFilter:sortOrder::update`
Updates the current sort options, it requiers an object sent with the object
#### Example of SortOn with ascending order
	{
		sortBy: "ArticleNr",
		SortDirection: "ASC"
	}

#### Example of SortOn with descending order
	{
		sortBy: "ArticleNr",
		SortDirection: "DESC"
	}

* `dynamicFilter:sortOrder::reset`
Reset the sort order to default.


### Sorting of products

This module also have sorting on products available on following 

* ArticleNr
* Name
* PublishedDate
* Price
* Subname
* ExactNumber
* Relevance

And on ascending or descending order, the direction only support ASC or DESC - See examples above.
This calls will not fetch / reload the page - it handles the sort order and you need to tell core to fetch products with the applied sorting.

When sort order has been applied it will trigger the event `dynamicFilter:sortOrder::updated`

If you need to reset the sort order, you need to trigger `dynamicFilter:sortOrder::reset` without any args and when completed it will respond with triggering `dynamicFilter:sortOrder::reseted`
