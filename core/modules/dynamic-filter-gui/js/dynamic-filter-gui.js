"use strict"

var dynamicFilterGui = {
	config: {
		enableXhr: true,
		productsPerPage: 24,
	},
	data: {
		productPageToFetch: 1
	},
	init: function() {
		// Listen to the activation of dynamic filter core.
		$(document).on('dynamicFilter:core::initialized', dynamicFilterGui.bindEvents);

		// Trigger activation of Dynamic Filter core to see if core is activated.
		$(document).trigger('dynamicFilter:core::initialize');
	},
	bindEvents: function() {
		$(document)
			.on('dynamicFilter:filterOptions::success', dynamicFilterGui.watchers.filterOptionsSuccess)
			.on('dynamicFilter:filterOptions::updated', dynamicFilterGui.watchers.filterOptionsUpdated)
			.on('dynamicFilter:filterOptions::removed', dynamicFilterGui.watchers.filterOptionsRemoved)
			.on('dynamicFilter:products::started', dynamicFilterGui.watchers.productsStarted)
			.on('dynamicFilter:products::success', dynamicFilterGui.watchers.productsSuccess) 
			.on('dynamicFilter:products::error', dynamicFilterGui.watchers.productsError) 
			.on('dynamicFilter:products::completed', dynamicFilterGui.watchers.productsCompleted)
			.on('dynamicFilter:products::initialize', dynamicFilterGui.watchers.initializeDynamicFilterProducts)
			.on('dynamicFilter:products::reset', dynamicFilterGui.watchers.resetProducts)

		// We start with triggering fetchFilterOptions
		$(document).trigger('dynamicFilter:filterOptions::fetch');

		if (dynamicFilterGui.config.enableXhr) {
			$(document).trigger('dynamicFilter:products::initialize');
		}
	},
	watchers: {
		filterOptionsUpdated: function() {console.log('DF::GUI:FILTER_OPTIONS_UPDATED')},
		filterOptionsSuccess: function() {console.log('DF::GUI:FILTER_OPTIONS_SUCCESS')},
		resetProducts: function() {console.log('DF::GUI:RESETTING_PRODUCTS')},
		initializeDynamicFilterProducts: function() {console.log('DF::GUI:INITIALIZING_PRODUCTS')},
		dynamicFilterProductsInitialized: function() {console.log('DF::GUI:PRODUCTS_INITIALIZED');},
		productsStarted: function(event) {console.log('DF::GUI:PRODUCTS_STARTED');},
		productsError: function(event,error) { console.log('DF::GUI:PRODUCTS_ERROR', error) },
		productsSuccess: function(event, products) { console.log('DF::GUI:PRODUCTS_SUCCESS', products);},
		productsCompleted: function(event) { console.log('DF::GUI:PRODUCTS_COMPLETED');},
	},
	triggers: {
		clearFilters: function(event) {
			// Trigger clear all filters to core
			$(document).trigger('dynamicFilter:filterOptions::clear');
		},
		removeFilterOption: function(array) {
			// Trigger to remove specific filteroption to core
			$(document).trigger('dynamicFilter:filterOptions::remove', [array]);
		},
		updateFilterOptions: function(array) {
			// Trigger filter options update to core.
			$(document).trigger('dynamicFilter:filterOptions::update', [array] )
		},
		fetchProducts: function() {
			// Example of fetching products
			var options = {
				xhr: dynamicFilterGui.config.enableXhr,
				productsPerPage: dynamicFilterGui.config.productsPerPage,
				page: dynamicFilterGui.data.productPageToFetch
			}
			$(document).trigger('dynamicFilter:products::fetch', options)
		}
	},
	renderProducts: function(data) {
		// Render Products
	},
	renderFilterOptions: function(event, filterOptions) {
		// Render filter options
	},
};

J.pages.addToQueue("category-page", dynamicFilterGui.init);
