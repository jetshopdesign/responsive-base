Handlebars.registerHelper('classFriendly', function(v1) {
    var str = arguments[0];
    //Lower case everything
    str = str.toLowerCase();
    //Convert whitespaces and underscore to dash
    str = str.replace(/\s/g, "-");
    return str;
});	

Handlebars.registerHelper('currency', function (value) {
    if (J.data.jetshopData) {
        return value + ' ' + J.data.currency.symbol;
    }
    else {
        return value;
    }
});