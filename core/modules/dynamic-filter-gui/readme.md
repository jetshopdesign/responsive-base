#### GUI Events

* `dynamicFilter:core::initialize`
Fire event to initialize Core functions

* `dynamicFilter:core::initialized`
Core acknowledge that it has been initialized and now listen to different events.

* `dynamicFilter:filterOptions::updated`
Is sent when the filter has been updated

* `dynamicFilter:filterOptions::removed`
Is sent when something was removed from the filter

* `dynamicFilter:filterOptions::started`
Is sent before starting the XHR

* `dynamicFilter:filterOptions::success`
Event from Core that tells GUI that filterOptions has been successfully fetch from API

* `dynamicFilter:filterOptions::error`
Event from Core when something went wrong with fetching data from API


* `dynamicFilter:filterOptions::completed`
Even that filterOptions xhr has been completed

* `dynamicFilter:products::started`
Starting to fetch products

* `dynamicFilter:products::success`
Products fetched and response is passed in the event, look at doc's for jetshop api for structur

* `dynamicFilter:products::error`
Event from Core when something went wrong with fetching data from API, see `dynamicFilter:filterOptions::error` for object passed in event

* `dynamicFilter:products::completed`
XHR for fetching products has been completed

* `dynamicFilter:products::reset`
Internal event to clear products
