var secondProductImageHover = {
    catID: J.data.categoryId,
    imageSize: "medium",
    init: function () {
        secondProductImageHover.appendImages();
        secondProductImageHover.bindHover();
    },
    appendImages: function () {
        var images = JetshopData.Images;

        // Loop products
        $('.product-outer-wrapper').each(function () {
            var $this = $(this),
                productId = $this.find('.product-wrapper h3 a').attr('data-productid'),
                secondImage = images[productId][1],
                prodUrl = $this.find('.product-image a').first().attr('href');

                if ( secondImage == 'undefined') {
                    return;
                }

            // Get second image if exists
            if (typeof secondImage != 'undefined') {
                var imgSrc = '/pub_images/' + secondProductImageHover.imageSize + '/' + secondImage,
                    imgString = '<a href="' + prodUrl + '" class="secondary-image" style=""><img class="lazyimg" data-original="' + imgSrc + '" src="../images/grey.gif" /></a>';

                $this.find('.product-image').append(imgString);
            }
        });
    },
    bindHover: function () {
        var $wrapper = $('.product-wrapper');

        $wrapper.on('mouseenter', function () {
            var $this = $(this),
                $firstImage = $this.find('a').first(),
                $secondImage = $this.find('a.secondary-image'),
                secondImageSrc = $secondImage.find('img').attr('data-original');

            if ( $secondImage.length > 0 ) {
                $secondImage.find('img.lazyimg')
                    .attr('src', secondImageSrc)    // lazy load on hover
                    .removeClass('lazyimg');        // prevent multiple lazy loads

                $firstImage.removeClass('active');
                $secondImage.addClass('active');
            }
        });
        $wrapper.on('mouseleave', function () {
            var $this = $(this),
                $image2 = $this.find('a.secondary-image'),
                $firstImage = $this.find('a').first();

            if ( $image2.length > 0 ) {
                $image2.removeClass('active');
                $firstImage.addClass('active');
            }
        });
    }
};

J.pages.addToQueue('category-page', function() {
    if ( J.checker.isTouch == false ) {
        secondProductImageHover.init();
    }
});