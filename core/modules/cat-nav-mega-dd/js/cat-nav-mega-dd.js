// INIT TOP DYNAMIC MENU -------------
var catNav = {
    initTopMenu: function () {

        J.translations.push(
            {
                seeAll: {
                    sv: "Se alla",
                    en: "See all",
                    da: "Se alle",
                    nb: "Se alle",
                    fi: "Katso kaikki"
                }
            }
        );

        var catNav = $("#cat-nav");
        var navBar = catNav.children(".nav-bar");
        // TOP MENU STYLE
        catNav.addClass("style-megadropdown");
        // MAKE SLIGHT DELAY ON HOVER
        var hoverAction;
        catNav.on("mouseenter", function () {
            clearTimeout(hoverAction);
            hoverAction = setTimeout(function () {
                catNav.addClass("hovered");
            }, 100);
        }).on("mouseleave", function () {
            clearTimeout(hoverAction);
            catNav.removeClass("hovered");
            catNav.checkTopMenuLayout();
        });

        // ADD SEE ALL LINK
        $("#cat-nav li.lv2").each(function () {
            var lv2Item = $(this);
            var lv2LinkHref = lv2Item.children("a").attr("href");
            var lv3List = lv2Item.children("ul.lv3");
            lv3List.append("<li class='see-all'><a href='" + lv2LinkHref + "'>" + J.translate("seeAll") + "</a></li>");
        });

        // ADD TOUCH SUPPORT FOR DESKTOP
        if (J.checker.isTouch && Foundation.utils.is_large_up()) {
            var expandingElements = $("#cat-nav li.lv1.has-subcategories > a");
            expandingElements.click(function (event) {
                var clickedNodeLink = $(this);
                event.preventDefault();
                if (!clickedNodeLink.parent().hasClass("clicked-once")) {
                    $("#cat-nav .clicked-once").not(clickedNodeLink.closest(".lv1")).removeClass("clicked-once");
                    $(this).parent().addClass("clicked-once");
                }
                else {
                    location.href = $(this).attr("href");
                }
            });
            $("body").on("click", function (event) {
                var target = $(event.target);
                if ($("#cat-nav .clicked-once").length && !target.closest("#cat-nav").length) {
                    $("#cat-nav .clicked-once").removeClass("clicked-once");
                }
            });
        }

        // STOP CLICK PROPAGATION FOR LINKS
        if (Foundation.utils.is_large_up()) {
            catNav.find('ul.category-navigation').on("click", "a", function (event) {
                event.stopPropagation();
            });
        }

        // TOP CAT MENU SPACE CALCULATION
        var lastWindowHeight = 0;
        catNav.checkTopMenuLayout = function () {
            if (Foundation.utils.is_large_up()) {
                var winScrollTop = $(window).scrollTop();
                var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
                var navBarTopOffset = navBar.offset().top;
                var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
                var viewportHeight = $(window).height();
                var currentTopOffset = 0; // OFFSET TO BE USED
                var viewportMaxCoverRatio = 0.9; // HOW MUCH OF VIEWPORT CAN BE COVERED BY MENU
                var windowEnlarged = false;
                // CHECK IF WINDOW WAS ENLARGED
                if (viewportHeight > lastWindowHeight) {
                    windowEnlarged = true;
                }
                lastWindowHeight = viewportHeight;
                // CHECK IF TOP MENU BAR SHOULD BE FIXED
                if (winScrollTop > (catNavTopOffset + navBarHeight)) { // IS SCROLLED
                    if (!catNav.is(".hovered")) {
                        catNav.height(navBarHeight);
                        $("html").addClass("menu-scrolled");
                        setTimeout(function () {
                            $("html").removeClass("menu-static");
                        }, 10);
                        currentTopOffset = navBarHeight;
                    }
                }
                else { // RETURN TO NORMAL
                    $("html").removeClass("menu-scrolled");
                    catNav.css("height", "auto");
                    setTimeout(function () {
                        $("html").addClass("menu-static");
                    }, 10);
                    currentTopOffset = catNavTopOffset;
                }
                var menuSpace = viewportHeight - currentTopOffset;
                if (winScrollTop <= currentTopOffset) {
                    menuSpace += winScrollTop;
                }
                else {
                    menuSpace += currentTopOffset;
                }
                // IF WINDOW WAS ENLARGED, SHOW ALL HIDDEN CATS
                if (windowEnlarged) {
                    catNav.find("li.lv3.hidden").removeClass("hidden");
                }
                // RESIZING FUNCTION
                catNav.resizeMegadropdownList = function (listElement) {
                    // START RESIZING ALL LV2 CATS IF NOT SPECIFIED
                    if (!listElement) {
                        listElement = $("#cat-nav ul.lv2");
                    }
                    catNav.addClass("resizing");
                    // CHECK EACH LV2 HEIGHT TO SEE IF IT FITS
                    listElement.each(function (index) {
                        var lv2List = $(this);
                        var lv2CatHeight = lv2List.height();
                        var largestLv3CatQty = 0;
                        if (lv2CatHeight > (menuSpace * viewportMaxCoverRatio)) {
                            //log("Cat menu too high - resizing...");
                            lv2List.find("ul.lv3").each(function () {
                                var lv3List = $(this);
                                var lv3CatQty = lv3List.find("li.lv3").not(".hidden").length;
                                if (lv3CatQty > largestLv3CatQty) {
                                    largestLv3CatQty = lv3CatQty;
                                }
                            });
                            // HIDE LAST 3 LV3 OF THE HIGHEST LV2 CATS
                            lv2List.find("ul.lv3").each(function () {
                                var lv3List = $(this);
                                var catQtyToHide = largestLv3CatQty - 3;
                                if (catQtyToHide >= 0) {
                                    lv3List.find("li.lv3:gt(" + catQtyToHide + ")").addClass("hidden");
                                }
                                else {
                                    lv3List.find("li.lv3").addClass("hidden");
                                }
                            });
                            // RE-CHECK HEIGHT FOR THIS CAT IF NOT ALREADY AT MINIMUM
                            if (lv2List.find("li.lv3").not(".hidden").length) {
                                setTimeout(function () {
                                    catNav.resizeMegadropdownList(lv2List);
                                }, 1);
                            }
                        }
                    });
                    catNav.removeClass("resizing");
                };
                if (!catNav.is(".hovered")) {
                    catNav.resizeMegadropdownList();
                }
            }
        };

        // CHECK MENU HEIGHT
        catNav.checkTopMenuLayout();

        // BIND DYNAMIC MENU SPACE CALC TO RESIZE AND SCROLL
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {
                // RESIZE DESKTOP MENU
                catNav.checkTopMenuLayout();
            }, 100);
        });

        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {

                // RESIZE DESKTOP MENU
                catNav.checkTopMenuLayout();
            }, 100);
        });

        // THIS RESETS MENU WHEN GOING FROM MEDIUM TO LARGE
        // FIRED FROM J.switch.toLargeUp
        catNav.resetMobileMenuStyles = function () {
            catNav.add(navBar).removeAttr("style");
        };

        // ADD CHECK FOR MOBILE STYLING WHEN IN LARGE-UP
        J.switch.addToLargeUp(catNav.resetMobileMenuStyles);
    }
};

J.pages.addToQueue("all-pages", catNav.initTopMenu);
