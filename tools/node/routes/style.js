//
//  GET /style page.
//
var fs = require("fs");
var path = require("path");
require("../node-functions.js");

var whiteList = [
    {
        "label": "Primary Color",
        "key": "$primary-color",
        "type": "color"
    },
    {
        "label": "Page width",
        "key": "$js-max-page-width",
        "type": "text"

    },
    {
        "label": "Logo max width",
        "key": "$js-logo-wrapper-max-width",
        "type": "text"
    },
    {
        "label": "Primary link color",
        "key": "$js-primary-link-color",
        "type": "color"
    },
    {
        "label": "Font Family",
        "key": "$font-family-sans-serif",
        "type": "font"
    },
    {
        "label": "Page padding: Left",
        "key": "$js-page-padding-left",
        "type": "text"

    },
    {
        "label": "Page padding: Right",
        "key": "$js-page-padding-right",
        "type": "text"

    },
    {
        "label": "Buy-button background color",
        "key": "$js-button-buy-background-color",
        "type": "color"

    },
    {
        "label": "Breakpoints",
        "type": "group"
    },
    {
        "label": "Breakpoint: Small",
        "key": "$small-breakpoint",
        "type": "text"

    },
    {
        "label": "Breakpoint: Medium",
        "key": "$medium-breakpoint",
        "type": "text"

    },
    {
        "label": "Breakpoint: Large",
        "key": "$large-breakpoint",
        "type": "text"

    },
    {
        "label": "Breakpoint: X-Large",
        "key": "$xlarge-breakpoint",
        "type": "text"

    },
    {
        "label": "Category",
        "type": "group"
    },
    {
        "label": "Image proportion",
        "key": "$js-product-wrapper-image-wrapper-height",
        "type": "text"
    },
    {
        "label": "Product per row, size small",
        "key": "$js-total-products-small",
        "type": "text"
    },
    {
        "label": "Product per row, size medium",
        "key": "$js-total-products-medium",
        "type": "text"

    },
    {
        "label": "Product per row, size large",
        "key": "$js-total-products-large",
        "type": "text"
    },
    {
        "label": "Product per row, size x-large",
        "key": "$js-total-products-xlarge",
        "type": "text"
    },
    {
        "label": "Product per row, size xx-large",
        "key": "$js-total-products-xxlarge",
        "type": "text"
    }

];

exports.index = function (req, res) {
    res.render('style', {
        layout: false,
        data: getStyleFile('core/scss/_base_settings.scss', whiteList)
    });
};


exports.save = function (req, res) {
    updateStyleFile('core/scss/_base_settings.scss', req.body.data, res);
};